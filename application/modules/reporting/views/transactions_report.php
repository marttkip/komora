<?php

$visit_date = date('jS M Y',strtotime($transaction_date));

$months_date = date('M Y',strtotime($transaction_date));


$where = 'payments.lease_id = leases.lease_id AND payment_method.payment_method_id = payments.payment_method_id AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND payments.payment_status = 1 AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0 AND payments.payment_date = "'.$transaction_date.'"';
$table = 'payments, leases,rental_unit,property,tenant_unit,tenants,payment_method';

$total_payments = $this->reporting_model->get_total_cash_collection($where, $table);
$normal_payments = $this->reporting_model->get_normal_payments($where, $table);
$payment_methods = $this->reporting_model->get_payment_methods($where, $table);

$total_cash_breakdown = 0;
$transactions='';
if($payment_methods->num_rows() > 0)
{
    foreach($payment_methods->result() as $res)
    {
        $method_name = $res->payment_method;
        $payment_method_id = $res->payment_method_id;
        $total = 0;

        if($normal_payments->num_rows() > 0)
        {
            foreach($normal_payments->result() as $res2)
            {
                $payment_method_id2 = $res2->payment_method_id;

                if($payment_method_id == $payment_method_id2)
                {
                    $total += $res2->amount_paid;
                }
            }
        }

        $total_cash_breakdown += $total;

        $transactions .=
        '
        <tr>
            <th style="text-align:left">'.strtoupper($method_name).'</th>
            <td style="text-align:right">'.number_format($total, 2).'</td>
        </tr>
        ';
    }

    $transactions .=
    '
    <tr>
        <th style="text-align:left">TOTAL</th>
        <td style="text-align:right;border-top:#000 2px solid;">'.number_format($total_cash_breakdown, 2).'</td>
    </tr>
    ';

}


$landlord_expenses = $this->reporting_model->get_expense_records($transaction_date,1);
$inhouse_expenses = $this->reporting_model->get_expense_records($transaction_date,0);

$received_where = 'lease_invoice.lease_invoice_id = invoice.lease_invoice_id AND invoice.month = "'.date('m').'"
AND invoice.year = "'.date('Y').'" AND  lease_invoice.invoice_deleted = 0 ';
$received_select = 'sum(invoice.invoice_amount) AS number';
$received_table = 'invoice,lease_invoice';
$total_month_invoices = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);



$received_where = 'payments.payment_id = payment_item.payment_id AND payment_item.payment_month = "'.date('m').'"
AND payment_item.payment_year = "'.date('Y').'" AND  payments.cancel = 0 ';
$received_select = 'sum(payment_item.amount_paid) AS number';
$received_table = 'payment_item,payments';
$total_month_payments = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);




$percentage4 = ($total_month_invoices-$total_month_payments)/$total_month_invoices *100;
$percentage4 = 100 - $percentage4;




$received_where = 'DATE(created) = "'.date('Y-m-d').'"';
$received_select = 'sum(amount) AS number';
$received_table = 'mpesa_transactions';
$total_received = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);


$received_where = 'DATE(mpesa_transactions.created) = "'.date('Y-m-d').'" AND mpesa_transactions.mpesa_id = payment_item.mpesa_id AND payment_item.payment_id = payments.payment_id AND payments.cancel = 0 ';
$received_select = 'sum(payment_item.amount_paid) AS number';
$received_table = 'mpesa_transactions,payment_item,payments';
$total_reconcilled = $this->dashboard_model->count_items_group($received_table, $received_where,$received_select);

echo '<p>Good evening to you,<br>
  		Herein is a report of today '.$visit_date.'
  		</p>


      <h4 style="text-decoration:underline"><strong>'.strtoupper($months_date).' REVENUE SUMMARY</strong></h4>
      <table  class="table table-hover table-bordered ">
      <thead>
        <tr>
          <th width="70%"></th>
          <th width="30%"></th>
        </tr>
      </thead>
      </tbody>
        <tr>
            <td style="text-align:left">INVOICED</td>
            <td style="text-align:right">'.number_format($total_month_invoices, 2).'</td>
        </tr>
        <tr>
            <td style="text-align:left">COLLECTED</td>
            <td style="text-align:right">'.number_format($total_month_payments, 2).'</td>
        </tr>
        <tr>
            <td style="text-align:left"> PENDING</td>
            <td style="text-align:right">'.number_format($total_month_invoices-$total_month_payments, 2).'</td>
        </tr>
        <tr>
            <td style="text-align:left">PERCENTAGE</td>
            <td style="text-align:right">'.number_format($percentage4, 2).' %</td>
        </tr>

      </tbody>
      </table>

  		<h4 style="text-decoration:underline"><strong>COLLECTIONS SUMMARY</strong></h4>
  		<table  class="table table-hover table-bordered ">
  				<thead>
  					<tr>
  						<th width="70%"></th>
  						<th width="30%"></th>
  					</tr>
  				</thead>
  				</tbody>
  		      '.$transactions.'
  		  	</tbody>

  		</table>
      <h4 style="text-decoration:underline"><strong>PAYBILL RECONCILLIATION SUMMARY</strong></h4>

      <table  class="table table-hover table-bordered ">
          <thead>
            <tr>
              <th width="70%"></th>
              <th width="30%"></th>
            </tr>
          </thead>
          </tbody>
            <tr>
                <td style="text-align:left">TRANSACTIONS TOTAL</td>
                <td style="text-align:right">'.number_format($total_received, 2).'</td>
            </tr>
            <tr>
                <td style="text-align:left">RECONCILLED AMOUNT</td>
                <td style="text-align:right">'.number_format($total_reconcilled, 2).'</td>
            </tr>
          </tbody>

      </table>
      <h4 style="text-decoration:underline"><strong>EXPENSES</strong></h4>
      <table  class="table table-hover table-bordered ">
          <thead>
            <tr>
              <th width="70%"></th>
              <th width="30%"></th>
            </tr>
          </thead>
          </tbody>
            <tr>
                <td style="text-align:left">LANDLORD EXPENSES</td>
                <td style="text-align:right">'.number_format($landlord_expenses, 2).'</td>
            </tr>
            <tr>
                <td style="text-align:left">INHOUSE EXPENSES</td>
                <td style="text-align:right">'.number_format($inhouse_expenses, 2).'</td>
            </tr>
            <tr>
                <td style="text-align:left"><strong>TOTAL EXPENSES</strong></td>
                <td style="text-align:right;border-top:#000 2px solid;">'.number_format($inhouse_expenses+$landlord_expenses, 2).'</td>
            </tr>
          </tbody>

      </table>
		';

?>
