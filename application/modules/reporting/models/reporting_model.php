<?php
class Reporting_model extends CI_Model
{

  	/*
  	*	Retrieve total revenue
  	*
  	*/
  	public function get_total_cash_collection($where, $table, $page = NULL)
  	{
  		//payments
  		$table_search = $this->session->userdata('all_transactions_tables');

  		if($page != 'cash')
  		{
  			$where .= ' AND payments.cancel = 0';
  		}
  		if((!empty($table_search)) || ($page == 'cash'))
  		{
  			$this->db->from($table);
  		}

  		else
  		{
  			$this->db->from($table);
  		}
  		$this->db->select('SUM(payments.amount_paid) AS total_paid');
  		$this->db->where($where);
  		$query = $this->db->get();

  		$cash = $query->row();
  		$total_paid = $cash->total_paid;
  		if($total_paid > 0)
  		{
  		}

  		else
  		{
  			$total_paid = 0;
  		}

  		return $total_paid;
  	}

  	/*
  	*	Retrieve total revenue
  	*
  	*/
  	public function get_normal_payments($where, $table, $page = NULL)
  	{
  		if($page != 'cash')
  		{
  			$where .= ' AND payments.cancel = 0';
  		}
  		//payments
  		$table_search = $this->session->userdata('all_transactions_tables');
  		if((!empty($table_search)) || ($page == 'cash'))
  		{
  			$this->db->from($table);
  		}

  		else
  		{
  			$this->db->from($table);
  		}
  		$this->db->select('*');
  		$this->db->where($where);
  		$query = $this->db->get();

  		return $query;
  	}
  	public function get_payment_methods()
  	{
  		$this->db->select('*');
  		$query = $this->db->get('payment_method');

  		return $query;
  	}


    public function get_expense_records($transaction_date,$type)
    {

      if($type == 1)
      {
        $type_check = ' AND projectId > 0';
      }
      else {
        $type_check = ' AND projectId = 0';
      }
      $this->db->select('SUM(dr_amount) AS total_amount');
      $this->db->where('transactionCategory = "Expense" AND transactionDate = "'.$transaction_date.'" '.$type_check);
  		$query = $this->db->get('v_general_ledger');
      $row = $query->row();

      $total_amount = $row->total_amount;

  		return $total_amount;

    }
    public function get_amount_collected_invoice_type($invoice_type_id,$month_id,$year,$property_owner_id=null)
  	{
          $prop = $this->session->userdata('search_property');
  		if(!empty($prop))
  		{
  			$add = $this->session->userdata('search_property');
  		}
  		else
  		{
  			$add = '';
  		}


  		if(!empty($property_owner_id))
  		{
  			$add .= ' AND property.property_owner_id ='.$property_owner_id;
  		}
  		else
  		{
  			$add .= '';
  		}

  		if($invoice_type_id == NULL)
  		{
  			$where = 'payments.payment_status = 1 AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id  AND MONTH(payments.payment_date) = \''.$month_id.'\' AND YEAR(payments.payment_date) =  '.$year.'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0  '.$add;

  		}
  		else
  		{
  			$where = 'payments.payment_status = 1 AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id  AND MONTH(payments.payment_date) = \''.$month_id.'\' AND YEAR(payments.payment_date) =  '.$year.'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0  AND payment_item.invoice_type_id = '.$invoice_type_id.' '.$add;
  		}

  		// var_dump($where); die();
  		$this->db->select('SUM(payment_item.amount_paid) AS total_amount');
  		$this->db->where($where);
  		$query = $this->db->get('payments,payment_item,leases,rental_unit,property');
  		$total_amount = 0;
  		if($query->num_rows() > 0)
  		{
  			foreach ($query->result() as $key) {
  				# code...
  				$total_amount = $key->total_amount;
  			}
  		}

  		return $total_amount;
  	}
}
?>
