<?php 
	
	$contacts = $this->site_model->get_contacts();
	
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$email2 = $contacts['email'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$phone = $contacts['phone'];
		
		if(!empty($facebook))
		{
			$facebook = '<li class="facebook"><a href="'.$facebook.'" target="_blank" title="Facebook">Facebook</a></li>';
		}
		
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
?>
<!doctype html>
<html>
	<head>
        <?php echo $this->load->view('admin/includes/header', $contacts, TRUE); ?>
    </head>
  
	<body class="hold-transition login-page">
		<div class="login-box">
		  <div class="login-logo">
		    <a href="<?php echo site_url().'login';?>"><b><?php echo $company_name;?></a>
		  </div>
		  <!-- /.login-logo -->
		  <div class="login-box-body">
		    <p class="login-box-msg">Sign in to start your session</p>

		    <?php
				$login_error = $this->session->userdata('login_error');
				$this->session->unset_userdata('login_error');
				
				if(!empty($login_error))
				{
					echo '<div class="alert alert-danger">'.$login_error.'</div>';
				}
			?>
				<form action="<?php echo site_url().$this->uri->uri_string();?>" method="post">
			      <div class="form-group has-feedback">
			        <input type="text" class="form-control" name="personnel_username" placeholder="Username">
			        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			      </div>
			      <div class="form-group has-feedback">
			        <input type="password" class="form-control" name="personnel_password" placeholder="Password">
			        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
			      </div>
			      <div class="row">
			        <div class="col-xs-8">
			          
			        </div>
			        <!-- /.col -->
			        <div class="col-xs-4">
			          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
			        </div>
			        <!-- /.col -->
			      </div>
			    </form>

		    <!-- <a href="#">I forgot my password</a><br> -->

		  </div>
		  <!-- /.login-box-body -->
		</div>
		<!-- /.login-box -->
      <!-- jQuery 3 -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/jquery/dist/jquery.min.js"></script>
		<!-- Bootstrap 3.3.7 -->
		<script src="<?php echo base_url()."assets/themes/admin/";?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- iCheck -->
		
	</body>
</html>
