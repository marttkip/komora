<?php

class Auth_model extends CI_Model
{
	/*
	*	Validate a personnel's login request
	*
	*/
	public function validate_personnel()
	{
		//select the personnel by username from the database
		$this->db->select('*');
		$this->db->where(
			array(
				'personnel_username' => $this->input->post('personnel_username'),
				'personnel_status' => 1,
				'personnel_password' => md5($this->input->post('personnel_password'))
			)
		);
		$this->db->join('branch', 'branch.branch_id = personnel.branch_id');
		$query = $this->db->get('personnel');

		//if personnel exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();

			// get an active branch

			//$branch_details = $this->get_active_branch();

			//create personnel's login session
			$properties = $this->get_allocated_personnel_properties($result[0]->personnel_id);

			$properties = $this->get_log_date($result[0]->personnel_id);

			$newdata = array(
                   'login_status'     			=> TRUE,
                   'first_name'     			=> $result[0]->personnel_fname,
                   'username'     				=> $result[0]->personnel_username,
                   'personnel_id'  				=> $result[0]->personnel_id,
                   'branch_id'  				=> $result[0]->branch_id,
                   'branch_code'  				=> $result[0]->branch_code,
                   'branch_name'  				=> $result[0]->branch_name,
								   'authorize_invoice_changes'	=> $result[0]->authorize_invoice_changes,
								   'authorize_supervisor_changes'	=> $result[0]->authorize_supervisor_changes,
								    'properties'	=> $properties,
										'session_'	=> $session_number
               );

			$this->session->set_userdata($newdata);

			//update personnel's last login date time
			$this->update_personnel_login($result[0]->personnel_id);
			return TRUE;
		}

		//if personnel doesn't exist
		else
		{
			return FALSE;
		}
	}
	/*
	*	Validate a personnel's login request
	*
	*/
	public function validate_personnel_mobile()
	{
		//select the personnel by username from the database
		$this->db->select('*');
		$this->db->where(
			array(
				'personnel_username' => $this->input->post('personnel_username'),
				'personnel_status' => 1,
				'personnel_password' => md5($this->input->post('personnel_password'))
			)
		);
		$this->db->join('branch', 'branch.branch_id = personnel.branch_id');
		$query = $this->db->get('personnel');

		//if personnel exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();

			// get an active branch

			//$branch_details = $this->get_active_branch();

			//create personnel's login session
			$properties = $this->get_allocated_personnel_properties($result[0]->personnel_id);

			$newdata = array(
                   'first_name'     			=> $result[0]->personnel_fname,
                   'username'     				=> $result[0]->personnel_username,
                   'personnel_id'  				=> $result[0]->personnel_id,
                   'properties'  				=> $properties,
               );

			//update personnel's last login date time
			$this->update_personnel_login($result[0]->personnel_id);
			return $newdata;
		}

		//if personnel doesn't exist
		else
		{
			return FALSE;
		}
	}

	public function get_allocated_personnel_properties($personnel_id)
	{
		//retrieve all users
		$this->db->select('personnel_properties.*,property.*');
		$this->db->from('personnel_properties');
		$this->db->join('property','property.property_id = personnel_properties.property_id','left');
		$this->db->join('property_owners','property.property_owner_id = property_owners.property_owner_id','left');
		$this->db->where('personnel_properties_status = 1 AND personnel_properties.personnel_id = '.$personnel_id);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			$count = $query->num_rows();
			$x = 0;
			$property_list = '';
			foreach ($query->result() as $key => $value) {
				# code...
				$x++;

				$property_id = $value->property_id;

				$property_list .= ' AND property.property_id ='.$property_id;
				if($x < $count)
				{
					$property_list .= ',';
				}


			}
		}
		else
		{
			$property_list = FALSE;
		}

		return $property_list;


	}
	public function get_log_date($personnel_id)
	{

		$this->db->where('branch_status = 1');
		$this->db->from('branch');
		$query = $this->db->get();



		if($query->num_rows() == 0)
		{
			// create
			$array['personnel_id'] = $personnel_id;
			$array['logged_date'] = date('Y-m-d');
			$array['session_number'] = $session_number = date('Ymdhis');
			$this->db->insert('user_log',$array);
			$user_log_id = $this->db->insert_id();
			// $this->db->where('user_log_id = '.$user_log_id);
			// $this->db->from('user_log');
			// $query = $this->db->get();
			// $result = $query->row();
			$session_number = $session_number;

		}
		else {
			$result = $query->row();
			$session_number = $result->session_number;
		}

		return $session_number;

	}
	public function get_active_branch()
	{
		$this->db->where('branch_status = 1');
		$this->db->from('branch');
		$query = $this->db->get();

		$result = $query->row();

		return $result;
	}

	/*
	*	Update personnel's last login date
	*
	*/
	private function update_personnel_login($personnel_id)
	{
		$data['last_login'] = date('Y-m-d H:i:s');
		$this->db->where('personnel_id', $personnel_id);
		$this->db->update('personnel', $data);
	}

	/*
	*	Reset a personnel's password
	*
	*/
	public function reset_password($personnel_id)
	{
		$new_password = substr(md5(date('Y-m-d H:i:s')), 0, 6);

		$data['personnel_password'] = md5($new_password);
		$this->db->where('personnel_id', $personnel_id);
		$this->db->update('personnel', $data);

		return $new_password;
	}

	/*
	*	Check if a has logged in
	*
	*/
	public function check_login()
	{
		if($this->session->userdata('login_status'))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}

	public function get_personnel_roles($personnel_id)
	{
	}
}
?>
