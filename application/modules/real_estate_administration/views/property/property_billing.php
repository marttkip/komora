<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Invoice Type</th>
						<th>Billing Schedule</th>
						<th>Charge To</th>
						<th>Billing Amount</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			
			foreach ($query->result() as $row)
			{
				$property_billing_id = $row->property_billing_id;
				$invoice_type_name = $row->invoice_type_name;
				$billing_schedule_name = $row->billing_schedule_name;
				$billing_amount = $row->billing_amount;
				$created = $row->created;
				$charge_to = $row->charge_to;
				$property_billing_status = $row->property_billing_status;
				
				if($charge_to == 1)
				{
					$charge_to_name = 'Tenant';
				}
				else
				{
					$charge_to_name = 'Owner';
				}
				//status
				if($property_billing_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($property_billing_status == 0)
				{
					$status = '<span class="label label-default"> Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'activate-property/'.$property_billing_id.'" onclick="return confirm(\'Do you want to activate '.$invoice_type_name.'?\');" title="Activate '.$invoice_type_name.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				//create activated status display
				else if($property_billing_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'deactivate-property/'.$property_billing_id.'" onclick="return confirm(\'Do you want to deactivate '.$invoice_type_name.'?\');" title="Deactivate '.$invoice_type_name.'"><i class="fa fa-thumbs-down"></i></a>';
				}
			
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$invoice_type_name.'</td>
						<td>'.$billing_schedule_name.'</td>
						<td>'.$charge_to_name.'</td>
						<td>'.number_format($billing_amount).'</td>
						<td>'.$button.'</td>
						<td><a href="'.site_url().'delete-billing/'.$property_billing_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$invoice_type_name.'?\');" title="Delete '.$invoice_type_name.'"><i class="fa fa-trash"></i></a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no properties added";
		}
?>

<section class="panel">
	<header class="panel-heading">						
		<h2 class="panel-title"><?php echo $title;?> 
		<div class="pull-right" >
			<a  class="btn btn-sm btn-success pull-right" id="open_new_tenant" onclick="get_new_tenant();" style="margin-top:-5px">Add billing to <?php echo $title;?></a>
			<a  class="btn btn-sm btn-warning pull-right" id="close_new_tenant" style="display:none; margin-top:-5px;" onclick="close_new_tenant();">Close billing view</a>
			
		</div></h2>
	</header>
	<div class="panel-body">
    	<?php
        $success = $this->session->userdata('success_message');

		if(!empty($success))
		{
			echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
			$this->session->unset_userdata('success_message');
		}
		
		$error = $this->session->userdata('error_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
			$this->session->unset_userdata('error_message');
		}
		?>
		<div class="row">
			 <div style="display:none;" class="col-md-12" style="margin-bottom:20px;" id="new_tenant" >
		    	<section class="panel">
					<header class="panel-heading">
						<div class="panel-actions">
						</div>
						<h2 class="panel-title">Add a billing to property</h2>
					</header>
					<div class="panel-body">
						<div class="row" style="margin-bottom:20px;">
		        			<div class="col-lg-12 col-sm-12 col-md-12">
		        				<div class="row">
		        				<?php 
		        					echo form_open("add-biliing/".$property_id, array("class" => "form-horizontal", "role" => "form"));
		        				?>
		            				<div class="col-md-12">
		            					<div class="row">
		                					<div class="col-md-6">
		                    					<div class="form-group">
										            <label class="col-lg-5 control-label">Invoice Type: </label>
										            
										            <div class="col-lg-7">
										            	<select class="form-control" name="invoice_type_id" required>
										            		<option value="0">SELECT THE INVOICE TYPE</option>
										            		<?php echo $invoice_type_list?>
										            	</select>
										            </div>
										        </div>
										        <div class="form-group">
										            <label class="col-lg-5 control-label">Charge To: </label>
										            
										            <div class="col-lg-7">
										            	  <div class="radio">
									                            <label>
									                                <input id="optionsRadios1" type="radio" checked value="1" name="charge_to">
									                                Tenant
									                            </label>
									                        </div>
									                        <div class="radio">
									                            <label>
									                                <input id="optionsRadios2" type="radio" value="0" name="charge_to">
									                                Owner
									                            </label>
									                        </div>
										            </div>
										        </div>
										    </div>
										    <div class="col-md-6">
										    	<div class="form-group">
										            <label class="col-lg-5 control-label">Billing Schedule: </label>
										            
										            <div class="col-lg-7">
										            	<select class="form-control" name="billing_schedule_id" required>
										            		<option value="0">SELECT THE SCHEDULE</option>
										            		<?php echo $billing_schedule_list?>
										            	</select>
										            </div>
										        </div>
										        <div class="form-group">
										            <label class="col-lg-5 control-label">Amount To Charge: </label>
										            
										            <div class="col-lg-7">
										            	<input type="text" class="form-control" name="amount" placeholder="Amount" value="" >
										            </div>
										        </div>
										    </div>
										</div>
									    <div class="row" style="margin-top:10px;">
											<div class="col-md-12">
										        <div class="form-actions center-align">
										            <button class="submit btn btn-primary" type="submit">
										                SUBMIT
										            </button>
										        </div>
										    </div>
										</div>
		            				</div>
		            				<?php echo form_close();?>
		            				<!-- end of form -->
		            			</div>

		        				
		        			</div>
		        			
		        		</div>
					</div>
				</section>
		    </div>
		</div>
		
    	<div class="col-md-12" style="margin-bottom:20px;">
            <div class="table-responsive">
	        	
				<?php echo $result;?>
		
	        </div>
        </div>
		
	</div>
    <div class="panel-footer">
    	<?php if(isset($links)){echo $links;}?>
    </div>
</section>
<script type="text/javascript">
	function get_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant");
		var button = document.getElementById("open_new_tenant");
		var button2 = document.getElementById("close_new_tenant");

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant");
		var button = document.getElementById("open_new_tenant");
		var button2 = document.getElementById("close_new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}

</script>