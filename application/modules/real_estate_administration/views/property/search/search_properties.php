<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Search</h3>

      <div class="box-tools pull-right">
      </div>
    </div>
    <div class="box-body">
	<?php
    echo form_open("search-properties", array("class" => "form-horizontal"));
    ?>
    <div class="row">
     	<div class="col-md-5">            
            <div class="form-group">
                <label class="col-lg-4 control-label">LandLord: </label>
               							            
	            <div class="col-lg-8">	            	
                    <select class="form-control select2" name="property_owner_id">
                        <option value="">-- Select property Owner --</option>
                        <?php
                        if($property_owners->num_rows() > 0)
                        {
                            $property_owner = $property_owners->result();
                            
                            foreach($property_owner as $res)
                            {
                                $db_property_owner_id = $res->property_owner_id;
                                $property_owner_name = $res->property_owner_name;
                                echo '<option value="'.$db_property_owner_id.'">'.$property_owner_name.'</option>';                                
                            }
                        }
                    ?>
                    </select>
                                  
	            </div>
            </div>
        </div>
        <div class="col-md-5">            
            <div class="form-group">
                <label class="col-lg-4 control-label">Property: </label>
               							            
	            <div class="col-lg-8">
	            	 <select class="form-control select2" name="property_id">
                        <option value="">-- Select property --</option>
                        <?php
                        if($properties->num_rows() > 0)
                        {
                            $property = $properties->result();
                            
                            foreach($property as $res)
                            {
                                $db_property = $res->property_id;
                                $property_name = $res->property_name;
                                echo '<option value="'.$db_property.'">'.$property_name.'</option>';                                
                            }
                        }
                    ?>
                    </select>
	            </div>
            </div>
        </div>
        <div class="col-md-2">            
            <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                	<div class="center-align">
                   		<button type="submit" class="btn btn-info">Search</button>
    				</div>
                </div>
            </div>
        </div>    
    </div>

    
    
    <?php
    echo form_close();
    ?>
  </div>
</div>