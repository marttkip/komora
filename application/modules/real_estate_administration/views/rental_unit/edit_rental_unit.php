<?php
// var_dump($query);
foreach ($query->result() as $key) {
	# code...
	$property_id_one = $key->property_id;
	$rental_unit_id = $key->rental_unit_id;
	$rental_unit_name = $key->rental_unit_name;
	$rental_unit_number = $key->rental_unit_number;

}


$properties = $this->property_model->get_active_property();
$rs8 = $properties->result();
$property_list = '';
foreach ($rs8 as $property_rs) :
	$property_id = $property_rs->property_id;
	$property_name = $property_rs->property_name;
	$property_location = $property_rs->property_location;
	if($property_id == $property_id_one)
	{
		  $property_list .="<option value='".$property_id."' selected>".$property_name." Location: ".$property_location."</option>";
	}
	else
	{
		  $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";
	}


endforeach;
?>
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Edit Rental Unit</h3>

      <div class="box-tools pull-right">
         <a href="<?php echo site_url();?>property-manager/rental-units" class="btn btn-sm btn-info "><i class="fa fa-plus"></i> Back to rental units</a>
      </div>
    </div>
    <div class="box-body">
		<div class="row" style="margin-bottom:20px;">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="row">
				<?php echo form_open("edit-rental-unit/".$rental_unit_id, array("class" => "form-horizontal", "role" => "form"));?>
    				<div class="col-md-12">
    					<div class="row">
        					<div class="col-md-2">
            					<div class="form-group">
						            <label class="col-lg-5 control-label">Unit Name: </label>

						            <div class="col-lg-7">
						            	<input type="text" class="form-control" name="rental_unit_name" placeholder="Rental Unit Name" value="<?php echo $rental_unit_name?>">
						            </div>
						        </div>
						    </div>
						    <div class="col-md-6">
						    	<div class="form-group">
						            <label class="col-lg-5 control-label">Property Name: </label>

						            <div class="col-lg-5">
						            	<select id='property_id' name='property_id' class='form-control select2 '>
					                    <!-- <select class="form-control custom-select " id='procedure_id' name='procedure_id'> -->
					                      <option value=''>None - Please Select a property</option>
					                      <?php echo $property_list;?>
					                    </select>
						            </div>
						        </div>
						    </div>
								<div class="col-md-2">
										<div class="form-group">
											<label class="col-lg-5 control-label">Unit Number: </label>

											<div class="col-lg-7">
												<input type="text" class="form-control" name="rental_unit_number" placeholder="Rental Unit Number" value="<?php echo $rental_unit_number?>">
											</div>
									</div>
							</div>

						</div>
					    <div class="row" style="margin-top:10px;">
							<div class="col-md-12">
						        <div class="form-actions text-center">
						            <button class="submit btn btn-primary" type="submit">
						                Edit rental unit
						            </button>
						        </div>
						    </div>
						</div>
    				</div>
    				<?php echo form_close();?>
    				<!-- end of form -->
    			</div>


			</div>

		</div>
	</div>
</div>

<?php
$rental_unit_prices = $this->rental_unit_model->get_rental_unit_prices($rental_unit_id);
// var_dump($rental_unit_prices); die();
$result = '	<table class="table table-bordered table-striped table-condensed">
								<thead>
									<tr>
										<th>#</th>
										<th><a>Previous</a></th>
										<th><a>Current</a></th>
										<th><a>Tax</a></th>
										<th><a>Total Amount</a></th>
										<th><a>Taxed</a></th>
										<th><a>Start Date</a></th>
									</tr>
								</thead>
									<tbody>';

if($rental_unit_prices->num_rows() > 0)
{
	$count = 0;
	 foreach ($rental_unit_prices->result() as $key => $value) {
	 	// code...
			$previous = $value->previous;
			$current = $value->current;
			$end_date = $value->enddate;
			$taxed= $value->taxed;
			$tax_amount= $value->tax_amount;

			if($taxed == 1)
			{
				$taxed = ' Taxed';
			}
			else {
				$taxed = 'Not Taxed';
			}
			$count++;
			$result .= '<tr>
											<td>'.$count.'</td>
											<td>'.$previous.'</td>
											<td>'.number_format($current-$tax_amount).'</td>
											<td>'.$tax_amount.'</td>
											<td>'.$current.'</td>
											<td>'.$taxed.'</td>
											<td>'.$end_date.'</td>
									</tr>';

	 }
}
$result .=
'
				</tbody>
			</table>
';
?>
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Rent Prices</h3>
      <div class="box-tools pull-right">
      </div>
    <div class="box-body">
		<div class="row" style="margin-bottom:20px;">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="row">
				<?php echo form_open("change-rental-unit-price/".$rental_unit_id, array("class" => "form-horizontal", "role" => "form"));?>
    				<div class="col-md-12">
    					<div class="row">
        					<div class="col-md-4">
            					<div class="form-group">
						            <label class="col-lg-5 control-label">Amount: </label>

						            <div class="col-lg-7">
						            	<input type="text" class="form-control" name="rent_amount" placeholder="Current Rent Price" value="">
						            </div>
						        </div>
						    </div>
						    <div class="col-md-4">
						    	<div class="form-group">
						            <label class="col-lg-5 control-label">Start Date: </label>

						            <div class="col-lg-7">
													<div class="input-group">
															<span class="input-group-addon">
																	<i class="fa fa-calendar"></i>
															</span>
															<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="end_date" value="" placeholder=" Date To" id="datepicker">
													</div>
						            </div>
						        </div>
						    </div>
								<div class="col-md-4">
						    	<div class="form-group">
						            <label class="col-lg-5 control-label">Tax 16% : </label>

						            <div class="col-lg-7">
													<div class="radio">
															<label>
																	<input  type="radio" checked value="0" name="taxed" id="account_to_type" >
																	No
															</label>
															<label>
																	<input  type="radio"  value="1" name="taxed" id="account_to_type" >
																	Yes
															</label>
													</div>
						            </div>
						        </div>
						    </div>
						</div>

					    <div class="row" style="margin-top:10px;">
							<div class="col-md-12">
						        <div class="form-actions text-center">
						            <button class="submit btn btn-primary" type="submit">
						                Update Rental Charge
						            </button>
						        </div>
						    </div>
						</div>
    				</div>
    				<?php echo form_close();?>
    				<!-- end of form -->
    			</div>
					<br>
					<div class="row">
						<?php echo $result;?>
					</div>

			</div>

		</div>
	</div>
</div>
