
<?php
		
		$result = '';
		
		//if users exist display them


		
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Rental Unit Name</th>
						<th>Status</th>
						<th>Tenancy Status</th>
					</tr>
				</thead>
				<tbody>
				  
			';
			

			
			foreach ($query->result() as $row)
			{
				$rental_unit_id = $row->rental_unit_id;
				$rental_unit_name = $row->rental_unit_name;
				$rental_unit_price = $row->rental_unit_price;
				$created = $row->created;
				$rental_unit_status = $row->rental_unit_status;
				
				//status
				if($rental_unit_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				// get tenancy status 

				
				$tenancy_status = '<span class="label label-warning"> Vacant </span>';
					
			
			
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$status.'</td>
						<td>'.$tenancy_status.'</td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no vacant rental units";
		}
?>
						 <div class="box">
			                <div class="box-header with-border">
			                  <h3 class="box-title"><?php echo $title;?></h3>

			                  <div class="box-tools pull-right">
			                    
			                  </div>
			                </div>
			                <div class="box-body">
						
                            	<?php
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								$search =  $this->session->userdata('all_rental_unit_search');
								if(!empty($search))
								{
									echo '<a href="'.site_url().'close_search_rental_units" class="btn btn-sm btn-warning">Close Search</a>';
								}
								?>
                            	
								
                               		<div class="table-responsive">
											<?php echo $result;?>
                                		</div>
                                    <div class="panel-footer">
		                            	<?php if(isset($links)){echo $links;}?>
		                            </div>
                               
							</div>
                           
						</div>
<script type="text/javascript">
	$(function() {
	    $("#property_id").customselect();
	});
	$(document).ready(function(){
		$(function() {
			$("#property_id").customselect();
		});
	});
	function get_new_rental_unit(){

		var myTarget2 = document.getElementById("new_rental_unit");
		var button = document.getElementById("open_new_rental_unit");
		var button2 = document.getElementById("close_new_rental_unit");

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_new_rental_unit(){

		var myTarget2 = document.getElementById("new_rental_unit");
		var button = document.getElementById("open_new_rental_unit");
		var button2 = document.getElementById("close_new_rental_unit");

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
</script>