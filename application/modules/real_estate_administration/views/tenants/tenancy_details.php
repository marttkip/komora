<?php //echo $this->load->view('search/search_terminated_leases','', true); ?>
<?php

$result = '';

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;

	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Lease Number</th>
				<th>Unit</th>
				<th>Tenant Name</th>
				<th>Lease Start Date</th>
				<th>Duration</th>
        <th>Notice Date</th>
        <th>Vacated Date</th>
				<th>Lease Status</th>
				<th>Actions</th>
			</tr>
		</thead>
		  <tbody>

	';


	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_end_date = $leases_row->lease_end_date;

		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
    $vacated_on = $leases_row->vacated_on;
    $notice_date = $leases_row->notice_date;
    $lease_status = $leases_row->lease_status;
		$created = $leases_row->created;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));

		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime($lease_end_date));


    if($vacated_on != "0000-00-00" AND !empty($vacated_on))
    {
      $vacated_on = date('jS M Y',strtotime($vacated_on));
    }
    else {
      $vacated_on = '-';
    }


    if($notice_date != "0000-00-00" AND !empty($notice_date))
    {
      $notice_date = date('jS M Y',strtotime($notice_date));
    }
    else {
      $notice_date = '-';
    }



		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-info"> Inactive Lease</span>';
      $highlight = 'info';

			$button = '';
			$delete_button = '<td><a href="'.site_url().'delete-lease/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to cancel lease?\');" title="Cancel Lease"><i class="fa fa-trash"></i></a></td>';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '';
      $highlight = 'success';

		}
		else if($lease_status == 2)
		{
			$status = '<span class="label label-danger">Terminated Lease</span>';
			$button = '';
			$delete_button = '';
      $highlight = 'danger';
		}

		else if($lease_status == 3)
		{
			$status = '<span class="label label-warning">Lease on Notice</span>';
			$button = '';
			$delete_button = '';
      $highlight = 'warning';
		}
		else if($lease_status == 4)
		{
			$status = '<span class="label label-danger">Terminated Lease</span>';
			$button = '';
			$delete_button = '';
      $highlight = 'danger';
		}

		if(empty($lease_number))
		{
			$lease_start_date ='';
			$expiry_date = '';
			$status = '<span class="label label-warning">Inactive</span>';
      $highlight = 'info';
		}

		if(empty($module))
		{
			$charged = '<a href="'.site_url().'lease-detail/'.$lease_id.'"  class="btn btn-sm btn-danger">View</a>';
		}
		else {
			$charged =  '';
		}

		$count++;
		$result .=
		'
			<tr>
				<td class="'.$highlight.'">'.$count.'</td>
				<td class="'.$highlight.'">'.$lease_number.'</td>
				<td class="'.$highlight.'">'.$property_name.' '.$rental_unit_name.'</td>
				<td class="'.$highlight.'">'.$tenant_name.'</td>
				<td class="'.$highlight.'">'.$lease_start_date.'</td>
				<td>'.$lease_duration.'</td>
        <td>'.$notice_date.'</td>
        <td>'.$vacated_on.'</td>
				<td>'.$status.'</td>
				<td>
				'.$charged.'
        <a href="'.site_url().'print-tenant-statement/'.$lease_id.'/'.date('Y').'" target="_blank" class="btn btn-sm btn-success"> Statement</a></td>
			</tr>
		';

	}

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases due";
}
?>
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $title;?></h3>

      <div class="box-tools pull-right">
        <!-- <a href="<?php echo site_url().'print-tenant-statement/'.$tenant_id;?>" class="btn btn-success btn-sm" ><i class="fa fa-folder"></i> Full Tenant Statement</a> -->
				<?php
				if(empty($module))
				{
					?>
					<a href="<?php echo site_url().'property-manager/tenants'?>" class="btn btn-warning btn-sm" ><i class="fa fa-arrow-left"></i> Back to tenants</a>
					<?php

				}
				else {
					?>
					<a href="<?php echo site_url().'accounts/tenants-accounts'?>" class="btn btn-warning btn-sm" ><i class="fa fa-arrow-left"></i> Back to tenants</a>
					<?php
				}
				?>

      </div>
    </div>
    <div class="box-body">
    	<?php
        $success = $this->session->userdata('success_message');

  			if(!empty($success))
  			{
  				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
  				$this->session->unset_userdata('success_message');
  			}

  			$error = $this->session->userdata('error_message');

  			if(!empty($error))
  			{
  				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
  				$this->session->unset_userdata('error_message');
  			}


  			$search =  $this->session->userdata('all_terminated_leases_search');
  			if(!empty($search))
  			{
  				echo '<a href="'.site_url().'close-terminated-lease-search" class="btn btn-sm btn-warning">Close Search</a>';
  			}
	    ?>
			<div class="table-responsive">
				<?php echo $result;?>
      </div>
      <div class="panel-footer">
	        	<?php if(isset($links)){echo $links;}?>
	    </div>
		</div>

	</div>

	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>
  <script type="text/javascript">
	// $(function() {
	//     $("#tenant_id").customselect();
	//     $("#rental_unit_id").customselect();
	// });
	// $(document).ready(function(){
	// 	$(function() {
	// 		$("#rental_unit_id").customselect();
	// 		$("#tenant_id").customselect();
	// 	});
	// });

	function get_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant");
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var button = document.getElementById("open_new_tenant");
		var button2 = document.getElementById("close_new_tenant");

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		button2.style.display = '';
	}
	function close_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant");
		var button = document.getElementById("open_new_tenant");
		var button2 = document.getElementById("close_new_tenant");
		var myTarget3 = document.getElementById("new_tenant_allocation");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		button2.style.display = 'none';
	}


	function assign_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant_allocation");
		var myTarget3 = document.getElementById("new_tenant");
		var button2 = document.getElementById("close_assign_new_tenant");
		var button = document.getElementById("assign_new_tenant");

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = 'block';
		myTarget3.style.display = 'none';
	}
	function close_assign_new_tenant(){

		var myTarget2 = document.getElementById("new_tenant_allocation");
		var button = document.getElementById("assign_new_tenant");
		var button2 = document.getElementById("close_assign_new_tenant");
		var myTarget3 = document.getElementById("new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
		myTarget3.style.display = 'none';
	}


	// lease details


	function get_tenant_leases(tenant_id){

		var myTarget2 = document.getElementById("lease_details"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");
		var button = document.getElementById("open_lease_details"+tenant_id);
		var button2 = document.getElementById("close_lease_details"+tenant_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = '';
	}
	function close_tenant_leases(tenant_id){

		var myTarget2 = document.getElementById("lease_details"+tenant_id);
		var button = document.getElementById("open_lease_details"+tenant_id);
		var button2 = document.getElementById("close_lease_details"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = 'none';
	}

	// tenant_info
	function get_tenant_info(tenant_id){

		var myTarget2 = document.getElementById("tenant_info"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");
		var button = document.getElementById("open_tenant_info"+tenant_id);
		var button2 = document.getElementById("close_tenant_info"+tenant_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = '';
	}
	function close_tenant_info(tenant_id){

		var myTarget2 = document.getElementById("tenant_info"+tenant_id);
		var button = document.getElementById("open_tenant_info"+tenant_id);
		var button2 = document.getElementById("close_tenant_info"+tenant_id);
		var myTarget3 = document.getElementById("new_tenant_allocation");
		var myTarget4 = document.getElementById("new_tenant");

		myTarget2.style.display = 'none';
		button.style.display = '';
		myTarget3.style.display = 'none';
		myTarget4.style.display = 'none';
		button2.style.display = 'none';
	}

  </script>
