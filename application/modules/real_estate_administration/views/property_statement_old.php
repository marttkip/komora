<?php

$result = '';
$total_invoice_balance =0;
$total_balance_bf = 0;
$total_rent_payable = 0;

$this_year_item = $this->session->userdata('year');
$this_month_item = $this->session->userdata('month');
$total_invoice_balance_end =0;
$total_payable_end =0;
$total_arreas_end = 0;
$total_rent_end = 0;
		
//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;
	
	$result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>HOUSE NUMBER</th>
				<th>TENANT NAME</th>
				<th>RENT PAYABLE</th>
				<th>ARREARS B/F</th>
				<th>RENT PAID</th>
				<th>ARREARS</th>
			</tr>
		</thead>
		  <tbody>
		  
	';
	
	
	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$rental_unit_id = $leases_row->rental_unit_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$property_name = $leases_row->property_name;
		$rent_payable = $leases_row->rent_amount;
		$property_id = $leases_row->property_id;
		$tenant_name = $leases_row->tenant_name;
		$tenant_email = $leases_row->tenant_email;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$points = 0;//$leases_row->points;
		$created = $leases_row->created;

	
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		

		//create deactivated status display
		if($lease_status == 0)
		{
			$status = '<span class="label label-default"> Deactivated</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($lease_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}

			$lease_start_date = date('jS M Y',strtotime($lease_start_date));
	     	
	     	$this_month = $this->session->userdata('month');
	     	$amount_paid = 0;
		     	$payments = $this->accounts_model->get_this_months_payment($lease_id,$this_month);
		     	$current_items = '
		     			  <td>-</td>'; 
		     	$total_paid_amount = 0;
		     	if($payments->num_rows() > 0)
		     	{
		     		$counter = 0;
		     		
		     		$receipt_counter = '';
		     		foreach ($payments->result() as $value) {
		     			# code...
		     			$receipt_number = $value->receipt_number;
		     			$amount_paid = $value->amount_paid;

		     			if($counter > 0)
		     			{
		     				$addition = '#';
		     				// $receipt_counter .= $receipt_number.$addition;
		     			}
		     			else
		     			{
		     				$addition = ' ';
		     				
		     			}
		     			$receipt_counter .= $receipt_number.$addition;

		     			$total_paid_amount = $total_paid_amount + $amount_paid;
		     			
		     		}
		     		$current_items = '
		     					<td>'.number_format($amount_paid,0).'</td>';
		     	}
		     	else
		     	{
		     		$current_items = '
		     					<td>-</td>';
		     	}
				
				$todays_date = date('Y-m-d');
				$todays_month = $this->session->userdata('month');
				$todays_year = $this->session->userdata('year');
				// $todays_month = date('m'); 
				// $todays_year = date('Y'); 

				// 11

				//  get all bills for this month

				$bills = $this->accounts_model->get_all_invoice_current_month($lease_id,$todays_month,$todays_year);

				// get all the payments done for this month
				$payments = $this->accounts_model->get_all_payments_current_lease($lease_id ,$todays_month,$todays_year);


				// $x=0;

				$bills_result = '';
				$last_date = '';
				$current_year = date('Y');
				$total_invoices = $bills->num_rows();
				$invoices_count = 0;
				$total_invoice_balance = 0;
				$total_arrears = 0;
				$total_payment_amount = 0;
				if($bills->num_rows() > 0)
				{
					foreach ($bills->result() as $key_bills) {
						# code...
						$invoice_month = $key_bills->invoice_month;
					    $invoice_year = $key_bills->invoice_year;
						$invoice_date = $key_bills->invoice_date;
						$invoice_amount = $key_bills->total_invoice;
						$invoices_count++;
						if($payments->num_rows() > 0)
						{
							foreach ($payments->result() as $payments_key) {
								# code...
								$payment_date = $payments_key->payment_date;
								$payment_year = $payments_key->year;
								$payment_month = $payments_key->month;
								$payment_amount = $payments_key->amount_paid;

								if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
								{
									$total_arrears -= $payment_amount;
									if($payment_year >= $current_year)
									{
										
									}
									
									$total_payment_amount += $payment_amount;

								}
							}
						}
						//display disbursment if cheque amount > 0
						if($invoice_amount != 0)
						{
							$total_arrears += $invoice_amount;
							$total_invoice_balance += $invoice_amount;
								
							if($invoice_year >= $current_year)
							{
								
							}
						}
								
						//check if there are any more payments
						if($total_invoices == $invoices_count)
						{
							//get all loan deductions before date
							if($payments->num_rows() > 0)
							{
								foreach ($payments->result() as $payments_key) {
									# code...
									$payment_date = $payments_key->payment_date;
									$payment_year = $payments_key->year;
									$payment_month = $payments_key->month;
									$payment_amount = $payments_key->amount_paid;

									if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
									{
										$total_arrears -= $payment_amount;
										if($payment_year >= $current_year)
										{
											
										}
										
										$total_payment_amount += $payment_amount;

									}
								}
							}
						}
								$last_date = $invoice_date;
					}
				}	
				else
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;



							if(($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								if($payment_year >= $current_year)
								{
									
								}
								
								$total_payment_amount += $payment_amount;

							}
						}
					}
				}




				
				// $total_payments = $this->accounts_model->get_total_payments_before($lease_id,$todays_year,$todays_month);
				
				// $total_invoices = $this->accounts_model->get_total_invoices_before($lease_id,$todays_year,$todays_month);
				// // var_dump($total_invoices); die();
				// $current_balance = $total_invoices - $total_payments;

				$current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));


				// $total_current_invoice = $current_invoice_amount + $current_balance - $total_paid_amount;

				$count++;
				//check if email for the current_month has been sent for that rental unit
				// $all_sms_notifications_sent = $this->accounts_model->check_if_sms_sent($rental_unit_id,$tenant_phone_number,$todays_year,$todays_month,1);
				// $all_email_notifications_sent = $this->accounts_model->check_if_email_sent($rental_unit_id,$tenant_email,$todays_year,$todays_month,1);
				// if(($all_sms_notifications_sent==FALSE) &&($all_email_notifications_sent==FALSE))
				// {
				// 	$send_notification_button = '<td><a class="btn btn-sm btn-default" href="'.site_url().'send-arrears/'.$lease_id.'"> Send Message</a></td>';	
				// }
				// else
				// {
				// 	$send_notification_button = '-';
				// }
				$total_balance = $total_arrears + $current_invoice_amount - $total_paid_amount;

				if($lease_status == 0)
				{
					$tenant_name = 'Vacant';
					$total_invoice_balance = 0;
					$current_invoice_amount = 0;
					$total_paid_amount = 0;
					$rent_payable = 0;
					$total_balance = 0;
					
				}


				$parent_invoice_date = $todays_year.'-'.$todays_month.'-03';

				// $fixed_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date);
				// $fixed_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date);

				// $total_brought_forward = $fixed_invoice_amount - $fixed_paid_amount;
				
				// $fixed_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date);
				// $fixed_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date);
			
				// $total_fixed = $fixed_current_invoice - $fixed_current_payment;


				// $account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date);
				// $account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date);
				// $account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date);
				// $total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment - $total_pardon_amount;

				// // $tenants_response = $this->accounts_model->get_tenants_billings($lease_id);
				// // $total_pardon_amount = $tenants_response['total_pardon_amount'];

				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$tenant_name.'</td>

						<td>'.number_format($rent_payable,2).'</td>
						<td>'.number_format($total_invoice_balance,2).'</td>
						'.$current_items.'
						<td>'.number_format($total_balance,0).'</td>
					</tr> 
				';
				// add all the balances


				$total_invoice_balance_end = $total_invoice_balance_end + $total_balance;
				$total_arreas_end = $total_arreas_end + $total_invoice_balance;
				$total_payable_end = $total_payable_end + $total_paid_amount;
				
				$total_rent_end = $total_rent_end + $rent_payable;
		
		
	}


	$result .='<tr> 
					<td></td>
					<td></td>
					<td><strong>Totals</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_rent_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_arreas_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_payable_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_invoice_balance_end,2).'</strong></td>
				</tr>';
	
	$result .= 
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no items for this month";
}


$property_id = $this->session->userdata('property_id');

//  GET ALL OUT FLOWS OF THE MONTH 
$outflows = $this->reports_model->get_month_outflows($property_id,$this_month_item,$this_year_item);
$total_outflows = 0;
if($outflows->num_rows() > 0)
{
	$outflow = '';

	foreach ($outflows->result() as $key) {
		# code...

		$creditor_service_name = $key->creditor_service_name;
		$creditor_account_amount = $key->creditor_account_amount;
		$creditor_name = $key->creditor_name;

		$outflow .='<li>'.$creditor_name.' : '.$creditor_service_name.' - <strong> KES. '.number_format($creditor_account_amount,2).'</strong></li>';
		$total_outflows = $total_outflows + $creditor_account_amount;
	}
	$outflow .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows ,2).' </strong> </li>';
}else
{
	$outflow ='<li>No expense added</li>';
}

?>  
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  font-size: 11px !important;
			  margin-bottom: 15px;
			  max-width: 100%;
			  width: 100%;

			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
			  padding: 2px;
			}
			h3, .h3 {
			  font-size: 18px !important;
			}
		</style>
    </head>
     <body class="receipt_spacing">
        <div class="row">
            <div class="col-xs-12">
                <div class="receipt_bottom_border">
                    <table class="table table-condensed">
                        <tr>
                            <th><h2><?php echo $contacts['company_name'];?> </h2></th>
                            <th class="align-right">
                                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
            	<tr>
                    <th class="align-center"><h3><?php echo $title;?> </h3></th>
                </tr>
                <?php echo $result;?>
            </div>
        </div>
        <div class="row" >
       		<div class="col-xs-12" style="border-top:1px #000 solid;">
       			<h4>Key Areas to Note:</h4>
       			<ul>
       				<?php echo $outflow;?>

       			</ul>
       		</div>
       	</div>
        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
     </body> 
    </html>
		
