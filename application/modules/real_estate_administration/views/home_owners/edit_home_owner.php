<?php
foreach ($query->result() as $key) {
	$home_owner_id = $key->home_owner_id;
	$home_owner_national_id = $key->home_owner_national_id;	
	$home_owner_name = $key->home_owner_name;
	$home_owner_phone_number = $key->home_owner_phone_number;
	$home_owner_email = $key->home_owner_email;
}
?>
<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
			</div>
			<h2 class="panel-title">Edit <?php echo $home_owner_name;?></h2>
		</header>
		<div class="panel-body">
			<div class="row" style="margin-bottom:20px;">
    			<div class="col-lg-12 col-sm-12 col-md-12">
    				<div class="row">
    				<?php echo form_open("edit-home-owner/".$home_owner_id, array("class" => "form-horizontal", "role" => "form"));?>
        				<div class="col-md-12">
        					<div class="row">
            					<div class="col-md-6">
                					<div class="form-group">
							            <label class="col-lg-5 control-label">Home owner Name: </label>
							            
							            <div class="col-lg-7">
							            	<input type="text" class="form-control" name="home_owner_name" placeholder="Name" value="<?php echo $home_owner_name;?>">
							            </div>
							        </div>
							        <div class="form-group">
							            <label class="col-lg-5 control-label">National id: </label>
							            
							            <div class="col-lg-7">
							            	<input type="text" class="form-control" name="home_owner_national_id" placeholder="National ID" value="<?php echo $home_owner_national_id;?>">
							            </div>
							        </div>
							    </div>
							    <div class="col-md-6">
							    	<div class="form-group">
							            <label class="col-lg-5 control-label">Phone number: </label>
							            
							            <div class="col-lg-7">
							            	<input type="text" class="form-control" name="home_owner_phone_number" placeholder="Phone" value="<?php echo $home_owner_phone_number;?>">
							            </div>
							        </div>
							        <div class="form-group">
							            <label class="col-lg-5 control-label">Email address: </label>
							            
							            <div class="col-lg-7">
							            	<input type="text" class="form-control" name="home_owner_email" placeholder="Email address" value="<?php echo $home_owner_email;?>">
							            </div>
							        </div>
							    </div>
							</div>
						    <div class="row" style="margin-top:10px;">
								<div class="col-md-12">
							        <div class="form-actions center-align">
							            <button class="submit btn btn-primary" type="submit">
							                Edit home owner
							            </button>
							        </div>
							    </div>
							</div>
        				</div>
        				<?php echo form_close();?>
        				<!-- end of form -->
        			</div>

    				
    			</div>
    			
    		</div>
		</div>
	</section>