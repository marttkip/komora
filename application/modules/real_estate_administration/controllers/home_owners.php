<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/real_estate_administration/controllers/property.php";

class Home_owners extends property {
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('home_owners_model');
		$this->load->model('rental_unit_model');
		$this->load->model('leases_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
	}
    
	/*
	*
	*	Default action is to show all the home_owners
	*
	*/
	public function index($rental_unit_id = NULL,$pager = NULL) 
	{
		
		if($rental_unit_id == NULL || $rental_unit_id == 0)
		{
			$where = 'home_owner_id > 0';
			$table = 'home_owners';
			$addition = '/'.$rental_unit_id.'/'.$pager;
			$segment = 5;
		}
		else
		{
			if($pager == NULL)
			{
				$addition = '/'.$rental_unit_id;
				$segment = 4;
			}
			else
			{
				$addition = '/'.$rental_unit_id.'/'.$pager;
				$segment = 5;
			}
			
			 $where = 'home_owners.home_owner_id > 0 AND home_owners.home_owner_id = home_owner_unit.home_owner_id AND home_owner_unit.rental_unit_id = rental_unit.rental_unit_id  AND home_owner_unit.rental_unit_id ='.$rental_unit_id;
			$table = 'home_owners,home_owner_unit,rental_unit';
		}
		
		$home_owners_search = $this->session->userdata('all_home_owners_search');
		
		if(!empty($home_owners_search))
		{
			$where .= $home_owners_search;	
			
		}
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'home_owners'.$addition;
		$config['total_rows'] = $this->home_owners_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 2;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->home_owners_model->get_all_home_owners($table, $where, $config["per_page"], $page, $order=NULL, $order_method=NULL);

		$home_owner_order = 'home_owners.home_owner_name';
		$home_owner_table = 'home_owners';
		$home_owner_where = 'home_owners.home_owner_status = 1';

		$home_owner_query = $this->home_owners_model->get_home_owner_list($home_owner_table, $home_owner_where, $home_owner_order);
		$rs8 = $home_owner_query->result();
		$home_owners_list = '';
		foreach ($rs8 as $home_owner_rs) :
			$home_owner_id = $home_owner_rs->home_owner_id;
			$home_owner_name = $home_owner_rs->home_owner_name;

			$home_owner_national_id = $home_owner_rs->home_owner_national_id;
			$home_owner_phone_number = $home_owner_rs->home_owner_phone_number;

		    $home_owners_list .="<option value='".$home_owner_id."'>".$home_owner_name." Phone: ".$home_owner_phone_number."</option>";

		endforeach;

		$v_data['home_owners_list'] = $home_owners_list;
		$v_data['pager'] = $pager;

		if($rental_unit_id != NULL AND $rental_unit_id != 0)
		{
			$rental_unit_name = $this->rental_unit_model->get_rental_unit_name($rental_unit_id);
			$data['title'] = $rental_unit_name.'\'s home_owners';
			$v_data['title'] = $data['title'];
		}
		else
		{
			$data['title'] = 'All home_owners';
			$v_data['title'] = $data['title'];
		}		
		
		$v_data['rental_unit_id'] = $rental_unit_id;
		$v_data['home_owners'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('home_owners/all_home_owners', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function all_home_owners()
	{

			
		$where = 'home_owner_id > 0';
		$table = 'home_owners';
	
		$segment = 3;
		$home_owners_search = $this->session->userdata('all_home_owners_search');
		
		if(!empty($home_owners_search))
		{
			$where .= $home_owners_search;	
			
		}
		//pagination
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'rental-management/home-owners';
		$config['total_rows'] = $this->home_owners_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->home_owners_model->get_all_home_owners($table, $where, $config["per_page"], $page, $order=NULL, $order_method=NULL);

		$home_owner_order = 'home_owners.home_owner_name';
		$home_owner_table = 'home_owners';
		$home_owner_where = 'home_owners.home_owner_status = 1';

		$home_owner_query = $this->home_owners_model->get_home_owner_list($home_owner_table, $home_owner_where, $home_owner_order);
		$rs8 = $home_owner_query->result();
		$home_owners_list = '';
		foreach ($rs8 as $home_owner_rs) :
			$home_owner_id = $home_owner_rs->home_owner_id;
			$home_owner_name = $home_owner_rs->home_owner_name;

			$home_owner_national_id = $home_owner_rs->home_owner_national_id;
			$home_owner_phone_number = $home_owner_rs->home_owner_phone_number;

		    $home_owners_list .="<option value='".$home_owner_id."'>".$home_owner_name." Phone: ".$home_owner_phone_number."</option>";

		endforeach;

		$v_data['home_owners_list'] = $home_owners_list;

		
			$data['title'] = 'All home_owners';
			$v_data['title'] = $data['title'];
			
		
		$v_data['rental_unit_id'] = NULL;
		$v_data['home_owners'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('home_owners/all_home_owners', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_home_owners()
	{
		$home_owner_name = $this->input->post('home_owner_name');
		$home_owner_phone_number = $this->input->post('home_owner_phone_number');
		$home_owner_national_id = $this->input->post('home_owner_national_id');
		
		$search_title = 'Showing reports for: ';
		
		
		if(!empty($home_owner_name))
		{
			$home_owner_name = ' AND home_owners.home_owner_name LIKE \'%'.$home_owner_name.'%\'';
		}
		else
		{
			$home_owner_name = '';
		}

		if(!empty($home_owner_phone_number))
		{
			$home_owner_phone_number = ' AND home_owners.home_owner_phone_number LIKE \'%'.$home_owner_phone_number.'%\'';
		}
		else
		{
			$home_owner_phone_number = '';
		}

		if(!empty($home_owner_national_id))
		{
			$home_owner_national_id = ' AND home_owners.home_owner_national_id LIKE \'%'.$home_owner_national_id.'%\'';
		}
		else
		{
			$home_owner_national_id = '';
		}

		$search = $home_owner_name.$home_owner_national_id.$home_owner_phone_number;

		$home_owners_search = $this->session->userdata('all_home_owners_search');
		
		
		$this->session->set_userdata('all_home_owners_search', $search);
		$this->all_home_owners();
	}
	public function close_home_owners_search()
	{
		$this->session->unset_userdata('all_home_owners_search');
		$this->session->unset_userdata('search_title');
		
		
		redirect('rental-management/home-owners');
	}
    
	/*
	*
	*	Add a new home_owner page
	*
	*/
	public function add_home_owner($rental_unit_id = NULL) 
	{
		//form validation rules
		// $this->form_validation->set_rules('home_owner_email', 'Email', 'xss_clean|is_unique[home_owners.home_owner_email]|valid_email');
		$this->form_validation->set_rules('home_owner_name', 'home_owner name', 'required|xss_clean|is_unique[home_owners.home_owner_name]');
		// $this->form_validation->set_rules('home_owner_phone_number', 'home_owner Phone Number', 'required|xss_clean|is_unique[home_owners.home_owner_phone_number]');
		$this->form_validation->set_rules('home_owner_national_id', 'National Id', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if home_owner has valid login credentials
			if($this->home_owners_model->add_home_owner())
			{
				$this->session->unset_userdata('home_owners_error_message');
				$this->session->set_userdata('success_message', 'home_owner has been successfully added');

				if($rental_unit_id == NULL OR $rental_unit_id == 0)
				{
					redirect('rental-management/home-owners');
				}
				else
				{
					redirect('rental-management/home-owners/'.$rental_unit_id);
				}

				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');
				if($rental_unit_id == NULL OR $rental_unit_id == 0)
				{
					redirect('rental-management/home-owners');
				}
				else
				{
					redirect('rental-management/home-owners/'.$rental_unit_id);
				}

			}
		}
		
		//open the add new home_owner page
	}
	public function allocate_home_owner_to_unit($rental_unit_id)
	{
		$this->form_validation->set_rules('home_owner_id', 'Home owner id', 'required|trim|xss_clean');

		if ($this->form_validation->run())
		{	
			if($this->home_owners_model->add_home_owner_to_unit($rental_unit_id))
			{
				$this->session->unset_userdata('lease_error_message');
				$this->session->set_userdata('success_message', 'home_owner has been added successfully');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Sorry please check for errors has been added successfully');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please fill in all the fields');
		}
		redirect('home-owners/'.$rental_unit_id);	
	}
	public function allocate_home_owner_to_unit_other($rental_unit_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('home_owner_id', 'home_owner name', 'required|xss_clean');
		$this->form_validation->set_rules('rental_unit_id', 'Rental Unit', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			// check if another home_owner account is active
			$rental_unit_id = $this->input->post('rental_unit_id');
			$checker = $this->home_owners_model->check_for_account($rental_unit_id);

			if($checker = TRUE)
			{
				$this->session->set_userdata('error_message', 'Seems Like another lease is still active, Please close the active lease before proceeding');
				redirect('rents/home_owners/'.$rental_unit_id);
			}
			else
			{
				//check if home_owner has valid login credentials
				if($this->home_owners_model->allocate_home_owner_to_rental_unit())
				{
					$this->session->unset_userdata('home_owners_error_message');
					$this->session->set_userdata('success_message', 'home_owner has been successfully added');
					redirect('rents/home_owners/'.$rental_unit_id);
				}
				
				else
				{
					$this->session->set_userdata('error_message', 'Sorry something went wrong. Please try again');
					redirect('rents/home_owners/'.$rental_unit_id);

				}
			}
				
		}
		
		//open the add new home_owner page
	}
    
	/*
	*
	*	Edit an existing home_owner page
	*	@param int $home_owner_id
	*
	*/
	public function edit_home_owner($home_owner_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('home_owner_email', 'Email', 'xss_clean');
		$this->form_validation->set_rules('home_owner_name', 'home_owner name', 'required|xss_clean');
		$this->form_validation->set_rules('home_owner_phone_number', 'home_owner Phone Number', 'required|xss_clean|exists[home_owners.home_owner_phone_number]');
		$this->form_validation->set_rules('home_owner_national_id', 'National Id', 'xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//check if home_owner has valid login credentials
			if($this->home_owners_model->edit_home_owner($home_owner_id))
			{
				$this->session->set_userdata('success_message', 'home_owner edited successfully');
				
				
			}
			
			else
			{
				$data['error'] = 'Unable to add home_owner. Please try again';
			}
			redirect('rental-management/home-owners');
		}
		
		//open the add new home_owner page
		$data['title'] = 'Edit home_owner';
		$v_data['title'] = $data['title'];
		
		//select the home_owner from the database
		$query = $this->home_owners_model->get_home_owner($home_owner_id);
		if ($query->num_rows() > 0)
		{
			$v_data['query'] = $query;
			$data['content'] = $this->load->view('home_owners/edit_home_owner', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'home_owner does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing home_owner page
	*	@param int $home_owner_id
	*
	*/
	public function delete_home_owner($home_owner_id) 
	{
		if($this->home_owners_model->delete_home_owner($home_owner_id))
		{
			$this->session->set_userdata('success_message', 'Administrator has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Administrator could not be deleted');
		}
		
		redirect('rental-management/home-owners');
	}
    
	/*
	*
	*	Activate an existing home_owner page
	*	@param int $home_owner_id
	*
	*/
	public function activate_home_owner($home_owner_id) 
	{
		if($this->home_owners_model->activate_home_owner($home_owner_id))
		{
			$this->session->set_userdata('success_message', 'Administrator has been activated');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Administrator could not be activated');
		}
		
		redirect('rental-management/home-owners');
	}
    
	/*
	*
	*	Deactivate an existing home_owner page
	*	@param int $home_owner_id
	*
	*/
	public function deactivate_home_owner($home_owner_id) 
	{
		if($this->home_owners_model->deactivate_home_owner($home_owner_id))
		{
			$this->session->set_userdata('success_message', 'Administrator has been disabled');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Administrator could not be disabled');
		}
		
		redirect('rental-management/home-owners');
	}
	
	/*
	*
	*	Reset a home_owner's password
	*	@param int $home_owner_id
	*
	*/
	public function reset_password($home_owner_id)
	{
		$new_password = $this->login_model->reset_password($home_owner_id);
		$this->session->set_userdata('success_message', 'New password is <br/><strong>'.$new_password.'</strong>');
		
		redirect('admin/administrators');
	}
	
	/*
	*
	*	Show an administrator's profile
	*	@param int $home_owner_id
	*
	*/
	public function admin_profile($home_owner_id)
	{
		//open the add new home_owner page
		$data['title'] = 'Edit Tenant';
		
		//select the home_owner from the database
		$query = $this->home_owners_model->get_home_owner($home_owner_id);
		if ($query->num_rows() > 0)
		{
			$v_data['home_owners'] = $query->result();
			$v_data['admin_home_owner'] = 1;
			$tab_content[0] = $this->load->view('home_owners/edit_home_owner', $v_data, true);
		}
		
		else
		{
			$data['tab_content'][0] = 'Home owner does not exist';
		}
		$tab_name[1] = 'Overview';
		$tab_name[0] = 'Edit Account';
		$tab_content[1] = 'Coming soon';//$this->load->view('account_overview', $v_data, true);
		$data['total_tabs'] = 2;
		$data['content'] = $tab_content;
		$data['tab_name'] = $tab_name;
		
		$this->load->view('templates/tabs', $data);
	}
	public function change_password()
	{
		$this->form_validation->set_rules('current_password', 'Current Password', 'required|xss_clean');
		$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
		$this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->input->post('new_password') == $this->input->post('confirm_new_password'))
			{
				if($this->home_owners_model->change_password())
				{
					$this->session->set_userdata('success_message', 'Your password has been changed successfully');
				}
				else
				{
					$this->session->set_userdata('error_message', 'Something went wrong, please try again');
				}
			}
			else
			{
				$this->session->set_userdata('error_message', 'Ensure that the new password match with the confirm password');
			}
		}
		else
		{
			$this->session->set_userdata('error_message', 'Please check that all the fields have values');
		}
		redirect('dashboard');
	}
	public function import_home_owners()
	{
		$v_data['title'] = $data['title'] = 'Import home_owners';
		
		$data['content'] = $this->load->view('import/import_home_owners', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function home_owners_import_template()
	{
		//export projects template in excel 
		$this->home_owners_model->home_owners_import_template();
	}
	public function home_owners_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->home_owners_model->import_csv_home_owners($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		redirect ('rental-management/home-owners');
	}
}
?>