<?php

class Login_model extends CI_Model
{
	/*
	*	Check if member has logged in
	*
	*/
	public function check_member_login()
	{
		if($this->session->userdata('member_login_status'))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}

	/*
	*	Update user's last login date
	*
	*/
	private function update_user_login($user_id)
	{
		$data['last_login'] = date('Y-m-d H:i:s');
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data);
	}

	/*
	*	Reset a user's password
	*
	*/
	public function reset_password($user_id)
	{
		$new_password = substr(md5(date('Y-m-d H:i:s')), 0, 6);

		$data['password'] = md5($new_password);
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data);

		return $new_password;
	}

	/*
	*	Reset a user's password
	*
	*/
	public function get_profile_details()
	{
		// 9530
		//.$this->session->userdata('member_id')
		$this->db->select('*');
		$this->db->where('id = '.$this->session->userdata('member_id'));
		$query = $this->db->get('jos_users');
		return $query;
	}

	public function get_from_question($member_id)
	{
		$this->db->select('*');
		$this->db->where('member_id = '.$member_id);
		$query = $this->db->get('cpd_question');

		return $query;
	}

	public function register_member_details()
	{
		// AND username = "'.$this->input->post('member_no').'"
		$this->db->select('*');
		$this->db->where('email = "'.$this->input->post('email').'" ');
		$query = $this->db->get('jos_users');

		return $query;


	}

	public function get_profile_items($member_no)
	{
		// 9530
		//.$this->session->userdata('member_id')
		$this->db->select('*');
		$this->db->where('member_number = "'.$member_no.'"');
		$query = $this->db->get('member');
		return $query;
	}

	/*
	*	Validate a customer's sign up request
	*
	*/
	public function signup_customer()
	{
		$newdata = array(
		   'customer_first_name'    	=> $this->input->post('fname'),
		   'customer_email'     		=> $this->input->post('email'),
		   'customer_phone'     		=> $this->input->post('phone'),
		   'customer_status'     		=> 1,
		   'customer_password'     		=> md5($this->input->post('password')),
		   'customer_created'     		=> date('Y-m-d H:i:s')
	   );

		//if users exists
		if ($this->db->insert('customer', $newdata))
		{
			$customer_id = $this->db->insert_id();

			$response['status'] = TRUE;
			$response['customer_id'] = $customer_id;
		}

		//if user doesn't exist
		else
		{
			$response['status'] = FALSE;
			$response['message'] = '';
		}
		return $response;
	}

	/*
	*	Validate a customer's sign up request
	*
	*/
	public function signup_customer2($customer_id)
	{
		$newdata = array(
		   'brand_model_id'    	=> $this->input->post('carmodel'),
		   'customer_id'    	=> $customer_id,
		   'created'     		=> date('Y-m-d H:i:s')
	   );

		//if users exists
		if ($this->db->insert('customer_model', $newdata))
		{
			$customer_model_id = $this->db->insert_id();

			$response['status'] = TRUE;
			$response['customer_model_id'] = $customer_model_id;
		}

		//if user doesn't exist
		else
		{
			$response['status'] = FALSE;
			$response['message'] = '';
		}
		return $response;
	}

	/*
	*	Validate a member's login request
	*
	*/
	public function validate_member($email, $password)
	{
		//select the user by email from the database
		$where = 'tenant_number = "'.$email.'" AND tenant_password = "'.md5($password).'"';
		//echo $where;
		$this->db->select('tenants.*');
		$this->db->where($where);
		$query = $this->db->get('tenants');

		//if users exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();

			//update user's last login date time
			// $this->update_member_login($result[0]->member_id);

			$newdata = array(
		                   'tenant_login_status'    => TRUE,
		                   'tenant_id'     			=> $result[0]->tenant_id,
		                   'tenant_email'     		=> $result[0]->tenant_email,
		                   'tenant_name'     	=> $result[0]->tenant_name,
											 'tenant_number'     	=> $result[0]->tenant_number,
		                   'tenant_phone_number'  			=> $result[0]->tenant_phone_number
		               );

			$response['status'] = TRUE;
			$response['message'] = $newdata;
		}

		//if user doesn't exist
		else
		{
			$response['status'] = FALSE;
			$response['message'] = '';
		}
		return $response;
	}

	/*
	*	Validate a member's login request
	*
	*/
	public function validate_rider()
	{
		//select the user by email from the database
		$where = 'personnel_status = 1 AND personnel_email = "'.$this->input->post('email').'" AND personnel_password = "'.md5($this->input->post('password')).'"';
		//echo $where;
		$this->db->select('personnel.*');
		$this->db->where($where);
		$query = $this->db->get('personnel');

		//if users exists
		if ($query->num_rows() > 0)
		{
			$result = $query->result();

			//update user's last login date time
			// $this->update_member_login($result[0]->member_id);

			$newdata = array(
		                   'customer_login_status'	=> TRUE,
		                   'personnel_id'     		=> $result[0]->personnel_id,
		                   'email'     				=> $result[0]->personnel_email,
		                   'first_name'     		=> $result[0]->personnel_fname,
		                   'phone'  				=> $result[0]->personnel_phone
		               );

			$response['status'] = TRUE;
			$response['message'] = $newdata;
		}

		//if user doesn't exist
		else
		{
			$response['status'] = FALSE;
			$response['message'] = '';
		}
		return $response;
	}

	/*
	*	Update user's last login date
	*
	*/
	private function update_member_login($member_id)
	{
		$data['last_login'] = date('Y-m-d H:i:s');
		$this->db->where('member_id', $member_id);
		$this->db->update('member', $data);
	}

	/*
	*	Retrieve a single user by their email
	*	@param int $email
	*
	*/
	public function get_user_by_email($email)
	{
		//retrieve all users
		$this->db->where('member_email', $email);
		$query = $this->db->get('member');

		return $query;
	}

	public function reset_member_password()
	{
		$email = $this->input->post('member_email');
		//reset password
		$result = md5(date("Y-m-d H:i:s"));
		$pwd2 = substr($result, 0, 6);
		$pwd = md5($pwd2);

		$data = array(
				'member_password' => $pwd
			);
		$this->db->where('member_email', $email);

		if($this->db->update('member', $data))
		{
			//email the password to the user
			$user_details = $this->get_user_by_email($email);

			$user = $user_details->row();
			$user_name = $user->member_username;

			$cc = NULL;
			$name = $user_name;

			$subject = 'You requested a password reset';
			$message = '<p>You have password has been successfully reset.</p><p>Next time you log in to ICPAK LIVE please use <strong>'.$pwd2.'</strong> as your password. You can change your password to something more memorable in your profile section once you log in.</p>';

			$button = '<p><a class="mcnButton " title="Sign in" href="'.site_url().'sign-in" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Sign in</a></p>';
			$shopping = '<p>If you have any queries or concerns do not hesitate to get in touch with us at <a href="mailto:info@nairobisingles.com">info@nairobisingles.com</a> </p>';
			$sender_email = 'icpak@icpak.com';
			$from = 'ICPAK LIVE';

			$response = $this->email_model->send_mandrill_mail($email, $name, $subject, $message, $sender_email, $shopping, $from, $button, $cc);

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function send_account_verification_email()
	{
		$email = $this->input->post('email');

		$this->db->where('email', $email);
		$query = $this->db->get('jos_users');

		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$name = $row->name;
			$id = $row->id;
			$username = $row->username;

			$cc = NULL;

			$token = $this->generateRandomString($email,10);

			$subject = 'Please activate your account';
			$message = '<p> Hello '.$name.',</p>
			<p>A request has been made to reset your ICPAK account password.</p>
			<p>To activate your account click on the button below</p>
			<p><a   href="http://www.icpak.com/index.php?option=com_resetpassword&task=confirmtoken&token='.$token.'" target="_blank" >Activate account</a></p>';

			$shopping = '<p>If you have any queries or concerns do not hesitate to get in touch with us at <a href="mailto:icpak@icpak.com">icpak@icpak.com</a> </p>';
			$sender_email = 'icpak@icpak.com';
			$from = 'ICPAK LIVE';


			// upadte the table



			$today = date('Y-m-d H:i:s');
			$newTime = strtotime('+2 weeks', strtotime($today));

		   	$this->db->where('user_id',$id);
			$this->db->delete('jos_resetpasswordtoken');

			$newdata = array(
			   'user_id'		=> $id,
			   'token'		=> $token,
			   'expire'			=> $today
		   	);
			$this->db->insert('jos_resetpasswordtoken', $newdata);

			$newdata = array(
			   'user_id'		=> $id,
			   'date'			=> $today
		   	);
			$this->db->insert('jos_resetpasswordlog', $newdata);

			// $response = $this->email_model->send_mandrill_mail($email, $name, $subject, $message, $sender_email, $shopping, $from, $button, $cc);
			include ('mailer/class.phpmailer.php');

			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->CharSet = 'UTF-8';
			$mail->Host       = "mail.safaricombusiness.co.ke"; // SMTP server example
			$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
			$mail->SMTPAuth   = false;                  // enable SMTP authentication
			$mail->Port       = 25;
			                    // set the SMTP port for the GMAIL server
			$mail->SetFrom("memberservices@icpak.com");

			$mail->AddAddress($email);


			$mail->IsHTML(true);
			$mail->Subject = 'ICPAK LIVE activate account';
			$mail->Body = $message;


			$mail->Send();

			return TRUE;
		}

	}
	public function generateRandomString($email,$length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString.md5($email);
	}
}
