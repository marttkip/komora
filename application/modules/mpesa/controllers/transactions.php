<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/real_estate_administration/controllers/property.php";
// require_once "./application/modules/mpesa/controllers/cron.php";
// header('Content-type: application/json;');
class Transactions extends property {
	var $csv_path;
	var $invoice_path;
	var $statement_path;
	function __construct()
	{
		parent:: __construct();
		$this->load->model('real_estate_administration/rental_unit_model');
		$this->load->model('real_estate_administration/leases_model');
		$this->load->model('real_estate_administration/property_owners_model');
		$this->load->model('real_estate_administration/property_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/petty_cash_model');
		$this->load->model('admin/email_model');
		$this->load->model('payroll/payroll_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/branches_model');
		$this->load->model('administration/reports_model');
    $this->load->model('mpesa/mpesa_model');
		// $this->load->controller('mpesa/cron');
		// $this->load->model('accounting/salary_advance_model');

		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->invoice_path = realpath(APPPATH . '../invoices/');
		$this->statement_path = realpath(APPPATH . '../statements/');
	}



  public function unreconcilled_payments()
	{
		$where = 'mpesa_status = 0 AND lease_id is null AND amount <> recon_amount';
		$table = 'v_mpesa_transactions';

		$search = $this->session->userdata('search_mpesa_received');
		if(!empty($search))
		{
			$where .= $search;
		}
				$this->session->unset_userdata('lease_number_searched');
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'mpesa-transactions/unallocated-transactions';
		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_unreconcilled_payments($table, $where, $config["per_page"], $page, $order='created', $order_method='DESC');

		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();

		$data['title'] = 'All Unreconcilled Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('unreconcilled_payments', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}
	public function reconcile_payment($mpesa_id)
	{

		$where = 'mpesa_status = 0 AND mpesa_id = '.$mpesa_id;
		$table = 'mpesa_transactions';

		$this->db->where($where);
		$query = $this->db->get('mpesa_transactions');
		$data['title'] = 'All Unreconcilled Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$data['content'] = $this->load->view('reconcile_payment', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function reconcile_payment_item($tenant_unit_id , $lease_id, $mpesa_id)
	{
		$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('amount_paid', 'Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$this->form_validation->set_rules('water_amount', 'Water amount', 'trim|xss_clean');
		$this->form_validation->set_rules('service_charge_amount', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('penalty_fee', 'Service charge amount', 'trim|xss_clean');
		$this->form_validation->set_rules('rent_amount', 'Rent amount', 'trim|xss_clean');



		$payment_method = $this->input->post('payment_method');
		$water_amount = $this->input->post('water_amount');
		$rent_amount = $this->input->post('rent_amount');
		$service_charge_amount = $this->input->post('service_charge_amount');
		$penalty_fee = $this->input->post('penalty_fee');
		$amount_paid = $this->input->post('amount_paid');
		$fixed_charge = $this->input->post('fixed_charge');
		$deposit_charge = $this->input->post('deposit_charge');

		$insurance = $this->input->post('insurance');
		$sinking_funds = $this->input->post('sinking_funds');
		$bought_water = $this->input->post('bought_water');
		$painting_charge = $this->input->post('painting_charge');
		$legal_fees = $this->input->post('legal_fees');


		if(empty($water_amount))
		{
			$water_amount = 0;
		}
		if(empty($service_charge_amount))
		{
			$service_charge_amount = 0;
		}
		if(empty($rent_amount))
		{
			$rent_amount = 0;
		}
		if(empty($penalty_fee))
		{
			$penalty_fee = 0;
		}
		if(empty($fixed_charge))
		{
			$fixed_charge = 0;
		}

		if(empty($deposit_charge))
		{
			$deposit_charge = 0;
		}
		if(empty($insurance))
		{
			$insurance = 0;
		}
		if(empty($sinking_funds))
		{
			$sinking_funds = 0;
		}
		if(empty($bought_water))
		{
			$bought_water = 0;
		}
		if(empty($painting_charge))
		{
			$painting_charge = 0;
		}
		if(empty($legal_fees))
		{
			$legal_fees = 0;
		}
		//  add all this items
		$total = $service_charge_amount + $rent_amount + $water_amount + $penalty_fee + $fixed_charge + $deposit_charge + $insurance + $sinking_funds + $bought_water + $painting_charge+ $legal_fees;

		// var_dump($total); die();
		// Normal


		if($payment_method == 1)
		{
			// check for cheque number if inserted
			$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');

		}
		else if($payment_method == 5)
		{
			//  check for mpesa code if inserted
			$this->form_validation->set_rules('mpesa_code', 'Amount', 'is_unique[payments.transaction_code]|trim|required|xss_clean');
		}

		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($amount_paid == $total)
			{
				$this->mpesa_model->receipt_payment($lease_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
				$response['status'] = 'success';
				$response['message'] = 'Payment successfully added';

				// update payments
				$update_array['lease_id'] = $lease_id;
				$update_array['mpesa_status'] = 1;
				$this->db->where('mpesa_id',$mpesa_id);
				$this->db->update('mpesa_transactions',$update_array);

				$sender_phone = $this->input->post('sender_phone');
				$account_number = $this->input->post('account_number');
				// $tenant_phone_number = '+'.$sender_phone;
				$tenant_phone_number = '+254734808007';
				if(!empty($tenant_phone_number))
				{
					$message = 'We have received Ksh. '.$amount_paid.' for account '.$account_number.'. Thank you';
					// $this->mpesa_model->sms($tenant_phone_number,$message,$tenant_name);

					// save the message sent out
					// $insert_array = array('phone_number'=>$tenant_phone_number,'client_name'=>$tenant_name,'type_of_account'=>1,'message'=>$message,'date_created'=>date('Y-m-d'),'sms_type'=>2);
					// $this->db->insert('sms',$insert_array);
					// save the message sent out

				}

				redirect('accounts/payments/'.$tenant_unit_id.'/'.$lease_id);
			}
			else
			{
				$this->session->set_userdata("error_message", 'The amounts of payment do not add up to the total amount paid');
				$response['status'] = 'fail';
				$response['message'] = 'The amounts of payment do not add up to the total amount paid';
				$redirect_url = $this->input->post('redirect_url');
				redirect($redirect_url);
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			$response['status'] = 'fail';
			$response['message'] = validation_errors();
			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);

		}

		// echo json_encode($response);

  }

  public function completed_mpesa_payments()
	{
		$where = 'mpesa_status = 0 AND lease_id is null AND amount = recon_amount';
		$table = 'v_mpesa_transactions';

		$search = $this->session->userdata('search_completed_mpesa');
		if(!empty($search))
		{
			$where .= $search;
		}
				$this->session->unset_userdata('lease_number_searched');
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'mpesa-transactions/completed-transactions';
		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_unreconcilled_payments($table, $where, $config["per_page"], $page, $order='created', $order_method='DESC');

		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();

		$data['title'] = 'Completed Mpesa Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('completed_payments', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_completed_mpesa_transactions()
	{
		$serial_number = $this->input->post('serial_number');
		$sender_name = $this->input->post('sender_name');
		$transaction_date_from = $this->input->post('transaction_date_from');
		$transaction_date_to = $this->input->post('transaction_date_to');
		// var_dump($_POST);die();
		$search_title = '';

		if(!empty($serial_number))
		{
			$serial_number = ' AND v_mpesa_transactions.serial_number = "'.$serial_number.'"';
		}
		if(!empty($sender_name))
		{
			$search_title .= $sender_name.' ';
			$sender_name = ' AND v_mpesa_transactions.sender_name LIKE \'%'.$sender_name.'%\'';


		}
		if(!empty($transaction_date_from) && !empty($transaction_date_to))
		{
			$transaction_date = ' AND v_mpesa_transactions.transaction_date >= \''.$transaction_date_from.'\' AND v_mpesa_transactions.transaction_date <= \''.$transaction_date_to.'\'';
			$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else if(!empty($transaction_date_from))
		{
			$transaction_date = ' AND v_mpesa_transactions.transaction_date = \''.$transaction_date_from.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
		}

		else if(!empty($transaction_date_to))
		{
			$transaction_date = ' AND v_mpesa_transactions.transaction_date = \''.$transaction_date_to.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else
		{
			$transaction_date = '';
		}


		$search = $serial_number.$sender_name.$transaction_date;

		$this->session->set_userdata('search_completed_mpesa', $search);
		redirect('mpesa-transactions/completed-transactions');
	}

	public function close_completed_mpesa_search()
	{
		$this->session->unset_userdata('search_completed_mpesa');
		redirect('mpesa-transactions/completed-transactions');
	}
  public function reversed_mpesa_payments()
  {
    $where = 'mpesa_status = 1 AND lease_id is null';
		$table = 'v_mpesa_transactions';

		$search = $this->session->userdata('search_reversed_mpesa');
		if(!empty($search))
		{
			$where .= $search;
		}
		$this->session->unset_userdata('lease_number_searched');
		$segment = 3;
		//pagination

		$this->load->library('pagination');
		$config['base_url'] = base_url().'mpesa-transactions/reversed-transactions';
		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->mpesa_model->get_unreconcilled_payments($table, $where, $config["per_page"], $page, $order='created', $order_method='DESC');

		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();

		$data['title'] = 'Reversed Mpesa Payments';
		$v_data['title'] = $data['title'];
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('reversed_payments', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
  }
	public function search_reversed_mpesa_transactions()
	{
		$serial_number = $this->input->post('serial_number');
		$sender_name = $this->input->post('sender_name');
		$transaction_date_from = $this->input->post('transaction_date_from');
		$transaction_date_to = $this->input->post('transaction_date_to');
		// var_dump($_POST);die();
		$search_title = '';

		if(!empty($serial_number))
		{
			$serial_number = ' AND v_mpesa_transactions.serial_number = "'.$serial_number.'"';
		}
		if(!empty($sender_name))
		{
			$search_title .= $sender_name.' ';
			$sender_name = ' AND v_mpesa_transactions.sender_name LIKE \'%'.$sender_name.'%\'';


		}
		if(!empty($transaction_date_from) && !empty($transaction_date_to))
		{
			$transaction_date = ' AND v_mpesa_transactions.transaction_date >= \''.$transaction_date_from.'\' AND v_mpesa_transactions.transaction_date <= \''.$transaction_date_to.'\'';
			$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else if(!empty($transaction_date_from))
		{
			$transaction_date = ' AND v_mpesa_transactions.transaction_date = \''.$transaction_date_from.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
		}

		else if(!empty($transaction_date_to))
		{
			$transaction_date = ' AND v_mpesa_transactions.transaction_date = \''.$transaction_date_to.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else
		{
			$transaction_date = '';
		}


		$search = $serial_number.$sender_name.$transaction_date;

		$this->session->set_userdata('search_reversed_mpesa', $search);
		redirect('mpesa-transactions/reversed-transactions');
	}

	public function close_reversed_mpesa_search()
	{
		$this->session->unset_userdata('search_reversed_mpesa');
		redirect('mpesa-transactions/reversed-transactions');
	}
    public function all_mpesa_transactions_payments()
  	{
  		$where = 'mpesa_id > 0';
  		$table = 'v_mpesa_transactions';

  		$search = $this->session->userdata('search_all_mpesa');
  		if(!empty($search))
  		{
  			$where .= $search;
  		}
  				$this->session->unset_userdata('lease_number_searched');
  		$segment = 3;
  		//pagination

  		$this->load->library('pagination');
  		$config['base_url'] = base_url().'mpesa-transactions/all-mpesa-transactions';
  		$config['total_rows'] = $this->mpesa_model->count_items($table, $where);
  		$config['uri_segment'] = $segment;
  		$config['per_page'] = 20;
  		$config['num_links'] = 5;

  		$config['full_tag_open'] = '<ul class="pagination pull-right">';
  		$config['full_tag_close'] = '</ul>';

  		$config['first_tag_open'] = '<li>';
  		$config['first_tag_close'] = '</li>';

  		$config['last_tag_open'] = '<li>';
  		$config['last_tag_close'] = '</li>';

  		$config['next_tag_open'] = '<li>';
  		$config['next_link'] = 'Next';
  		$config['next_tag_close'] = '</span>';

  		$config['prev_tag_open'] = '<li>';
  		$config['prev_link'] = 'Prev';
  		$config['prev_tag_close'] = '</li>';

  		$config['cur_tag_open'] = '<li class="active">';
  		$config['cur_tag_close'] = '</li>';

  		$config['num_tag_open'] = '<li>';
  		$config['num_tag_close'] = '</li>';
  		$this->pagination->initialize($config);

  		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
          $v_data["links"] = $this->pagination->create_links();
  		$query = $this->mpesa_model->get_unreconcilled_payments($table, $where, $config["per_page"], $page, $order='created', $order_method='DESC');

  		$v_data['cancel_actions'] = $this->mpesa_model->get_cancel_actions();

  		$data['title'] = 'All Mpesa Payments';
  		$v_data['title'] = $data['title'];
  		$v_data['query'] = $query;
  		$v_data['page'] = $page;
  		$data['content'] = $this->load->view('all_mpesa_transactions', $v_data, true);

  		$this->load->view('admin/templates/general_page', $data);



	}

	public function search_all_mpesa_transactions()
	{
		$serial_number = $this->input->post('serial_number');
		$sender_name = $this->input->post('sender_name');
		$transaction_date_from = $this->input->post('transaction_date_from');
		$transaction_date_to = $this->input->post('transaction_date_to');
		// var_dump($_POST);die();
		$search_title = '';

		if(!empty($serial_number))
		{
			$serial_number = ' AND v_mpesa_transactions.serial_number = "'.$serial_number.'"';
		}
		if(!empty($sender_name))
		{
			$search_title .= $sender_name.' ';
			$sender_name = ' AND v_mpesa_transactions.sender_name LIKE \'%'.$sender_name.'%\'';


		}
		if(!empty($transaction_date_from) && !empty($transaction_date_to))
		{
			$transaction_date = ' AND v_mpesa_transactions.transaction_date >= \''.$transaction_date_from.'\' AND v_mpesa_transactions.transaction_date <= \''.$transaction_date_to.'\'';
			$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else if(!empty($transaction_date_from))
		{
			$transaction_date = ' AND v_mpesa_transactions.transaction_date = \''.$transaction_date_from.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
		}

		else if(!empty($transaction_date_to))
		{
			$transaction_date = ' AND v_mpesa_transactions.transaction_date = \''.$transaction_date_to.'\'';
			$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
		}

		else
		{
			$transaction_date = '';
		}


		$search = $serial_number.$sender_name.$transaction_date;

		$this->session->set_userdata('search_all_mpesa', $search);
		redirect('mpesa-transactions/all-mpesa-transactions');
	}

	public function close_all_mpesa_search()
	{
		$this->session->unset_userdata('search_all_mpesa');
		redirect('mpesa-transactions/all-mpesa-transactions');
	}


  		public function cancel_mpesa_payment($mpesa_id)
  		{
  			$this->form_validation->set_rules('cancel_description', 'Description', 'trim|required|xss_clean');
  			$this->form_validation->set_rules('cancel_action_id', 'Action', 'trim|required|xss_clean');
  			$redirect_url = $this->input->post('redirect_url');
  			//if form conatins invalid data
  			if ($this->form_validation->run())
  			{
  				// end of checker function
  				if($this->mpesa_model->cancel_mpesa_payment($mpesa_id))
  				{
  					$this->session->set_userdata("success_message", "Transaction successfully cancelled");
  				}
  				else
  				{
  					$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");
  				}
  			}
  			else
  			{
  				$this->session->set_userdata("error_message", validation_errors());

  			}
  			redirect($redirect_url);
  		}

		public function search_mpesa_transactions()
		{
			$serial_number = $this->input->post('serial_number');
			$sender_name = $this->input->post('sender_name');
			$transaction_date_from = $this->input->post('transaction_date_from');
			$transaction_date_to = $this->input->post('transaction_date_to');
			// var_dump($_POST);die();
			$search_title = '';

			if(!empty($serial_number))
			{
				$serial_number = ' AND v_mpesa_transactions.serial_number = "'.$serial_number.'"';
			}
			if(!empty($sender_name))
			{
				$search_title .= $sender_name.' ';
				$sender_name = ' AND v_mpesa_transactions.sender_name LIKE \'%'.$sender_name.'%\'';


			}
			if(!empty($transaction_date_from) && !empty($transaction_date_to))
			{
				$transaction_date = ' AND v_mpesa_transactions.transaction_date >= \''.$transaction_date_from.'\' AND v_mpesa_transactions.transaction_date <= \''.$transaction_date_to.'\'';
				$search_title .= 'payment date from '.date('jS M Y', strtotime($transaction_date_from)).' to '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else if(!empty($transaction_date_from))
			{
				$transaction_date = ' AND v_mpesa_transactions.transaction_date = \''.$transaction_date_from.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_from)).' ';
			}

			else if(!empty($transaction_date_to))
			{
				$transaction_date = ' AND v_mpesa_transactions.transaction_date = \''.$transaction_date_to.'\'';
				$search_title .= 'payment date of '.date('jS M Y', strtotime($transaction_date_to)).' ';
			}

			else
			{
				$transaction_date = '';
			}


			$search = $serial_number.$sender_name.$transaction_date;

			$this->session->set_userdata('search_mpesa_received', $search);
			redirect('mpesa-transactions/unallocated-transactions');
		}

		public function close_mpesa_search()
		{
			$this->session->unset_userdata('search_mpesa_received');
			redirect('mpesa-transactions/unallocated-transactions');
		}


		public function update_mpesa_list()
		{
			$test_url = site_url().'mpesa/cron/pull_transactions';

			$patient_details['id'] = 1;
			$data_string = json_encode($patient_details);

			try{

				$ch = curl_init($test_url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);
				$result = curl_exec($ch);
				curl_close($ch);
			}
			catch(Exception $e)
			{
				$response = "something went wrong";
				echo json_encode($response.' '.$e);
			}
			redirect('mpesa-transactions/unallocated-transactions');
		}
		public function reverse_payment_detail($payment_id,$tenant_unit_id,$lease_id)
		{
			$this->db->where('lease_id = '.$lease_id.' AND payment_id ='.$payment_id);
			$query = $this->db->get('payments');

			if($query->num_rows() > 0)
			{
				$value = $query->row();
				$array = json_decode(json_encode($value), true);
				$amount_paid = $value->amount_paid;
				unset($array['payment_id']);
				unset($array['amount_paid']);
				$array['amount_paid'] = -$amount_paid;
				// var_dump($payment_id);die();
				$this->db->insert('payments',$array);
				$payment_idd = $this->db->insert_id();

				$this->db->where('payment_id ='.$payment_id);
				$query_two = $this->db->get('payment_item');

				if($query_two->num_rows() > 0)
				{
					foreach ($query_two->result() as $key => $value_two) {
						// code...
						$array_two = json_decode(json_encode($value_two), true);
						$amount_paid_two = $value_two->amount_paid;
						unset($array_two['payment_item_id']);
						unset($array_two['payment_id']);
						unset($array_two['remarks']);
						unset($array_two['amount_paid']);
						$array_two['amount_paid'] = -$amount_paid_two;
						$array_two['remarks'] = 'Payment Reversal';
						$array_two['payment_id'] = $payment_idd;
						$this->db->insert('payment_item',$array_two);
					}
				}
			}

			redirect('mpesa-transactions/unallocated-transactions');
		}
		public function print_mpesa_transactions($status = null)
		{

			if($status = 1)
			{
					$where = 'mpesa_status = 0 AND lease_id is null AND amount <> recon_amount';
			}
			else {
					$where = 'mpesa_status = 0 AND lease_id is null';
			}


			$table = 'v_mpesa_transactions';

			$search = $this->session->userdata('search_mpesa_received');
			if(!empty($search))
			{
				$where .= $search;
			}

			$data['contacts'] = $this->site_model->get_contacts();
			$data['query'] = $this->mpesa_model->get_unreconcilled_payments($table, $where,null,null,'v_mpesa_transactions.transaction_date');

			$this->load->view('print_mpesa_transactions', $data);
		}



		public function add_payment_item($tenant_unit_id , $lease_id, $close_page = NULL)
		{
			// var_dump($lease_id); die();
			$this->form_validation->set_rules('invoice_type_id', 'Invoice Item', 'trim|required|xss_clean');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
			$this->form_validation->set_rules('payment_month', 'Month', 'trim|required|xss_clean');
			$this->form_validation->set_rules('payment_year', 'Year', 'trim|required|xss_clean');

			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				$mpesa_id = $this->input->post('mpesa_id');

				if($mpesa_id > 0)
				{
					// check if the value is  greater that the one placed
						$actual_balance = $this->input->post('actual_balance');
						$amount = $this->input->post('amount');

						if($amount > $actual_balance)
						{
							$response['status'] = 'error';
							$response['message'] = 'Payment successfully added';
							$this->session->set_userdata('error_message', 'Sorry the amount placed cannot be more that the amount of money paid by the customer');
						}
						else {
							$this->mpesa_model->payment_item_add($lease_id);
							$this->session->set_userdata("success_message", 'Payment successfully added');
							$response['status'] = 'success';
							$response['message'] = 'Payment successfully added';
							$this->session->set_userdata('success_message', 'You have added the payment item successfully');
						}

				}
				else {
					$this->mpesa_model->payment_item_add($lease_id);
					$this->session->set_userdata("success_message", 'Payment successfully added');
					$response['status'] = 'success';
					$response['message'] = 'Payment successfully added';
					$this->session->set_userdata('success_message', 'You have added the payment item successfully');
				}

			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());
				$response['status'] = 'fail';
				$response['message'] = validation_errors();
				$this->session->set_userdata('error_message', validation_errors());
			}
			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);
		}

		public function delete_payment_items_id($payment_item_id)
		{

			$this->db->where('payment_item_id',$payment_item_id);
			if($this->db->delete('payment_item'))
			{
				$this->session->set_userdata('success_message', 'you have successfully removed the payment item');
			}
			else
			{
				$this->session->set_userdata('error_message', 'Please check the details and try again ');
			}

			redirect('mpesa-transactions/unallocated-transactions');
		}

		public function confirm_payment($tenant_unit_id , $lease_id, $close_page = NULL)
		{
			$this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
			$this->form_validation->set_rules('total_amount', 'Amount', 'trim|required|xss_clean');
			$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
			$payment_method = $this->input->post('payment_method');
			if(!empty($payment_method))
			{
				if($payment_method == 2 || $payment_method == 3 || $payment_method == 5)
				{
					// check for cheque number if inserted
					$this->form_validation->set_rules('bank_id', 'Bank Id', 'trim|required|xss_clean');
					$this->form_validation->set_rules('reference_number', 'Amount', 'trim|required|xss_clean');
				}

				$response['status'] = 'fail';
				$response['message'] = 'Please select a payment method';
			}

			//if form conatins invalid data
			if ($this->form_validation->run())
			{
				$mpesa_id = $this->input->post('mpesa_id');
					$this->mpesa_model->confirm_payment($lease_id);

					$this->session->set_userdata("success_message", 'Payment successfully added');
					$response['status'] = 'success';
					$response['message'] = 'Payment successfully added';
			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());
				$response['status'] = 'fail';
				$response['message'] = validation_errors();

			}

			// echo json_encode($response);


			$redirect_url = $this->input->post('redirect_url');
			redirect($redirect_url);
		}

		public function update_payments_item($payment_item_id,$lease_id)
		{
			$this->form_validation->set_rules('invoice_item_amount'.$payment_item_id, 'Payment Method', 'trim|required|xss_clean');

			//if form conatins invalid data
			if ($this->form_validation->run())
			{
					$array['amount_paid'] = $this->input->post('invoice_item_amount'.$payment_item_id);
					$this->db->where('payment_item_id',$payment_item_id);
					$this->db->update('payment_item',$array);

					$this->session->set_userdata("success_message", 'Successfully removed the payment item');
					$response['status'] = 'success';
					$response['message'] = 'Payment successfully added';
			}
			else
			{
				$this->session->set_userdata("error_message", validation_errors());
			}

			redirect('mpesa-transactions/unallocated-transactions');
		}


		public function update_mpesa_list_cron()
		{
			$test_url = site_url().'mpesa/cron/pull_transactions';

			$patient_details['id'] = 1;
			$data_string = json_encode($patient_details);

			try{

				$ch = curl_init($test_url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);
				$result = curl_exec($ch);
				curl_close($ch);
			}
			catch(Exception $e)
			{
				$response = "something went wrong";
				echo json_encode($response.' '.$e);
			}
		}


}
?>
