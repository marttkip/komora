<?php

class Mpesa_model extends CI_Model
{

  /*
  *	Count all items from a table
  *	@param string $table
  * 	@param string $where
  *
  */
  public function count_items($table, $where, $limit = NULL)
  {
    if($limit != NULL)
    {
      $this->db->limit($limit);
    }
    $this->db->from($table);
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  /*
  *	Retrieve all tenants
  *	@param string $table
  * 	@param string $where
  *
  */
  public function get_unreconcilled_payments($table, $where, $per_page=null, $page=null, $order=null, $order_method = 'ASC')
  {
    //retrieve all tenants
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    $this->db->order_by($order, $order_method);
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }



	public function get_cancel_actions()
	{
		$this->db->where('cancel_action_status', 1);
		$this->db->order_by('cancel_action_name');

		return $this->db->get('cancel_action');
	}

  public function cancel_mpesa_payment($mpesa_id)
	{
		$data = array(
			"cancel_action_id" => $this->input->post('cancel_action_id'),
			"cancel_description" => $this->input->post('cancel_description'),
			"cancelled_by" => $this->session->userdata('personnel_id'),
			"cancelled_date" => date("Y-m-d"),
			"mpesa_status" => 1
		);

		$this->db->where('mpesa_id', $mpesa_id);
		if($this->db->update('mpesa_transactions', $data))
		{
			return TRUE;
		}

		else
		{
			return FALSE;
		}
	}



  	public function payment_item_add($lease_id)
  	{
  		$amount = $this->input->post('amount');
  		$invoice_type_id=$this->input->post('invoice_type_id');
  		// $invoice_id=$this->input->post('invoice_id');
  		$payment_month=$this->input->post('payment_month');
  		$payment_year=$this->input->post('payment_year');
  		$lease_number=$this->input->post('lease_number');
  		$rental_unit_id=$this->input->post('rental_unit_id');
  		$tenant_id=$this->input->post('tenant_id');
  		// insert for service charge

  		$this->db->where('lease_invoice_id',$invoice_id);
  		$query = $this->db->get('lease_invoice');

  		$row = $query->row();

  		$invoice_date = $row->invoice_date;


  		$this->db->where('invoice_type_id',$invoice_type_id);
  		$query_type = $this->db->get('invoice_type');
  		$row_two = $query_type->row();
  		$invoice_type_name = $row_two->invoice_type_name;
  		$concate = $payment_year.'-'.$payment_month;
  		$date = date('M Y',strtotime($concate));

  		$remarks = 'Payment for '.$date.' '.$invoice_type_name;

  		$service = array(
  							'payment_id'=>0,
  							'amount_paid'=> $amount,
  							'payment_month'=> $payment_month,
  							'rental_unit_id'=> $rental_unit_id,
  							'tenant_id'=> $tenant_id,
  							'lease_number'=> $lease_number,
  							'payment_year'=> $payment_year,
  							'invoice_type_id' => $invoice_type_id,
  							'payment_item_status' => 0,
  							'lease_id' => $lease_id,
  							'mpesa_id' => $this->input->post('mpesa_id'),
  							'personnel_id' => $this->session->userdata('personnel_id'),
  							'payment_item_created' => date('Y-m-d'),
  							'remarks'=>$remarks
  						);

  		// var_dump($service); die();
  		$this->db->insert('payment_item',$service);
  		return TRUE;

  	}


  	public function confirm_payment($lease_id,$personnel_id = NULL)
  	{
  		$amount = $this->input->post('total_amount');
  		$payment_method=$this->input->post('payment_method');
  		$type_of_account=$this->input->post('type_of_account');
  		$rental_unit_id = $this->input->post('rental_unit_id');
  		if($payment_method == 2 || $payment_method == 3 || $payment_method == 5)
  		{
  			$transaction_code = $this->input->post('reference_number');
  			$bank_id = $this->input->post('bank_id');
  		}
  		else
  		{
  			$transaction_code = '';
  			$bank_id = 1;
  		}

  		// calculate the points to get
  		$payment_date = $this->input->post('payment_date');

  		$date_check = explode('-', $payment_date);
  		$month = $date_check[1];
  		$year = $date_check[0];


  		$receipt_number = $this->accounts_model->create_receipt_number();

  		$data = array(
  			'payment_method_id'=>$payment_method,
  			'bank_id'=>$bank_id,
  			'amount_paid'=>$amount,
  			'personnel_id'=>$this->session->userdata("personnel_id"),
  			'transaction_code'=>$transaction_code,
  			'payment_date'=>$this->input->post('payment_date'),
  			'receipt_number'=>$transaction_code,
  			'document_number'=>$receipt_number,
  			'paid_by'=>$this->input->post('paid_by'),
  			'payment_created'=>date("Y-m-d"),
  			'rental_unit_id'=>$rental_unit_id,
  			'year'=>$year,
  			'month'=>$month,
  			'confirm_number'=> $receipt_number,
  			'payment_created_by'=>$this->session->userdata("personnel_id"),
  			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
  		);

  		if($type_of_account == 1)
  		{
  			$data['lease_id'] = $lease_id;

  			if($this->db->insert('payments', $data))
  			{
  				$payment_id = $this->db->insert_id();
  				$service = array(
  									'payment_id'=>$payment_id,
  									'payment_item_created' =>$this->input->post('payment_date'),
  									'payment_item_status'=>1,
  									'rental_unit_id'=>$rental_unit_id,
  									'lease_id'=>$lease_id,
  									'document_no'=>$receipt_number
  								);
  				$this->db->where('payment_item_status = 0 AND lease_id = '.$lease_id.' and personnel_id = '.$this->session->userdata('personnel_id'));
  				$this->db->update('payment_item',$service);


  				return TRUE;
  			}
  			else{
  				return FALSE;
  			}
  		}
  		else if($type_of_account == 0)
  		{
  			return FALSE;

  		}
  	}



}
?>
