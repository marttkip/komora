        <!-- Basic -->
		<meta charset="UTF-8">

		<title><?php echo $company_name;?> | <?php echo $title;?></title>
		<meta name="keywords" content="ERP" />
		<meta name="description" content="Dobi admin">
		<meta name="author" content="alvaro masitsa">

		<!-- Tell the browser to be responsive to screen width -->
		  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		  <!-- Bootstrap 3.3.7 -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/bootstrap/dist/css/bootstrap.min.css">
		  <!-- Font Awesome -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/font-awesome/css/font-awesome.min.css">


		    <!-- daterange picker -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
		  <!-- bootstrap datepicker -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
		  <!-- iCheck for checkboxes and radio inputs -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>plugins/iCheck/all.css">
		  <!-- Bootstrap Color Picker -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
		  <!-- Bootstrap time Picker -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>plugins/timepicker/bootstrap-timepicker.min.css">
		  <!-- Select2 -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/select2/dist/css/select2.min.css">

			  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/morris.js/morris.css">

		  <!-- Ionicons -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/Ionicons/css/ionicons.min.css">
		  <!-- jvectormap -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>bower_components/jvectormap/jquery-jvectormap.css">
		  <!-- Theme style -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>dist/css/AdminLTE.min.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins
		       folder instead of downloading all of them to reduce the load. -->
		  <link rel="stylesheet" href="<?php echo base_url()."assets/themes/admin/"?>dist/css/skins/_all-skins.min.css">

		  <!-- Google Font -->
		  <link rel="stylesheet"
		        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

		<!-- <script src="<?php echo base_url()."assets/themes/bluish/"?>js/jquery.js"></script>  -->
		<!-- jQuery -->
