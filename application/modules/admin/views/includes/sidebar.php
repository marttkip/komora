<?php
	$personnel_id = $this->session->userdata('personnel_id');
	
	if($personnel_id == 0)
	{
		$parents = $this->sections_model->all_parent_sections('section_position');
	}
	
	else
	{
		$personnel_roles = $this->personnel_model->get_personnel_roles($personnel_id);
		
		$parents = $personnel_roles;
	}
	$children = $this->sections_model->all_child_sections();
	
	$sections = '';
	
	if($parents->num_rows() > 0)
	{
		foreach($parents->result() as $res)
		{
			$section_parent = $res->section_parent;
			$section_id = $res->section_id;
			$section_name = $res->section_name;
			$section_icon = $res->section_icon;
			
			if($section_parent == 0)
			{
				$web_name = strtolower($this->site_model->create_web_name($section_name));
				$link = site_url().$web_name;
				$section_children = $this->admin_model->check_children($children, $section_id, $web_name);
				$total_children = count($section_children);
				

				if($total_children == 0)
				{
					if($title == $section_name)
					{
						$sections .= '<li class="active">';
					}
					
					else
					{
						$sections .= '<li>';
					}
					$sections .= '
						<a href="'.$link.'">
							<!--<span class="pull-right label label-primary">182</span>-->
							<i class="fa fa-'.$section_icon.'" aria-hidden="true"></i>
							<span>'.$section_name.'</span>
						</a>
					</li>
					';
				}
				
				else
				{
					if($title == $section_name)
					{
						$sections .= '<li class="active treeview menu-open">';
					}
					
					else
					{
						$sections .= '<li class="treeview">';
					}
					$sections .= '
						<a>
							<i class="fa fa-'.$section_icon.'" aria-hidden="true"></i>
							<span>'.$section_name.'</span>
							<span class="pull-right-container">
				              <i class="fa fa-angle-left pull-right"></i>
				            </span>
						</a>
						<ul class="treeview-menu">';
					
					//children
					for($r = 0; $r < $total_children; $r++)
					{
						$name = $section_children[$r]['section_name'];
						$link = $section_children[$r]['link'];
						
						$sections .= '
							<li>
								<a href="'.$link.'">
									 '.$name.'
								</a>
							</li>
						';
					}
					
					$sections .= '
					</ul></li>
					';
				}
			}
			
			else
			{
				//get parent section
				$parent_query = $this->sections_model->get_section($section_parent);
				
				$parent_row = $parent_query->row();
				$parent_name = $parent_row->section_name;
				$section_icon = $parent_row->section_icon;
				
				$web_name = strtolower($this->site_model->create_web_name($parent_name));
				$link = site_url().$web_name.'/'.strtolower($this->site_model->create_web_name($section_name));
				
				$sections .= '
				<li>
					<a href="'.$link.'">
						<i class="fa fa-'.$section_icon.'" aria-hidden="true"></i>
						<span>'.$section_name.'</span>
					</a>
				</li>
				';
			}
		}
	}
	
?>				
 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="https://via.placeholder.com/160x160" class="img-circle" alt="User Image">
        </div> 
        <div class="pull-left info">
          <p>
          	<?php 
				//salutation
				if(date('a') == 'am')
				{
					echo 'Good morning, <br/>';
				}
				
				else if((date('H') >= 12) && (date('H') < 17))
				{
					echo 'Good afternoon, <br/>';
				}
				
				else
				{
					echo 'Good evening, <br/>';
				}
				echo $this->session->userdata('first_name');
				
			?>
          </p>
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
        </div>
      </div>
     <!--  <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php echo $sections;?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
