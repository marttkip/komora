<?php



  $all_leases = $this->leases_model->get_lease_detail($lease_id);
  	foreach ($all_leases->result() as $leases_row)
  	{
  		$lease_id = $leases_row->lease_id;
  		$tenant_id = $leases_row->tenant_id;
  		$tenant_unit_id = $leases_row->tenant_unit_id;
  		$property_name = $leases_row->property_name;
  		$property_id = $leases_row->property_id;
  		$rental_unit_name = $leases_row->rental_unit_name;
      $rental_unit_id = $leases_row->rental_unit_id;
  		$tenant_name = $leases_row->tenant_name;
  		$lease_start_date = $leases_row->lease_start_date;
  		$lease_duration = $leases_row->lease_duration;
  		$rent_amount = $leases_row->rent_amount;
  		$lease_number = $leases_row->lease_number;
  		$arreas_bf = $leases_row->arrears_bf;
  		$rent_calculation = $leases_row->rent_calculation;
  		$deposit = $leases_row->deposit;
  		$deposit_ext = $leases_row->deposit_ext;
  		$tenant_phone_number = $leases_row->tenant_phone_number;
  		$tenant_national_id = $leases_row->tenant_national_id;
  		$lease_status = $leases_row->lease_status;
  		$tenant_status = $leases_row->tenant_status;
  		$created = $leases_row->created;

  		$lease_start_date = date('jS M Y',strtotime($lease_start_date));

  		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
  		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
  		//create deactivated status display
  		if($lease_status == 0)
  		{
  			$status = '<span class="label label-default"> Deactivated</span>';

  			$button = '';
  			$delete_button = '';
  		}
  		//create activated status display
  		else if($lease_status == 1)
  		{
  			$status = '<span class="label label-success">Active</span>';
  			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
  			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

  		}

  		//create deactivated status display
  		if($tenant_status == 0)
  		{
  			$status_tenant = '<span class="label label-default">Deactivated</span>';
  		}
  		//create activated status display
  		else if($tenant_status == 1)
  		{
  			$status_tenant = '<span class="label label-success">Active</span>';
  		}
  	}


    $month = $this->accounts_model->get_months();
    $months_list = '<option value="">Select a Type</option>';
    foreach($month->result() as $res)
    {
      $month_id = $res->month_id;
      $month_name = $res->month_name;
      if($month_id < 10)
      {
        $month_id = '0'.$month_id;
      }
      $month = date('M');

      if($month == $month_name)
      {
        $months_list .= '<option value="'.$month_id.'" selected>'.$month_name.'</option>';
      }
      else {
        $months_list .= '<option value="'.$month_id.'">'.$month_name.'</option>';
      }



    }


    $start = 2015;
    $end_year = 2030;
    $year_list = '<option value="">Select a Type</option>';
    for ($i=$start; $i < $end_year; $i++) {
      // code...
      $year= date('Y');

      if($year == $i)
      {
        $year_list .= '<option value="'.$i.'" selected>'.$i.'</option>';
      }
      else {
        $year_list .= '<option value="'.$i.'">'.$i.'</option>';
      }
    }

?>
<div class="col-md-4">
  <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Tenants Details</h3>

        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <table class="table table-bordered table-striped table-condensed">
          <thead>
            <tr>
              <th style="width: 40%;">Title</th>
              <th style="width: 60%;">Detail</th>
            </tr>
          </thead>
            <tbody>
              <tr><td><span>Tenant Name :</span></td><td><?php echo $tenant_name;?> </td></tr>
              <tr><td><span>Account No :</span></td><td><?php echo $lease_number;?></td></tr>
              <tr><td><span>Contract Status :</span></td><td><?php echo $status;?></td></tr>
              <tr><td><span>Property Name :</span></td><td><?php echo $property_name;?> - <?php echo $rental_unit_name;?></td></tr>
              <!-- <tr><td><span>Rental Unit :</span></td><td><?php echo $rental_unit_name;?></td></tr> -->
              <tr><td><span>Lease Start date :</span></td><td>
                        <?php echo $lease_start_date;?>
                </td></tr>
              <tr><td><span>Lease Expiry date :</span></td>
                <td>
                <?php echo $lease_end_date;?>
                </td>
              </tr>
            
            </tbody>
        </table>
      </div>
  </div>
</div>

<?php

	$amount_to_return = $this->accounts_model->get_deposits_paid($lease_id);
	if(empty($amount_to_return))
	{
		$amount_to_return = 0;
	}

	$expenses_payable = $this->accounts_model->get_expenses_payable($lease_id);
	if(empty($expenses_payable))
	{
		$expenses_payable = 0;
	}


	$tenants_response = $this->accounts_model->get_lease_balance($lease_id);
	$total_arrears = $tenants_response['balance'];
	$invoiced = $tenants_response['invoiced'];
	$waived = $tenants_response['waived'];
	$paid = $tenants_response['paid'];
?>
<div class="col-md-4 ">

    <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Account </h3>

          <div class="box-tools pull-right">

          </div>
        </div>
        <div class="box-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-condensed">
            <thead>
              <tr>
                <th>Title</th>
                <th>Detail</th>
              </tr>
            </thead>
              <tbody>
                <tr><td><span>Deposits Paid :</span></td><td><strong>KES. <?php echo number_format(($amount_to_return),2);?></strong> </td></tr>
                <tr><td><span>Total Invoices :</span></td><td><strong>KES. <?php echo number_format($invoiced,2);?></strong> </td></tr>
                <tr><td><span>Total Debit Notes :</span></td><td>KES <?php echo number_format($waived,2);?></td></tr>
                <tr><td><span>Total Credit Notes :</span></td><td><strong>KES. (<?php echo number_format(($waived),2);?>)</strong> </td></tr>
                <tr><td><span>Total Payments :</span></td><td><strong>KES. (<?php echo number_format(($paid),2);?>)</strong> </td></tr>
                <tr><td><span>Total Balance :</span></td><td><strong>KES. <?php echo number_format($total_arrears,2);?></strong> </td>
                </tr>             

              </tbody>
            </table>

        </div>
      </div>
  </div>


</div>

<div class="col-md-4">
  <div class="box box-info">
     
      <div class="box-body">
      	<div class="row">
	      	<div class="col-md-12">
	      		<a href="<?php echo site_url().'cash-office/tenants-leases';?>" class="btn btn-info btn-md col-md-12" > 
	      		<i class="fa fa-arrow-left"></i> Back to Leases </a>
	      	</div>
	    </div>
	    <br>
	    <div class="row">
	      	<div class="col-md-12">
	      		<a href="<?php echo site_url().'view-tenant-payments/'.$lease_id.'/'.$tenant_id;?>" class="btn btn-default btn-md col-md-12" > 
	      		<i class="fa fa-bank"></i> Payments </a>
	      	</div>
	    </div>
	    <br>
	    <div class="row">
	      	<div class="col-md-12">
	      		<a href="<?php echo site_url().'view-tenant-payments/'.$lease_id.'/'.$tenant_id;?>" class="btn btn-success btn-md col-md-12" > 
	      		<i class="fa fa-bank"></i> Invoices </a>
	      	</div>
	    </div>
	    <br>
	    <div class="row">
	      	<div class="col-md-12">
	      		<a href="<?php echo site_url().'view-tenant-credits/'.$lease_id.'/'.$tenant_id;?>" class="btn btn-default btn-md col-md-12" > 
	      		<i class="fa fa-credit-card"></i> Credit Notes </a>
	      	</div>
	    </div>
	    <br>
	    <div class="row">
	      	<div class="col-md-12">
	      		<a href="<?php echo site_url().'view-tenant-debits/'.$lease_id.'/'.$tenant_id;?>" class="btn btn-default btn-md col-md-12" > 
	      		<i class="fa fa-book"></i> Debit Notes </a>
	      	</div>
	    </div>
	    <br>
      <div class="row">
          <div class="col-md-12">
            <a href="<?php echo site_url().'print-tenant-statement/'.$lease_id.'/'.date('Y');?>" class="btn btn-default btn-md col-md-12" target="_blank"> 
            <i class="fa fa-file-pdf-o"></i> Statement </a>
          </div>
      </div>
       
      </div>
  </div>
</div>

<div class="col-md-12">
<?php
$success = $this->session->userdata('success_message');

if(!empty($success))
{
echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
$this->session->unset_userdata('success_message');
}

$error = $this->session->userdata('error_message');

if(!empty($error))
{
echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
$this->session->unset_userdata('error_message');
}
?>
  <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Add an invoice </h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-sm btn-default"  onclick="display_payment_model()">
                Add Invoice
          </button>
        </div>
      </div>
      <div class="box-body">
        <div class="box box-primary">
                <div class="box-body">
                    <!-- <form id="add_payments" method="post" class="form-horizontal"> -->
                    <?php echo form_open("accounts/add_invoice/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>
                      <input type="hidden" name="type_of_account" value="1">
                      <input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
                      <input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
                      <input type="hidden" name="tenant_id" id="tenant_id" value="<?php echo $tenant_id?>">
                      <input type="hidden" name="rental_unit_id" id="rental_unit_id" value="<?php echo $rental_unit_id?>">
                      <input type="hidden" name="lease_number" id="lease_number" value="<?php echo $lease_number?>">
                      <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                      <div class="col-md-4">
                        <div class="form-group" id="payment_method">
                        <label class="col-md-4 control-label">Type: </label>

                        <div class="col-md-7">
                          <select class="form-control select2" name="invoice_type_id" required>
                            <option value="">Select a Type</option>
                                                  <?php
                              $invoice_type = $this->accounts_model->get_invoice_types();

                              foreach($invoice_type->result() as $res)
                              {
                                $invoice_type_id = $res->invoice_type_id;
                                $invoice_type_name = $res->invoice_type_name;

                                echo '<option value="'.$invoice_type_id.'">'.$invoice_type_name.'</option>';

                              }

                            ?>
                          </select>
                          </div>
                      </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="col-md-4 control-label">Amount: </label>
                          <div class="col-md-7">
                            <input type="number" class="form-control" name="amount" placeholder=""  autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="center-align">
                          <button class="btn btn-info btn-sm" type="submit">Add Item </button>
                        </div>
                      </div>
                    <?php echo form_close();?>
          </div>
        </div>
        <hr>
         <?php echo form_open("accounts/confirm_invoice_note/".$tenant_unit_id."/".$lease_id, array("class" => "form-horizontal"));?>

        <?php
        $tenant_where = 'invoice.invoice_type = invoice_type.invoice_type_id AND lease_id = '.$lease_id.' AND invoice_item_status = 0 AND invoice.lease_invoice_id = 0';
        $tenant_table = 'invoice,invoice_type';
        $tenant_order = 'invoice_type_name';

        $tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);

        $result_payment ='<table class="table table-bordered table-striped table-condensed">
                            <thead>
                              <tr>  
                                <th ></th>
                                <th >#</th>
                                <th >Type</th>
                                <th >Amount</th>
                                <th colspan="2" >Action</th>
                              </tr>
                            </thead>
                              <tbody>';
        $total_amount = 0;
        if($tenant_query->num_rows() > 0)
        {
          $x = 0;

          foreach ($tenant_query->result() as $key => $value) {
            // code...
            $invoice_type_name = $value->invoice_type_name;
            $invoice_note_amount = $value->invoice_amount;
            $invoice_id = $value->invoice_id;
            $total_amount += $invoice_note_amount;
            $checkbox_data = array(
                            'name'        => 'tenant_invoice_items[]',
                            'id'          => 'checkbox'.$invoice_id,
                            'class'          => 'css-checkbox  lrg ',
                            'checked'=>'checked',
                            'value'       => $invoice_id
                          );


            $x++;
            // $result_payment .= form_open("accounts/update_invoice_item/".$invoice_id."/".$lease_id, array("class" => "form-horizontal"));
            $result_payment .= '<tr>
                                    <td>'.form_checkbox($checkbox_data).'<label for="checkbox'.$invoice_id.'" name="checkbox79_lbl" class="css-label lrg klaus"></label>'.'</td>
                                    <td>'.$x.'</td>
                                    <td>'.$invoice_type_name.'</td>
                                    <td><input class="form-control" name="invoice_item_amount'.$invoice_id.'" value="'.$invoice_note_amount.'"></td>
                                    <td><a href="'.site_url().'delete-invoice-item/'.$invoice_id.'" type="submit" class="btn btn-sm btn-danger" ><i class="fa fa-trash"></i></a></td>
                                </tr>';
            // $result_payment .=form_close();
          }

          // display button

          $display = TRUE;
        }
        else {
          $display = FALSE;
        }

        $result_payment .='</tbody>
                        </table>';
        ?>

        <?php echo $result_payment;?>
        <br>
        <?php
        if($display)
        {
          ?>
          <div class="row">
            <div class="col-md-12">
             
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <!-- <h2 class="pull-right"> KES. <?php echo number_format($total_amount,2);?></h2> -->

                    <input type="hidden" name="type_of_account" value="1">
                    <input type="hidden" name="tenant_unit_id" id="tenant_unit_id" value="<?php echo $tenant_unit_id;?>">
                    <input type="hidden" name="lease_id" id="lease_id" value="<?php echo $lease_id?>">
                    <input type="hidden" name="rental_unit_id" id="lease_id" value="<?php echo $rental_unit_id?>">
                    <input type="hidden" name="tenant_id" id="lease_id" value="<?php echo $tenant_id?>">
                    <input type="hidden" name="lease_number" id="lease_id" value="<?php echo $lease_number?>">
                    <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
                    <div class="form-group" id="payment_method">
                      <label class="col-md-4 control-label">Month: </label>

                      <div class="col-md-7">
                        <select class="form-control select2" name="invoice_month"   required>
                          <?php echo $months_list;?>
                        </select>
                        </div>
                    </div>
                    <div class="form-group" id="payment_method">
                      <label class="col-md-4 control-label">Year: </label>
                      <div class="col-md-7">
                        <select class="form-control select2" name="invoice_year"   required>

                          <?php echo $year_list;?>
                        </select>
                        </div>
                    </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Date: </label>

                    <div class="col-md-7">
                       <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="invoice_date" placeholder="Payment Date" id="datepicker" value="<?php echo date('Y-m-d')?>" required>
                        </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label">Total Amount: </label>

                    <div class="col-md-7">
                      <input type="number" class="form-control" name="total_amount" placeholder=""  autocomplete="off" value="<?php echo $total_amount;?>" readonly>
                    </div>
                  </div>

                  <div class="col-md-12">
                      <div class="text-center">
                        <button class="btn btn-info btn-sm " type="submit" onclick="return confirm('Do you want to complete this transaction ? ')">Complete Invoice </button>
                      </div>
                  </div>
                </div>
             
            </div>
          </div>
          <?php
        }
        ?>
         <?php echo form_close();?>

      </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Invoices</h3>

          <div class="box-tools pull-right">

          </div>
        </div>
        <div class="box-body">
        	<div class="table-responsive">
	            <table class="table table-hover table-bordered col-md-12">
	              <thead>
	                <tr>
	                  <th>#</th>
	                  <th>Invoice Date</th>
	                  <th>Document Number</th>
                    <th>Remarks</th>
	                  <th>Amount </th>
	                  <th>Captured Date</th>
	                  <th colspan="2">Actions</th>
	                </tr>
	              </thead>
	              <tbody>
	                <?php
	                  // var_dump($tenant_query);die();
	                if($lease_invoices->num_rows() > 0)
	                {
	                  $y = 0;
	                  foreach ($lease_invoices->result() as $key) {
	                    # code...
	                    $total_amount = $key->invoice_amount;
	                    $lease_invoice_id = $key->lease_invoice_id;
                      $remarks = $key->item_remarks;
	                    $invoice_date = $key->invoice_date;
	                    $document_number = $key->document_number;
	                    $created = $key->created;
	                    $created_by = $key->created_by;
	                    $payment_explode = explode('-', $invoice_date);
	                    $invoice_year= $payment_explode[0];
	                    $invoice_month= $payment_explode[1];

	                    $invoice_note_date = date('jS M Y',strtotime($invoice_date));
	                    $created = date('jS M Y',strtotime($created));
	                    $y++;
                      $authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
                      $button =  '';
                      if($authorize_invoice_changes)
                      {
                        $button = '<td><a href="'.site_url().'edit-invoice/'.$lease_id.'/'.$lease_invoice_id.'" class="btn btn-sm btn-warning" onclick="return confirm("Are you sure you want to edit this invoice ?")"><i class="fa fa-pencil"></i> Edit </a></td>';
                      }

	                    ?>
	                    <tr>
	                      <td><?php echo $y?></td>
	                      <td><?php echo $invoice_note_date;?></td>
	                      <td><?php echo $document_number?></td>
                        <td><?php echo $remarks;?></td>
	                      <td><?php echo number_format($total_amount,2);?></td>
	                      <td><?php echo $invoice_note_date;?></td>
	                      <td><a href="<?php echo site_url().'invoice/'.$lease_id.'/'.$invoice_month.'/'.$invoice_year.'/'.$lease_invoice_id;?>" class="btn btn-sm btn-primary" target="_blank">Invoice</a></td>
	                       <?php
                         $authorize_invoice_changes = $this->session->userdata('authorize_invoice_changes');
                          $button =  '';
                          if($authorize_invoice_changes)
                          {
                            ?>
                            <td><a href="<?php echo site_url().'edit-invoice/'.$lease_id.'/'.$lease_invoice_id;?>" class="btn btn-sm btn-warning" onclick="return confirm('Are you sure you want to edit this invoice ?'')"><i class="fa fa-pencil"></i> Edit </a></td>
                            <?php
                          }

                      ?>

	                    </tr>
	                    <?php

	                  }
	                }
	                ?>

	              </tbody>
	            </table>
	         </div>

          <div class="panel-footer">
	        	<?php if(isset($links)){echo $links;}?>
	        </div>

        </div>
      </div>
    </div>
  </div>