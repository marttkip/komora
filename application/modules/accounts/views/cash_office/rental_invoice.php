<?php
	$all_leases = $this->leases_model->get_lease_detail($lease_id);
	$items = '';
	$items_charge ='';
	$total_bill = 0;
	$service_charge = 0;
	$penalty_charge =0;
	$water_charge =0;
	foreach ($all_leases->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$property_name = $leases_row->property_name;
		// $units_name = $leases_row->units_name;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arrears_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$tenant_national_id = $leases_row->tenant_national_id;
		$lease_status = $leases_row->lease_status;
		$tenant_status = $leases_row->tenant_status;
		$created = $leases_row->created;
		$message_prefix = $leases_row->message_prefix;

		$lease_start_date = date('jS M Y',strtotime($lease_start_date));
		
		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
		
	}

	$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
	
	$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
	// get when the lease was active

	// get all the invoiced months
	// var_dump($lease_invoice); die();
	
	if($lease_invoice->num_rows() > 0)
	{
		$water_charge = 0;
		$service_bf = 0;
		$water_charge = 0;
		$water_bf = 0;
		$penalty_charge =0;
		$penalty_bf =0;
		$water_payments = 0;
		$total_penalty = 0;
		$total_water = 0;

		foreach ($lease_invoice->result() as $key_invoice) {
			# code...
			$invoice_date = $key_invoice->invoice_date;
			$invoice_id = $key_invoice->invoice_id;
			$invoice_type = $key_invoice->invoice_type;


			
			$invoice_date_date = date('jS F Y',strtotime($invoice_date));
			
			if($invoice_type == 1)
			{
				// service charge
				$rental_charge = $key_invoice->invoice_amount;
				$invoice_type_name = $key_invoice->invoice_type_name;
				
				$rental_payments = $this->accounts_model->get_total_payments_end($lease_id,$invoice_year,$invoice_month,$invoice_type);
				
				$rental_invoices= $this->accounts_model->get_total_invoices_end($lease_id,$invoice_year,$invoice_month,$invoice_type);

				$rental_invoices_bf= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_year,$invoice_month,$invoice_type);
				// var_dump($rental_invoices);die();
				$current_rental_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
				$rental_arrears = $rental_invoices - $rental_payments;
				$total_rental = $rental_arrears; 

				$rental_bf = $key_invoice->arrears_bf;
				$items .= '
							 <div class="row">
					        	<div class="col-xs-2 center-align">
					        		'.$rental_unit_name.'
					        	</div>
					        	<div class="col-xs-2 center-align">
					        		'.$invoice_type_name.'
					        	</div>
					        	<div class="col-xs-3 center-align">
					        		'.number_format($current_rental_invoice_amount) .'
					        	</div>
					        	<div class="col-xs-3 pull-right">
					        		'.number_format($total_rental).'
					        	</div>
					        </div>
							';
			}


			if($invoice_type == 5)
			{
				// penalty

				$penalty_charge = $key_invoice->invoice_amount;
				$invoice_type_name = $key_invoice->invoice_type_name;


				$penalty_payments = $this->accounts_model->get_total_payments_before($lease_id,$invoice_date,5);
				$penalty_invoices= $this->accounts_model->get_total_invoices_before($lease_id,$invoice_date,5);
				$penalty_arrears = $penalty_invoices - $penalty_payments;
				$total_penalty = $penalty_arrears + $penalty_charge; 
				$items_charge .= 			
								'
								  <div class="row">
							        	<div class="col-xs-2 center-align">
							        		'.$rental_unit_name.'
							        	</div>
							        	<div class="col-xs-2 center-align">
							        		'.$invoice_type_name.'
							        	</div>
							        	<div class="col-xs-3 center-align">
							        		'.number_format($penalty_charge) .'
							        	</div>
							        	<div class="col-xs-3 pull-right">
							        		'.number_format($total_penalty).'
							        	</div>
							        </div>
								';
			}

			$total_bill = $total_bill + $total_water + $total_penalty;
		}
	}



?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | INVOICE</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_top_border{border-top: #888888 medium solid; margin-top:1px;}
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
	        <div class="col-xs-12">
		    	<div class="receipt_bottom_border">
		        	<table class="table table-condensed">
		                <tr>
		                    <th><h4><?php echo $contacts['company_name'];?> Invoice </h4></th>
		                    <th class="align-right">
		                        <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="float:right;"/>
		                    </th>
		                </tr>
		            </table>
		        </div>
		    	
		        <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        	<div class="col-xs-6 ">
		                
		                <p><strong> Name </strong> : <?php echo $tenant_name;?></p>
		                <p><strong> Phone </strong> :<?php echo $tenant_phone_number;?></p>
		                <p><strong> Property </strong> :<?php echo $property_name;?></p>
		                 

		               
		            </div>
		            <div class="col-xs-6">
		             	<p><strong> Invoice # </strong> : <?php echo $receipt_number;?></p>
		             	<p><strong> Invoice Date </strong> : <?php echo date('jS M Y',strtotime(date('Y-m-d')));?></p>
		                
		            </div>
		        </div>
		      
		        <div class="row">
			        <div class="col-xs-12">
			        	 <div class="row receipt_bottom_border">
				        	<div class="col-xs-12" id="title">
				            	<strong>A. WATER DUES</strong>
				            </div>
				        </div>
				        <div class="row">
				        	<div class="col-xs-2 center-align">
				        		Apartment
				        	</div>
				        	<div class="col-xs-2 center-align">
				        		Billing For
				        	</div>
				        	<div class="col-xs-3 center-align">
				        		Current Dues + Bal B/F
				        	</div>
				        	<div class="col-xs-3 pull-right">
				        		Total Dues KES
				        	</div>
				        </div>
				        <?php echo $items;?>
			        </div>
			   	</div>
			   	<div class="row">
			        <div class="col-xs-12">
			        	<div class="row receipt_bottom_border">
				        	<div class="col-xs-12"  id="title">
				            	<strong>B. PENALTIES</strong>
				            </div>
				        </div>
				       
			        	<table class="table table-hover">
				           
				            <tbody>
				               	<?php echo $items_charge;?>
				            </tbody>
				        </table>
			        </div>
			     </div>
			    <div class="row" >
			    	<div class="col-xs-12" style="padding-top: 10px;padding-bottom: 5px;">
			    		<div class="row receipt_bottom_border">
				        	<div class="col-xs-12"  id="title">
				            	<strong>TOTAL DUES </strong>
				            </div>
				        </div>
				        <div class="row">
				        	<div class="col-xs-8 pull-left">
				        		Total Dues  A+B 
				        	</div>
				        	<div class="col-xs-3 pull-right">
				        	  <strong> <?php echo number_format($total_bill);?> </strong>
				        	</div>
				        </div>
				    </div>
			    </div>
		       
		       <div class="row " style="border-top:1px #000 solid; ">
		       		<div class="col-xs-8 pull-left">
		       			<strong style="text-align:left;"> <?php echo $message_prefix;?></strong>
		       		</div>

		       			
		            
		       </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>

