<?php

$company_name = $contacts['company_name'];


$all_leases = $this->leases_model->get_lease_detail($lease_id);
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$penalty_charge =0;
$water_charge =0;
foreach ($all_leases->result() as $leases_row)
{
	$lease_id = $leases_row->lease_id;
	$tenant_unit_id = $leases_row->tenant_unit_id;
	$property_name = $leases_row->property_name;
	// $units_name = $leases_row->units_name;
	$rental_unit_name = $leases_row->rental_unit_name;
	$tenant_name = $leases_row->tenant_name;
	$lease_start_date = $leases_row->lease_start_date;
	$lease_duration = $leases_row->lease_duration;
	$rent_amount = $leases_row->rent_amount;
	$lease_number = $leases_row->lease_number;
	$arrears_bf = $leases_row->arrears_bf;
	$rent_calculation = $leases_row->rent_calculation;
	$deposit = $leases_row->deposit;
	$deposit_ext = $leases_row->deposit_ext;
	$tenant_phone_number = $leases_row->tenant_phone_number;
	$tenant_email = $leases_row->tenant_email;
	$tenant_national_id = $leases_row->tenant_national_id;
	$lease_status = $leases_row->lease_status;
	$tenant_status = $leases_row->tenant_status;
	$account_id = $leases_row->account_id;
	$created = $leases_row->created;
	$message_prefix = $leases_row->message_prefix;

	$lease_start_date = date('jS M Y',strtotime($lease_start_date));

	// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
	$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

}

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));

$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
// get when the lease was active

$lease_invoice = $this->accounts_model->get_invoices_month($lease_id,$invoice_month,$invoice_year,$lease_invoice_id);
// get all the invoiced months
// $parent_invoice_date = $this->accounts_model->get_max_invoice_date($lease_id,$invoice_month,$invoice_year);
$result = '';
// var_dump($lease_invoice); die();

if($lease_invoice->num_rows() > 0)
{
	$water_charge = 0;
	$service_bf = 0;
	$water_charge = 0;
	$water_bf = 0;
	$penalty_charge =0;
	$penalty_bf =0;
	$water_payments = 0;
	$total_penalty = 0;
	$total_water = 0;
	$total_variance = 0;
	$count = 0;
	foreach ($lease_invoice->result() as $key_invoice) {
		# code...


		$invoice_date = $parent_invoice_date = $key_invoice->invoice_date;
		$lease_invoice_id = $key_invoice->lease_invoice_id;
		$invoice_type = $key_invoice->invoice_type;
		$invoice_type_name = $key_invoice->invoice_type_name;
		$document_number = $key_invoice->document_number;


		$invoice_date_date = date('jS F Y',strtotime($invoice_date));

		// if($invoice_type == 1)
		// {
			$count++;
			// service charge
			$invoice_type_name = $key_invoice->invoice_type_name;
			$rent_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);
			$rent_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date,$invoice_type);

			$total_brought_forward = $rent_invoice_amount - $rent_paid_amount;

			$rent_current_invoice =  $this->accounts_model->get_invoice_tenants_current_forward($lease_id,$parent_invoice_date,$invoice_type);
			$rent_current_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date,$invoice_type);

			$total_rent += $rent_current_invoice - $rent_current_payment;

			$result .= '
						<tr>
					        <td>'.$count.'</td>
					        <td>'.$property_name.'</td>
					        <td>'.$rental_unit_name.'</td>
					        <td>'.$invoice_type_name.'</td>
					        <td>'.$rent_current_invoice.'</td>
					        <td>'.$rent_current_invoice.'</td>
					      </tr>
						';


	}
}

$due_date = date('jS F Y', strtotime($invoice_date. ' + 6 days'));

$tenants_response = $this->accounts_model->get_lease_balance($lease_id);
$total_pardon_amount = $tenants_response['total_pardon'];

$account_invoice_amount = $this->accounts_model->get_invoice_tenants_brought_forward($lease_id,$parent_invoice_date);

// var_dump($account_invoice_amount); die();
$account_paid_amount = $this->accounts_model->get_paid_tenants_brought_forward($lease_id,$parent_invoice_date);

$account_todays_payment =  $this->accounts_model->get_today_tenants_paid_current_forward($lease_id,$parent_invoice_date);

if($account_paid_amount == null)
{
	$account_paid_amount = 0;
}
if($account_todays_payment == null)
{
	$account_todays_payment = 0;
}
if(empty($total_pardon_amount))
{
	$total_pardon_amount = 0;
}
$total_brought_forward_account = $account_invoice_amount - $account_paid_amount - $account_todays_payment - $total_pardon_amount;
// var_dump($total_brought_forward_account);die();
$invoice_result = '';
$bf ='';
if($total_brought_forward_account > 0)
{
	$bf = '
			<tr>
			 	<td>'.$property_name.'</td>
				<td>'.$rental_unit_name.'</td>
		        <td colspan="3">Arrears</td>
		        <td>'.number_format($total_brought_forward_account).'</td>
		        <td>'.number_format($total_brought_forward_account).'</td>
		      </tr>
			';
}
$invoice_result = $bf.$result;

$total_bill = $total_rent + $total_deposit + $total_brought_forward_account + $total_service_charge + $total_variance;

?>
<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header ">
     <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" style="margin: 0px"/>
       <!-- <?php echo $company_name;?><br> -->
      <!-- <small class="pull-right">Date: <?php echo $invoice_date_date?></small> -->
    </h2>
  </div>
  <!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info">

  <!-- /.col -->
  <div class="col-sm-4 invoice-col">

    <address>
    <b>NAME : <?php echo strtoupper($tenant_name);?></b><br>
	  TENANT CODE: <?php echo $account_id?>  <br>
	  INVOICE NO : <?php echo $document_number;?><br>
	  INVOICE ON : <?php echo $invoice_date_date;?><br>
	  PAYMENT DUE : <?php echo $due_date?> <br>
    </address>
  </div>


  <!-- /.col -->
   <div class="col-sm-4 invoice-col">
    <!-- <h3><b>TENANT INVOICE</b> </h3> -->
  </div>

  <div class="col-sm-4 invoice-col">

    <address>
      <strong><?php echo $company_name;?></strong><br>
     	<?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?><br/>
        Address : P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?> <br/>
        Phone:  <?php echo $contacts['phone'];?>
        <br/> <?php echo $contacts['email'];?>.<br/>
    </address>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- info row -->
<div class="row invoice-info">
	<div class="col-sm-4 invoice-col">
 </div>
 <div class="col-sm-4 invoice-col">
	<h3><b>TENANT INVOICE</b> </h3>
</div>
<div class="col-sm-4 invoice-col">
</div>
</div>


<!-- Table row -->
<div class="row">
  <div class="col-xs-12 table-responsive">
    <table class="table table-striped">
      <thead>
      <tr>
        <th>#</th>
        <th>Plot / Landloard</th>
        <th>Hse No</th>
        <th>Desc</th>
        <th>Amount</th>
        <th>Total</th>
      </tr>
      </thead>
      <tbody>
     	<?php echo $invoice_result;?>
      </tbody>
    </table>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
  <!-- accepted payments column -->
  <div class="col-xs-6">
    <!-- <p class="lead">Payment Methods:</p> -->
    <!-- <img src="../../dist/img/credit/visa.png" alt="Visa">
    <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
    <img src="../../dist/img/credit/american-express.png" alt="American Express">
    <img src="../../dist/img/credit/paypal2.png" alt="Paypal"> -->

    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
      NOTE : Kindly take note that Accounts are due from the date hereof and you are hereby asked to remit the sum of Kshs <?php echo number_format($total_bill,2)?> in settlement of the invoice hereof.<br>
      <?php echo $message_prefix;?>
    </p>
    <br>
    <br>
    <p><b><?php echo strtoupper($contacts['company_name']);?> </b><br>
    	<br>
    	<br>
    Sign .......................................   </p>
    <br>
  </div>
  <!-- /.col -->
  <div class="col-xs-6">
    <p class="lead">Amount Due <?php echo $due_date?></p>

    <div class="table-responsive">
      <table class="table">
        <tr>
          <th style="width:50%">Subtotal:</th>
          <td>Ksh.<?php echo number_format($total_bill,2);?></td>
        </tr>

        <tr>
          <th>Total:</th>
          <td><strong>Ksh <?php echo number_format($total_bill,2);?></strong> </td>
        </tr>
      </table>
    </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
