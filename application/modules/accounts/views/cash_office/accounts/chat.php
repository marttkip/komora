<?php
$chat_query = $this->accounts_model->get_tenants_chat($tenant_id);
$result = '';
if($chat_query->num_rows() > 0)
{
  foreach ($chat_query->result() as $key => $value) {
    // code...
    $chat_type = $value->chat_type;
    $chat_message = $value->chat_message;
    $created = $value->created;
    $user_id = $value->user_id;

    if($chat_type == 1)
    {
      $username = '';
      $result .= '<div class="direct-chat-msg">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left">Alexander Pierce</span>
                        <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                      </div>
                      <div class="direct-chat-text">
                        Is this template really for free? That unbelievable!
                      </div>
                    </div>';
    }
    else
    {
      $username = $value->personnel_fname;

      $result .= '<div class="direct-chat-msg right">
                    <div class="direct-chat-info clearfix">
                      <span class="direct-chat-name pull-right">Sarah Bullock</span>
                      <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                    </div>
                    <div class="direct-chat-text">
                      You better believe it!
                    </div>
                  </div>';
    }



  }
}

?>

<div class="box box-primary direct-chat direct-chat-primary">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $ticket_number?> Conversation</h3>

      <div class="box-tools pull-right">
        <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span>
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
          <i class="fa fa-comments"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- Conversations are loaded here -->
      <div class="direct-chat-messages">
          <?php echo $result?>
      </div>
      <!--/.direct-chat-messages-->

      <!-- Contacts are loaded here -->
      <div class="direct-chat-contacts">
        <ul class="contacts-list">
          <li>
            <a href="#">
              <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="User Image">

              <div class="contacts-list-info">
                    <span class="contacts-list-name">
                      Count Dracula
                      <small class="contacts-list-date pull-right">2/28/2015</small>
                    </span>
                <span class="contacts-list-msg">How have you been? I was...</span>
              </div>
              <!-- /.contacts-list-info -->
            </a>
          </li>
          <!-- End Contact Item -->
        </ul>
        <!-- /.contatcts-list -->
      </div>
      <!-- /.direct-chat-pane -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <form action="#" method="post">
        <div class="input-group">
          <input type="text" name="message" placeholder="Type Message ..." class="form-control">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary btn-flat">Send</button>
              </span>
        </div>
      </form>
    </div>
    <!-- /.box-footer-->
</div>
