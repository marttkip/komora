<?php

$company_name = $contacts['company_name'];


$all_leases = $this->leases_model->get_lease_detail($lease_id);
$items = '';
$items_charge ='';
$total_bill = 0;
$service_charge = 0;
$penalty_charge =0;
$water_charge =0;
foreach ($all_leases->result() as $leases_row)
{
	$lease_id = $leases_row->lease_id;
	$tenant_unit_id = $leases_row->tenant_unit_id;
	$property_name = $leases_row->property_name;
	// $units_name = $leases_row->units_name;
	$rental_unit_name = $leases_row->rental_unit_name;
	$tenant_name = $leases_row->tenant_name;
	$lease_start_date = $leases_row->lease_start_date;
	$lease_duration = $leases_row->lease_duration;
	$rent_amount = $leases_row->rent_amount;
	$lease_number = $leases_row->lease_number;
	$arrears_bf = $leases_row->arrears_bf;
	$rent_calculation = $leases_row->rent_calculation;
	$deposit = $leases_row->deposit;
	$deposit_ext = $leases_row->deposit_ext;
	$tenant_phone_number = $leases_row->tenant_phone_number;
	$tenant_email = $leases_row->tenant_email;
	$tenant_national_id = $leases_row->tenant_national_id;
	$lease_status = $leases_row->lease_status;
	$tenant_status = $leases_row->tenant_status;
  $account_id = $leases_row->account_id;
	$created = $leases_row->created;
	$message_prefix = $leases_row->message_prefix;

	$lease_start_date = date('jS M Y',strtotime($lease_start_date));

	// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
	$expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));

}


?>
<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header">
       <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" />
       <?php echo $company_name;?>

      <small class="pull-right">Date: <?php echo date('jS M Y')?></small>
    </h2>
  </div>
  <!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info">
  <div class="col-md-4 invoice-col">
    <address>
      <strong><?php echo $company_name;?></strong><br>
     	<?php echo $contacts['location'];?><br/>
        Address : P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?> <br/>
        Phone:  <?php echo $contacts['phone'];?>
        <br/><?php echo $contacts['email'];?>.<br/>
    </address>
  </div>
  <!-- /.col -->
  <div class="col-md-4 invoice-col">

  </div>
  <!-- /.col -->
  <div class="col-md-4 invoice-col">
    <address>
      Name: <strong><?php echo strtoupper($tenant_name);?></strong><br>
      Phone: <?php echo $tenant_phone_number;?><br>
      Email: <?php echo $tenant_email;?> <br>
      Account No: <strong> <?php echo $account_id;?> </strong>
    </address>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<!-- info row -->
<div class="row invoice-info">
	<div class="col-md-3 invoice-col">
 </div>
 <div class="col-md-6 invoice-col">
	<h3><b>TENANT STATEMENT</b> </h3>
</div>
<div class="col-md-3 invoice-col">
</div>
</div>

<!-- Table row -->
<div class="row">
  <div class="col-xs-12 table-responsive">
    <table class="table table-hover table-bordered col-md-12">
		<thead>
			<tr>
				<th>Date</th>
				<th>Description</th>
        <th>Document no</th>
				<th>Invoices</th>
				<th>Payments</th>
				<th>Arrears</th>
			</tr>
		</thead>
	  	<tbody>
	  		<?php echo $tenant_response['result'];?>
	  	</tbody>
	</table>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
  <!-- accepted payments column -->
  <div class="col-xs-7">
    <!-- <p class="lead">Payment Methods:</p> -->
    <br>
     <br>
      <br>
    <p><b>Prepared By :</b> ..............................  <b>Checked By :</b> ..................................  <b>Approved By :</b> ............................................ </p>
    <br>
     <br>
      <br>
     <p><b>Signature :</b> ....................................  <b>Signature :</b> .......................................  <b>Signature :</b> ............................................ </p>
  </div>
  <!-- /.col -->
  <div class="col-xs-5">
    <p class="lead">Payment Summary</p>

    <div class="table-responsive">
      <table class="table">
        <tr>
          <th>Total Amount Payable:</th>
          <td>Ksh.<?php echo number_format($tenant_response['total_invoice'],2);?></td>
        </tr>
        <tr>
          <th>Total Debit Notes:</th>
          <td>Ksh.<?php echo number_format(0,2);?></td>
        </tr>
        <tr>
          <th>Total Credit Notes:</th>
          <td>Ksh. (<?php echo number_format($tenant_response['total_pardon_amount'],2);?>)</td>
        </tr>
        <tr>
          <th>Total Amount Paid:</th>
          <td><strong>Ksh. (<?php echo number_format($tenant_response['total_payment_amount'],2);?>)</strong> </td>
        </tr>

        <tr>
          <th>Total Amount Due:</th>
          <td><strong>Ksh <?php echo number_format($tenant_response['total_arrears'],2);?></strong> </td>
        </tr>
      </table>
    </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
