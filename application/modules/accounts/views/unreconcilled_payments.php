<?php echo $this->load->view('search/search_mpesa_records','', true); ?>
<?php

$result = '';

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;

	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Date Paid</th>
				<th>Serial Number</th>
				<th>Phone Number</th>
				<th>Sender Name</th>
				<th>Account </th>
				<th>Amount </th>
				<th>Recon </th>
				<th colspan="2">Actions</th>
			</tr>
		</thead>
		  <tbody>

	';


	foreach ($query->result() as $leases_row)
	{
		$created = $leases_row->created;
		$mpesa_id = $leases_row->mpesa_id;
		$serial_number = $leases_row->serial_number;
		$account_number = $leases_row->account_number;
		$sender_name = $leases_row->sender_name;
		$sender_phone = $leases_row->sender_phone;
		$amount = $leases_row->amount;
		$account_number = $leases_row->account_number;
		$mpesa_status = $leases_row->mpesa_status;

		$amount_recon = $this->accounts_model->get_amount_reconcilled($mpesa_id);

		$sender_name = str_replace('%20', ' ', $sender_name);

		// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
		$date_sent  = date('jS M Y', strtotime($created));
		$count++;

		if($mpesa_status == 0)
		{
			if($amount_recon > 0)
			{
				$button = '<td><a class="btn btn-sm btn-info" href="'.site_url().'reconcile-payment/'.$mpesa_id.'" > <i class="fa fa-folder"></i> Detail</a></td> <td></td>';
			}
			else {
				$button = '	<td><a class="btn btn-sm btn-info" href="'.site_url().'reconcile-payment/'.$mpesa_id.'" > <i class="fa fa-folder"></i> Detail</a></td>
										<td>
											<a class="btn btn-sm btn-danger" data-toggle="modal" data-target="#refund_payment'.$mpesa_id.'"> <i class="fa fa-arrow-left"></i> Reverse</a>
											<div class="modal fade" id="refund_payment'.$mpesa_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
														<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																	<h4 class="modal-title" id="myModalLabel">Cancel payment</h4>
																</div>
																<div class="modal-body">';
																			$button .= form_open("accounts/cancel_mpesa_payment/".$mpesa_id, array("class" => "form-horizontal"));
																	$button .= '
																	<input type="hidden" name="redirect_url" value="'.$this->uri->uri_string().'">
																		<div class="form-group">
																				<label class="col-md-4 control-label">Action: </label>

																				<div class="col-md-8">
																						<select class="form-control" name="cancel_action_id">
																							<option value="">-- Select action --</option>';

																										if($cancel_actions->num_rows() > 0)
																										{
																												foreach($cancel_actions->result() as $res)
																												{
																														$cancel_action_id = $res->cancel_action_id;
																														$cancel_action_name = $res->cancel_action_name;

																														$button .= '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
																												}
																										}
																								$button .= '
																						</select>
																				</div>
																		</div>

																		<div class="form-group">
																				<label class="col-md-4 control-label">Description: </label>

																				<div class="col-md-8">
																						<textarea class="form-control" name="cancel_description"></textarea>
																				</div>
																		</div>

																		<div class="row">
																			<div class="col-md-8 col-md-offset-4">
																					<div class="center-align">
																							<button type="submit" class="btn btn-primary">Save action</button>
																						</div>
																				</div>
																		</div>';
																		$button .= form_close();
																		$button .= '
																</div>
																<div class="modal-footer">
																		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																</div>
														</div>
												</div>
										</div>

										</td>';
			}

		  $highight = 'default';
		}
    else
		{
			$button = '<td></td><td></td>';
			$highight = 'danger';
		}
		$result .=
					'
						<tr class="'.$highight.'">
							<td>'.$count.'</td>
							<td>'.$date_sent.'</td>
							<td>'.$serial_number.'</td>
							<td>'.$sender_phone.'</td>
							<td>'.$sender_name.'</td>
							<td>'.$account_number.'</td>
							<td>'.number_format($amount ,2).'</td>
							<td>'.number_format($amount_recon ,2).'</td>
							'.$button.'

						</tr>
					';


	}

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}


$accounts_search_title = $this->session->userdata('accounts_search_title');
?>
<!-- href="<?php echo site_url();?>accounts/update_invoices" -->

<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $title;?></h3>

			<div class="box-tools pull-right">

					<a href="<?php echo site_url();?>print-mpesa-transactions" class="btn btn-sm btn-success " target="_blank"  ><i class="fa fa-excel"></i> Print Transactions</a>
			</div>
    </div>
    <div class="box-body">

        	<?php
            $success = $this->session->userdata('success_message');

			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}

			$error = $this->session->userdata('error_message');

			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			$search =  $this->session->userdata('search_mpesa_received');
			if(!empty($search))
			{
				echo '<a href="'.site_url().'accounts/close_mpesa_search" class="btn btn-sm btn-warning">Close Search</a>';
			}

			?>
			<div class="table-responsive">

				<?php echo $result;?>

            </div>
             <div class="panel-footer">
	        	<?php if(isset($links)){echo $links;}?>
	        </div>
		</div>

	</div>

	<script type="text/javascript">
		function get_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_lease_details(lease_id){

			var myTarget2 = document.getElementById("lease_details"+lease_id);
			var button = document.getElementById("open_lease"+lease_id);
			var button2 = document.getElementById("close_lease"+lease_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>
