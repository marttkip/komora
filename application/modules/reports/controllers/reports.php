<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');

		$this->load->model('admin/dashboard_model');
		$this->load->model('real_estate_administration/property_model');

		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('reports/reports_model');



		// if(!$this->auth_model->check_login())
		// {
		// 	redirect('login');
		// }
	}

	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function print_montly_report($property_id, $start_date, $end_date)
	{

		$data['contacts'] = $this->site_model->get_contacts();

		$data['property_id'] = $property_id;
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		$contacts = $data['contacts'];

		$this->load->view('reports/prints/return_report', $data);
	}


	public function personnel_report()
	{
			$data['title'] = 'Personnel Report ';
			$v_data['title'] = $data['title'];
			$data['content'] = $this->load->view('personnel/personnel_report', $v_data, true);

			$this->load->view('admin/templates/general_page', $data);
	}
	public function search_personnel_report()
	{
		$personnel_id = $personnel_id_searched = $this->input->post('personnel_id');

		$search_title = '';

		if(!empty($personnel_id))
		{
			$this->db->where('personnel_id', $personnel_id);
			$personnel_id = ' AND personnel.personnel_id = '.$personnel_id.' ';
		}

		$search = $personnel_id;
		$this->session->set_userdata('personnel_id_searched', $personnel_id_searched);
		$this->session->set_userdata('search_personnel_report', $search);
		redirect('reports/personnel-report');
	}

	public function close_searched_personnel()
	{
		$this->session->unset_userdata('personnel_id_searched');
		$this->session->unset_userdata('search_personnel_report');
		redirect('reports/personnel-report');
	}

// land lord report


	public function landlord_report()
	{
			$data['title'] = 'Personnel Report ';
			$v_data['title'] = $data['title'];
			$data['content'] = $this->load->view('landlord/landlord_report', $v_data, true);

			$this->load->view('admin/templates/general_page', $data);
	}
	public function search_landlord_report()
	{
		$landlord_id = $landlord_id_searched = $this->input->post('landlord_id');

		$search_title = '';

		if(!empty($landlord_id))
		{
			// $this->db->where('landlord_id', $landlord_id);
			$landlord_id = ' AND property_owners.property_owner_id = '.$landlord_id.' ';
		}

		$search = $landlord_id;
		$this->session->set_userdata('landlord_id_searched', $landlord_id_searched);
		$this->session->set_userdata('search_landlord_report', $search);
		redirect('reports/landlord-report');
	}

	public function close_searched_landlord()
	{
		$this->session->unset_userdata('landlord_id_searched');
		$this->session->unset_userdata('search_landlord_report');
		redirect('reports/landlord-report');
	}


	// property reports

	public function property_report()
	{
			$data['title'] = 'Property Report ';
			$v_data['title'] = $data['title'];
			$data['content'] = $this->load->view('property/property_report', $v_data, true);

			$this->load->view('admin/templates/general_page', $data);
	}
	public function search_property_report()
	{
		$property_id = $property_id_searched = $this->input->post('property_id');

		$search_title = '';

		if(!empty($property_id))
		{
			// $this->db->where('property_id', $property_id);
			$property_id = ' AND property.property_id = '.$property_id.' ';
		}

		$search = $property_id;
		$this->session->set_userdata('property_id_searched', $property_id_searched);
		$this->session->set_userdata('search_property_report', $search);
		redirect('reports/property-report');
	}

	public function close_searched_property()
	{
		$this->session->unset_userdata('property_id_searched');
		$this->session->unset_userdata('search_property_report');
		redirect('reports/property-report');
	}



	// tenants_export


	public function tenants_report()
	{
			$data['title'] = 'Tenants Report ';
			$v_data['title'] = $data['title'];
			$data['content'] = $this->load->view('tenants/tenants_report', $v_data, true);

			$this->load->view('admin/templates/general_page', $data);
	}
	public function search_tenants_report()
	{
		$tenants_id = $tenants_id_searched = $this->input->post('tenant_id');

		$search_title = '';

		if(!empty($tenants_id))
		{
			// $this->db->where('tenants_id', $tenants_id);
			$tenants_id = ' AND tenants.tenant_id = '.$tenants_id.' ';
		}
		// var_dump($tenants_id); die();
		$search = $tenants_id;
		$this->session->set_userdata('tenants_id_searched', $tenants_id_searched);
		$this->session->set_userdata('search_tenants_report', $search);
		redirect('reports/tenants-reports');
	}

	public function close_searched_tenants()
	{
		$this->session->unset_userdata('tenants_id_searched');
		$this->session->unset_userdata('search_tenants_report');
		redirect('reports/tenants-reports');
	}

	public function all_transactions()
	{

		$where = 'payments.lease_id = leases.lease_id AND payment_method.payment_method_id = payments.payment_method_id AND leases.tenant_unit_id = tenant_unit.tenant_unit_id AND payments.payment_status = 1 AND tenant_unit.tenant_id = tenants.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0';
		$table = 'payments, leases,rental_unit,property,tenant_unit,tenants,payment_method';

		$search_transactions = $this->session->userdata('search_all_transactions');

		if(!empty($search_transactions))
		{
			$where .=$search_transactions;
		}
		else {
			$where .=' AND payments.payment_date = "'.date('Y-m-d').'" ';
		}

		$segment = 3;
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = site_url().'cash-office/cash-return-form';
		$config['total_rows'] = $this->reports_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;

		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';

		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_transactions($table, $where, $config["per_page"], $page, 'ASC');

		$v_data['branch_name'] = $this->session->userdata('branch_code');

		$v_data['query'] = $query;
		$v_data['page'] = $page;



		$properties = $this->property_model->get_active_property();
		$rs8 = $properties->result();
		$property_list = '';
		foreach ($rs8 as $property_rs) :
			$property_id = $property_rs->property_id;
			$property_name = $property_rs->property_name;
			$property_location = $property_rs->property_location;

		    $property_list .="<option value='".$property_id."'>".$property_name." Location: ".$property_location."</option>";

		endforeach;
		$v_data['property_list'] = $property_list;

		$branches = $this->reports_model->get_all_active_branches();

		$rs9 = $branches->result();
		$branches_list = '';
		foreach($branches->result() as $row):
			$branch_name = $row->branch_name;
			$branch_code = $row->branch_code;

		    $branches_list .="<option value='".$branch_code."'>".$branch_name."</option>";

		endforeach;
		$v_data['branches_list'] = $branches_list;

		// var_dump($where);die();

		$v_data['total_payments'] = $this->reports_model->get_total_cash_collection($where, $table);
		$v_data['normal_payments'] = $this->reports_model->get_normal_payments($where, $table);
		$v_data['payment_methods'] = $this->reports_model->get_payment_methods($where, $table);
		$v_data['total_expenses'] = 0;


		$leases_table= 'leases';
		$leases_where = 'lease_status = 1';
		$v_data['active_leases'] = $this->reports_model->count_items($leases_table, $leases_where);

		$inactive_leases_table= 'leases';
		$inactive_leases_where = 'lease_status <> 1';
		$v_data['inactive_leases'] = $this->reports_model->count_items($inactive_leases_table, $inactive_leases_where);



		// $v_data['properties'] = $this->properties_model->get_all_active_branches();
		$v_data['title'] = "All Transactions";
		$data['content'] = $this->load->view('reports/income/income_transactions', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);

	}

}
?>
