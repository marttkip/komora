<?php

/*$project_rs = $this->projects_model->get_project_detail($project_id);

if($project_rs->num_rows() > 0)
{
	$row = $project_rs->result();
	$project_id = $row[0]->project_id;
	$project_number = $row[0]->project_number;
	$project_title = $row[0]->project_title;
	$project_location = $row[0]->project_location;
	$project_grant_value = $row[0]->project_grant_value;
	$project_start_date = $row[0]->project_start_date;
	$project_end_date = $row[0]->project_end_date;

}*/

$result = '';
	$table = 'leases, payments, tenant_unit, tenants, rental_unit';
	$where = 'payments.payment_status = 1 and payments.payment_date >= '2018-01-10' and payments.payment_date <= '2018-02-10' and payments.lease_id = leases.lease_id
	 and tenant_unit.tenant_unit_id = leases.tenant_unit_id and tenant_unit.tenant_id = tenants.tenant_id and tenant_unit.rental_unit_id = rental_unit.rental_unit_id';
	$select = 'payments.lease_id, leases.tenant_unit_id, tenant_unit.tenant_id, tenant_unit.rental_unit_id, rental_unit.rental_unit_name, tenants.tenant_name, payments.amount_paid';
	$query = $this->reports_model->get_payment_data($table, $where,$select);
	//if users exist display them
	if ($query->num_rows() > 0)
	{
		
		$count = 0;
		$result .= 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>House Number</th>
					<th>Rent Payable</th>
					<th>Tenant Name</th>
					<th>Total Amount Paid</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		
		//get all administrators
		$administrators = $this->users_model->get_active_users();
		if ($administrators->num_rows() > 0)
		{
			$admins = $administrators->result();
		}
		
		else
		{
			$admins = NULL;
		}
		
		foreach ($query->result() as $row)
		{
	
			$lease_id = $row[0]->lease_id;
			$tenant_unit_id = $row[0]->tenant_unit_id;
			$tenant_id = $row[0]->tenant_id;
			$rental_unit_id = $row[0]->rental_unit_id;
			$rental_unit_name = $row[0]->rental_unit_name;
			$tenant_name = $row[0]->tenant_name;
			$amount_paid = $row[0]->amount_paid;

				
			
			
			
			
			
			$count++;
			$result .= 
			'
				<tr>
				
					<td>'.$count.'</td>
				    <td>'.$rental_unit_name.'</td>
				    <td>'6600.00'</td>
				    <td>'.$tenant_name.'</td>
				    <td>'.$amount_paid.'</td>
				
					
					
				</tr> 
			';
		}
		
		$result .= 
		'
					  </tbody>
					</table>
		';
	}

	else
	{
		$result .= "There was no Rent Collected";
	}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $contacts['company_name'];?> | <?php echo $project_title?> REPORT</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
			.receipt_spacing{letter-spacing:0px;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_top_border{border-top: #888888 medium solid; margin-top:1px;}
			.receipt_bottom_border{border-bottom: #888888 medium solid; margin-bottom:1px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
			.align-right{margin:0 auto; text-align: right !important;}
			.align-center
			{
				/*padding: 50px;*/
    			text-align: center;
    			font-weight: bolder;
			}
			.table {
			  margin-bottom: 5px;
			  max-width: 100%;
			  width: 100%;
			}
			.row {
  				margin-left: 0px !important;
    			margin-right: 0px !important;
			}
			body
			{
				font-size: 10px;
			}
			p
			{
				line-height:6px;
			}
		</style>
    </head>
    <body >
        <div class="row">
	        <div  class="col-xs-10  col-xs-offset-1">
		    	<div class="receipt_bottom_border">
		        	<div class="row">
			        	<div class="col-xs-12">
			            	<img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
			            </div>
			        </div>
			    	<div class="row">
			        	<div class="col-md-12 center-align">
			            	<strong>
			                	<?php echo $contacts['company_name'];?><br/>
			                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
			                    E-mail: <?php echo $contacts['email'];?>.
			                   
			                </strong>
			            </div>
			        </div>
		        </div>

		        <div class="row receipt_bottom_border">
		        	<div class="col-md-12 center-align">
		            	<strong>
		            		<h5><?php echo strtoupper($project_title);?> <br> CASUAL REPORT</h5>
		            	</strong>
		            </div>
		        </div>
		    	
		        <div class="row receipt_bottom_border" style="margin-bottom: 10px;margin-top: 5px; font-size: 10px;">
		        	<div class="col-xs-12 ">
		        		<?php echo $result;?>
		            </div>
		        </div>
		       <div class="row" >
		       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
		       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
		                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
		                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
		            </div>
		       </div>
		       
		</div>
		</div>
        
    </body>
    
</html>

