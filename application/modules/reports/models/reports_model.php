<?php

class Reports_model extends CI_Model
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	/*
	*	Retrieve all tenants
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_payment_data($table, $where,$select,$group_by=NULL)
	{
		$this->db->from($table);
		$this->db->select($select);
		$this->db->where($where);
		if($group_by != NULL)
		{
			$this->db->group_by($group_by);
		}
		$query = $this->db->get('');

		return $query;
	}

	public function get_list_items($table, $where, $order)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order,'asc');
		$query = $this->db->get('');

		return $query;
	}

	public function get_creditor_service_amounts($creditor_service_id,$month_id,$year,$property_owner_id=null)
	{
			$prop = $this->session->userdata('property_search_id');
		if(!empty($prop))
		{
			$add = $this->session->userdata('property_search_id');
		}
		else
		{
			$add = '';
		}

		if(!empty($property_owner_id))
		{
			$add .= ' AND property_owner_id ='.$property_owner_id;
		}
		else
		{
			$add = '';
		}


		if($creditor_service_id == NULL)
		{
			$this->db->select('SUM(creditor_account_amount) AS total_amount');
			$this->db->where('creditor_account.creditor_account_status = 1 AND creditor_account.transaction_type_id = 1 AND creditor_account.property_id = property.property_id AND YEAR(creditor_account.creditor_account_date) = '.$year.' AND MONTH(creditor_account.creditor_account_date) = "'.$month_id.'"'.$add);
		}
		else
		{
			$this->db->select('SUM(creditor_account_amount) AS total_amount');
			$this->db->where('creditor_account.creditor_account_status = 1 AND creditor_account.transaction_type_id = 1 AND creditor_account.property_id = property.property_id AND YEAR(creditor_account.creditor_account_date) = '.$year.' AND MONTH(creditor_account.creditor_account_date) = "'.$month_id.'" AND creditor_service_id ='.$creditor_service_id.' '.$add);

		}


		$query = $this->db->get('creditor_account,property');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}

		return $total_amount;

	}

	public function get_inflow_service_amounts($inflow_service_id,$month_id,$year)
	{
				$prop = $this->session->userdata('property_inflow_id');
		if(!empty($prop))
		{
			$add = $this->session->userdata('property_inflow_id');
		}
		else
		{
			$add = '';
		}


		$this->db->select('SUM(inflow_account_amount) AS total_amount');
		$this->db->where('inflow_account.inflow_account_status = 1 AND inflow_account.transaction_type_id = 1 AND YEAR(inflow_account.inflow_account_date) = '.$year.' AND MONTH(inflow_account.inflow_account_date) = "'.$month_id.'" AND inflow_service_id ='.$inflow_service_id.' '.$add);
		$query = $this->db->get('inflow_account');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}

		return $total_amount;

	}

	public function get_creditor_previous_service_amounts($year)
	{
				$prop = $this->session->userdata('property_search_id');
		if(!empty($prop))
		{
			$add = $this->session->userdata('property_search_id');
		}
		else
		{
			$add = '';
		}


		$this->db->select('SUM(creditor_account_amount) AS total_amount');
		$this->db->where('creditor_account.creditor_account_status = 1 AND creditor_account.transaction_type_id = 1 AND YEAR(creditor_account.creditor_account_date) < '.$year.' '.$add);
		$query = $this->db->get('creditor_account');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}

		return $total_amount;

	}
	public function get_amount_collected_invoice_type($invoice_type_id,$month_id,$year,$property_owner_id=null,$property_id = null)
	{
        $prop = $this->session->userdata('search_property');
		if(!empty($prop))
		{
			$add = $this->session->userdata('search_property');
		}
		else
		{
			$add = '';
		}


		if(!empty($property_owner_id))
		{
			$add .= ' AND property.property_owner_id ='.$property_owner_id;
		}
		else
		{
			$add .= '';
		}

		if(!empty($property_id))
		{
			$add .= ' AND property.property_id ='.$property_id;
		}
		else
		{
			$add .= '';
		}

		if($invoice_type_id == NULL)
		{
			$where = 'payments.payment_status = 1 AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id  AND MONTH(payments.payment_date) = \''.$month_id.'\' AND YEAR(payments.payment_date) =  '.$year.'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0  '.$add;

		}
		else
		{
			$where = 'payments.payment_status = 1 AND payments.cancel = 0 AND payment_item.payment_id = payments.payment_id  AND MONTH(payments.payment_date) = \''.$month_id.'\' AND YEAR(payments.payment_date) =  '.$year.'  AND payments.lease_id = leases.lease_id AND leases.rental_unit_id = rental_unit.rental_unit_id AND rental_unit.property_id = property.property_id AND payments.cancel = 0  AND payment_item.invoice_type_id = '.$invoice_type_id.' '.$add;
		}

		// var_dump($where); die();
		$this->db->select('SUM(payment_item.amount_paid) AS total_amount');
		$this->db->where($where);
		$query = $this->db->get('payments,payment_item,leases,rental_unit,property');
		$total_amount = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
				# code...
				$total_amount = $key->total_amount;
			}
		}

		return $total_amount;
	}



	// reports

	/*
	*	Retrieve visits
	*	@param string $table
	* 	@param string $where
	*	@param int $per_page
	* 	@param int $page
	*
	*/
	public function get_all_transactions($table, $where, $per_page, $page, $order = NULL)
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('payments.*,payment_method.payment_method AS payment_method, tenants.*, rental_unit.rental_unit_name,property.property_name, leases.*');
		$this->db->where($where);
		$this->db->order_by('payments.payment_date','ASC');
		// $this->db->group_by('visit.visit_id');
		$query = $this->db->get('', $per_page, $page);

		return $query;
	}

	/*
	*	Retrieve all active branches
	*
	*/
	public function get_all_active_branches()
	{
		//retrieve all users
		$this->db->from('branch');
		$this->db->where('branch_status = 1');
		$this->db->order_by('branch_name','ASC');
		$query = $this->db->get();

		return $query;
	}


	/*
	*	Retrieve total revenue
	*
	*/
	public function get_total_cash_collection($where, $table, $page = NULL)
	{
		//payments
		$table_search = $this->session->userdata('all_transactions_tables');

		if($page != 'cash')
		{
			$where .= ' AND payments.cancel = 0';
		}
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}

		else
		{
			$this->db->from($table);
		}
		$this->db->select('SUM(payments.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();

		$cash = $query->row();
		$total_paid = $cash->total_paid;
		if($total_paid > 0)
		{
		}

		else
		{
			$total_paid = 0;
		}

		return $total_paid;
	}

	/*
	*	Retrieve total revenue
	*
	*/
	public function get_normal_payments($where, $table, $page = NULL)
	{
		if($page != 'cash')
		{
			$where .= ' AND payments.cancel = 0';
		}
		//payments
		$table_search = $this->session->userdata('all_transactions_tables');
		if((!empty($table_search)) || ($page == 'cash'))
		{
			$this->db->from($table);
		}

		else
		{
			$this->db->from($table);
		}
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get();

		return $query;
	}
	public function get_payment_methods()
	{
		$this->db->select('*');
		$query = $this->db->get('payment_method');

		return $query;
	}

}
?>
