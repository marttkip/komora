<?php

$property_owner_id = $landlord_id;
$received_where = 'property_owner_id ='.$property_owner_id;
$received_table = 'property';
$total_properties = $this->dashboard_model->count_items($received_table, $received_where);



$received_where = 'rental_unit_status = 1 AND  rental_unit.property_id = property.property_id AND property.property_owner_id ='.$property_owner_id;
$received_table = 'rental_unit,property';

$total_rental_units = $this->dashboard_model->count_items($received_table, $received_where);


$received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = leases.rental_unit_id AND rental_unit.property_id = property.property_id AND leases.lease_id IN (SELECT lease_invoice.lease_id FROM lease_invoice WHERE invoice_month = "'.date('m').'" AND invoice_year = "'.date('Y').'") AND property.property_owner_id ='.$property_owner_id;
$received_table = 'rental_unit,leases,property';

$occupied_rental_units = $this->dashboard_model->count_items($received_table, $received_where,null);



$received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND tenant_unit.tenant_unit_id NOT IN (SELECT leases.tenant_unit_id FROM leases WHERE leases.lease_status = 1) AND rental_unit.property_id = property.property_id AND property.property_owner_id ='.$property_owner_id;
$received_table = 'rental_unit,tenant_unit,property';

$unoccupied_rental_units = $this->dashboard_model->count_items($received_table, $received_where);

// percentage
// var_dump($occupied_rental_units); die();

if($total_rental_units > 0 AND $occupied_rental_units > 0)
{
 $occupied_percentage = ($occupied_rental_units / $total_rental_units) * 100;
}
else
{
  $occupied_percentage = 0;
}



$received_where = 'rental_unit_status = 1 AND rental_unit.rental_unit_id = leases.rental_unit_id  AND leases.lease_status = 1 AND leases.vacated_on = "0000-00-00" AND rental_unit.property_id = property.property_id AND property.property_owner_id ='.$property_owner_id;
$received_table = 'rental_unit,leases,property';

$active_leases = $this->dashboard_model->count_items($received_table, $received_where);



$revenue_percentage = 1;//$total_payments/$total_invoices * 100;



// $chart_array = array();

$chartExpenses = '';
$chartIncomes = '';
$total_labels= '';

for ($i = 6; $i >= 0; $i--) {
  // code...
  $months = date("Y-m", strtotime( date( 'Y-m-d' )." -$i months"));
  $months_explode = explode('-', $months);
  $year = $months_explode[0];
  $month = $months_explode[1];
  $last_visit = date('M Y',strtotime($months));

  $total_amount = $this->reports_model->get_creditor_service_amounts(NULL,$month,$year,$property_owner_id);
  $total_tenant_inflow_amt = $this->reports_model->get_amount_collected_invoice_type(NULL,$month,$year,$property_owner_id);

  $total_labels .=  "'".$last_visit."',";

  if(empty($total_tenant_inflow_amt))
  {
    $total_tenant_inflow_amt = 0;
  }

  if(empty($total_amount))
  {
    $total_amount = 0;
  }

  $chartIncomes .= $total_tenant_inflow_amt.',';
  $chartExpenses .= $total_amount.',';
}

// var_dump($chartExpenses);die();

?>
	<div class="row">
		<div class="col-md-12 ">
			 <a href="<?php echo site_url();?>reports/landlord-report" class="btn btn-sm btn-warning pull-right "><i class="fa fa-arrow-left"></i> Back to Landlords </a>
		</div>
	</div>

	<div class="row">
       <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo number_format($occupied_percentage);?> %</h3>

              <p>Rental Units Occupation</p>
            </div>
            <div class="icon">
              <i class="fa fa-building"></i>
            </div>
            <a href="<?php echo site_url().'property-manager/rental-units'?>" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $active_leases;?></h3>

              <p>Active Leases</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo site_url().'lease-manager/leases'?>" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>0</h3>

              <p>Complaints Feedback</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo number_format(0)?><sup style="font-size: 20px">%</sup></h3>

              <p>Month's Payable</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo site_url().'accounts/tenants-invoices'?>" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
  </div>

   <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Monthly Recap Report</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <div class="btn-group">
              <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-wrench"></i></button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <p class="text-center">
                <strong>Cash flow for past six months</strong>
              </p>

              <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="salesChart2" style="height: 180px;"></canvas>
              </div>
              <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->
           
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>

<script type="text/javascript">
  $(function () {

  'use strict';

 

  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  // -----------------------
  // - MONTHLY SALES CHART -
  // -----------------------

  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = $('#salesChart2').get(0).getContext('2d');
  // This will get the first returned node in the jQuery collection.
  var salesChart       = new Chart(salesChartCanvas);

  var salesChartData = {
    labels  : [<?php echo $total_labels;?>],
    datasets: [
      {
        label               : 'Expense',
        fillColor           : 'rgb(210, 214, 222)',
        strokeColor         : 'rgb(210, 214, 222)',
        pointColor          : 'rgb(210, 214, 222)',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgb(220,220,220)',
        data                : [<?php echo $chartExpenses?>]
      },
      {
        label               : 'Income / Cash Flow',
        fillColor           : 'rgba(60,141,188,0.9)',
        strokeColor         : 'rgba(60,141,188,0.8)',
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : [<?php echo $chartIncomes?>]
      }
    ]
  };

  var salesChartOptions = {
    // Boolean - If we should show the scale at all
    showScale               : true,
    // Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines      : false,
    // String - Colour of the grid lines
    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    // Number - Width of the grid lines
    scaleGridLineWidth      : 1,
    // Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    // Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines  : true,
    // Boolean - Whether the line is curved between points
    bezierCurve             : true,
    // Number - Tension of the bezier curve between points
    bezierCurveTension      : 0.3,
    // Boolean - Whether to show a dot for each point
    pointDot                : false,
    // Number - Radius of each point dot in pixels
    pointDotRadius          : 4,
    // Number - Pixel width of point dot stroke
    pointDotStrokeWidth     : 1,
    // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,
    // Boolean - Whether to show a stroke for datasets
    datasetStroke           : true,
    // Number - Pixel width of dataset stroke
    datasetStrokeWidth      : 2,
    // Boolean - Whether to fill the dataset with a color
    datasetFill             : true,
    // String - A legend template
    legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio     : true,
    // Boolean - whether to make the chart responsive to window resizing
    responsive              : true
  };

  // Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);



    // -------------
  // - PIE CHART -
  // -------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $('#pieChart2').get(0).getContext('2d');
  var pieChart       = new Chart(pieChartCanvas);
  var PieData        = [
    {
      value    : 700,
      color    : '#f56954',
      highlight: '#f56954',
      label    : 'Chrome'
    },
    {
      value    : 500,
      color    : '#00a65a',
      highlight: '#00a65a',
      label    : 'IE'
    },
    {
      value    : 400,
      color    : '#f39c12',
      highlight: '#f39c12',
      label    : 'FireFox'
    },
    {
      value    : 600,
      color    : '#00c0ef',
      highlight: '#00c0ef',
      label    : 'Safari'
    },
    {
      value    : 300,
      color    : '#3c8dbc',
      highlight: '#3c8dbc',
      label    : 'Opera'
    },
    {
      value    : 100,
      color    : '#d2d6de',
      highlight: '#d2d6de',
      label    : 'Navigator'
    }
  ];
  var pieOptions     = {
    // Boolean - Whether we should show a stroke on each segment
    segmentShowStroke    : true,
    // String - The colour of each segment stroke
    segmentStrokeColor   : '#fff',
    // Number - The width of each segment stroke
    segmentStrokeWidth   : 1,
    // Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    // Number - Amount of animation steps
    animationSteps       : 100,
    // String - Animation easing effect
    animationEasing      : 'easeOutBounce',
    // Boolean - Whether we animate the rotation of the Doughnut
    animateRotate        : true,
    // Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale         : false,
    // Boolean - whether to make the chart responsive to window resizing
    responsive           : true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio  : false,
    // String - A legend template
    legendTemplate       : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
    // String - A tooltip template
    tooltipTemplate      : '<%=value %> <%=label%> users'
  };
  // Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);

  });

</script>
