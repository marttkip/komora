<?php
$property_id = set_value('property_id');
$creditor_account_date = set_value('creditor_account_date');
$year = $this->reports_model->generate_financial_year();


if(!empty($this->session->userdata('financial_year_search')))
{
	$current_year = $this->session->userdata('financial_year_search');
	$financial_year =  $current_year;

	$next_financial_year = $current_year + 1;
    $previous_financial_year = $current_year - 1;
}
else
{
	$financial_year =  date('Y');

	$next_financial_year = date('Y') + 1;
    $previous_financial_year = date('Y') - 1;
}
// $financial_year = 2016;
// $next_financial_year = 2017;

// get balance of the previous year 
$previous_creditor_amount = $this->reports_model->get_creditor_previous_service_amounts($financial_year);
$previous_tenants_inflow_amount = $this->reports_model->get_tenants_previous_inflow_amounts($financial_year);
$previous_owners_inflow_amount = $this->reports_model->get_owners_previous_inflow_amounts($financial_year);


$balance  =  $previous_owners_inflow_amount + $previous_tenants_inflow_amount - $previous_creditor_amount;

$months = '';
$count = $invoice_count= 0;
$total_months =count($year);
$totals_array = array();
for($r=0;$r<$total_months;$r++)
{
	$month_id = $year[$r];
	$totals_array[$month_id]  =0;

	if($month_id === 1 OR $month_id === 2 OR $month_id === 3)
	{
		$financial_year = $next_financial_year;
	}

	$month_name  = $this->reports_model->get_month_name($month_id);
	$months .='<th>'.$financial_year.' '.$month_name.'</th>'; 
}

// bank balances 

$bank_balance = 0;

// end of bank balance

//display al creditors 
$result ='';

if($all_creditor_services->num_rows() > 0)
{
	//var_dump($all_creditor_services);die();
	foreach($all_creditor_services->result() as $creditors_services)
	{
		$creditor_service_id = $creditors_services->creditor_service_id;//echo $creditor_service_id;die();
		$creditor_service_name = $creditors_services->creditor_service_name;
		$count++;
		$result .='<tr>
						<td>'.$count.'</td>
						<td>'.$creditor_service_name.'</td>
					';
		$creditor_result = '';

		if(!empty($this->session->userdata('financial_year_search')))
		{
			$current_year = $this->session->userdata('financial_year_search');
			$financial_year_two =  $current_year;

			$next_financial_year_two = $current_year + 1;
		}
		else
		{
			$financial_year_two =  date('Y');

			$next_financial_year_two = date('Y') + 1;
		}

        // var_dump($next_financial_year_two); die();

		for($r=0;$r<$total_months;$r++)
		{
			$month_id = $original_month_id = $year[$r];
			if(strlen($month_id)!=2)
			{
				$month_id = '0'.$month_id;
			}
			$total_amount = $service_total = 0;
			if($original_month_id === 1 OR $original_month_id === 2 OR $original_month_id === 3)
			{
				$financial_year_two = $next_financial_year_two;
			}
			$total_amount = $this->reports_model->get_creditor_service_amounts($creditor_service_id,$month_id,$financial_year_two);

			
			$totals_array[$original_month_id] += $total_amount;
			$result .='
				<td>'.number_format($total_amount).'</td>
			';

		}
		
		$result .= '</tr>
					';
	}
	
	$result.='<tr class="info">
			<th colspan="2"><strong>Total Outflow</strong></th>';
	for($r=0;$r<$total_months;$r++)
	{
		
		$month_id = $year[$r];
		$month_amount = $totals_array[$month_id];
		$result .='
					<td>'.number_format($month_amount).'</td>'; 
	}
	$result.='</tr>';
	
}
//inflows 
$income_result ='';
$total_incomes = array();
for($r=0;$r<$total_months;$r++)
{
	$month_id = $original_month_id = $year[$r];
	$total_incomes[$month_id] = 0;
}
if($all_invoice_types->num_rows() > 0)
{
	foreach($all_invoice_types->result() as $invoice_types)
	{
		$invoice_type_id = $invoice_types->invoice_type_id;
		$invoice_type_name = $invoice_types->invoice_type_name;
		$invoice_count++;
		$income_result .='
		<tr>
			<td>'.$invoice_count.'</td>
			<td>'.$invoice_type_name.'</td>
		';
		$creditor_result = '';
		// var_dump($total_months); die();
		$month_item=0;

		if(!empty($this->session->userdata('financial_year_search')))
		{
			$current_year = $this->session->userdata('financial_year_search');
			$financial_year_two =  $current_year;

			$next_financial_year_two = $current_year + 1;
		}
		else
		{
			$financial_year_two =  date('Y');

			$next_financial_year_two = date('Y') + 1;
		}
		for($r=0;$r<$total_months;$r++)
		{
			$month_id = $month_item = $year[$r];
			if(strlen($month_id)!= 2)
			{
				$month_id = '0'.$month_id;
			}
			$total_amount = 0;


			// get total_amount made on the invoice type

			if($month_item === 1 OR $month_item === 2 OR $month_item === 3)
			{
				$financial_year_two = $next_financial_year_two;
			}

			$total_tenant_inflow_amt = $this->reports_model->get_amount_collected_invoice_type($invoice_type_id,$month_item,$financial_year_two);

			$total_owner_inflow_amt = $this->reports_model->get_amount_owners_collected_invoice_type($invoice_type_id,$month_item,$financial_year_two);
           // var_dump($invoice_type_id."sdhakjda");

			$total_inflow_amt = $total_tenant_inflow_amt + $total_owner_inflow_amt;

			$income_result .='
				<td>'.number_format($total_inflow_amt).'</td>
			';
			$total_incomes[$month_item] += $total_inflow_amt;
			

		}
		$income_result .= '</tr>';
	}

    

    if($all_other_inflows->num_rows() > 0)
    {
        //var_dump($all_other_inflows);die();
        foreach($all_other_inflows->result() as $inflow_services)
        {
            $inflow_id = $inflow_services->inflow_service_id;//echo $inflow_id;die();
            $inflow_name = $inflow_services->inflow_service_name;
            $count++;
            $income_result .='<tr>
                            <td>'.$count.'</td>
                            <td>'.$inflow_name.'</td>
                        ';

            if(!empty($this->session->userdata('financial_year_search')))
            {
                $current_year = $this->session->userdata('financial_year_search');
                $financial_year_two =  $current_year;

                $next_financial_year_two = $current_year + 1;
            }
            else
            {
                $financial_year_two =  date('Y');

                $next_financial_year_two = date('Y') + 1;
            }

            // var_dump($next_financial_year_two); die();

            for($r=0;$r<$total_months;$r++)
            {
                $month_id = $original_month_id = $year[$r];
                if(strlen($month_id)!=2)
                {
                    $month_id = '0'.$month_id;
                }
                if($original_month_id === 1 OR $original_month_id === 2 OR $original_month_id === 3)
                {
                    $financial_year_two = $next_financial_year_two;
                }
                $total_amount = $this->reports_model->get_inflow_service_amounts($inflow_id,$month_id,$financial_year_two);

                
                $income_result .='
                    <td>'.number_format($total_amount).'</td>
                ';

                $total_incomes[$original_month_id] +=  $total_amount;

            // var_dump($income_result); die();

            }
            
            $income_result .= '</tr>
                        ';
        }
        
     
        
    }

	$income_result .='<tr class="info">
					<th colspan="2">Total Inflows</th>';
	for($r=0;$r<$total_months;$r++)
	{
		$month_id = $year[$r];
		$month_payment_amount = $total_incomes[$month_id];
        // openning balance from the previous



		$income_result .='
					<th>'.number_format($month_payment_amount+$balance).'</th>'; 
		
	}
	$income_result.='</tr>';
	/*
	 $income_result .='   	
    </tbody>
</table>
';*/



	
}

// var_dump($total_incomes);die();
?>

        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Search Period</h2>
            </header>
            <div class="panel-body">
                <?php echo form_open("administration/reports/search_income_expense_statement", array("class" => "form-horizontal"));?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-lg-5 control-label"> Start Period* </label>
                            
                            <div class="col-lg-7">
        
                                <select class="form-control" name="financial_year">
                                    <option value="2015">2015</option>
                                    <option value="2016" selected>2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                </select>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Property *</label>
                            
                            <div class="col-md-8">
                                <select class="form-control" name="property_id">
                                    <option value="">-- Select property --</option>
                                    <?php
                                    if($properties->num_rows() > 0)
                                    {
                                        $property = $properties->result();
                                        
                                        foreach($property as $res)
                                        {
                                            $db_property_id = $res->property_id;
                                            $property_name = $res->property_name;
                                            
                                            if($db_property_id == $property_id)
                                            {
                                                echo '<option value="'.$db_property_id.'" selected>'.$property_name.'</option>';
                                            }
                                            
                                            else
                                            {
                                                echo '<option value="'.$db_property_id.'">'.$property_name.'</option>';
                                            }
                                        }
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-12">
                        <div class="form-actions center-align">
                            <button class="submit btn btn-primary" type="submit">
                                Search Statement
                            </button>
                        </div>
                    </div>
                </div>
                <?php echo form_close();?>
            </div>
        </section>
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title"><?php echo $title;?></h2>
                <a href="<?php echo site_url()?>print-income-statement" target="_blank" class="btn btn-sm btn-warning pull-right" style="margin-top: -25px;">Print Statement</a>
            </header>
        
            <!-- Widget content -->
            <div class="panel-body" style="font-size: 12px !important;">
               <div class="row">
                    <div class="center-align">
                        <?php
                        if(!empty($this->session->userdata('search_title_data')))
                        {
                            echo $this->session->userdata('search_title_data');
                        }
                        else
                        {
                            echo 'Showing report for all properties Financial Year '.date('Y');
                        }
        
                        $search =  $this->session->userdata('search_title_data');
                        if(!empty($search))
                        {
                            echo '<a href="'.site_url().'administration/reports/close_income_search" class="btn btn-sm btn-warning">Close Search</a>';
                        }
                        ?>
                    </div>
               </div>
                <tr>
                    <td colspan="14">INFLOWS</td>
                </tr>
                <?php
                 $income_result_changed ='
                    <table class="table table-condensed table-striped table-hover" id="customers">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Invoice Type</th>
                                '.$months.'
                            </tr>
                        </thead>
                        <tbody>
                        ';
        
                    $income_result_changed .= '<tr>
                                        <td colspan="2">Opening Balance</td>';


                                        $changed_bal_amount = $balance;
                                        $cummulative_amount_account_balance = 0;
                                        $total_months = $total_months -1;

                                        // $balance_bf = 100;
                                        $r = 0;

                                        foreach ($year as $key) {

                                            $cummulative_amount_account = $total_incomes[$key];
                                            $cummulative_amount_account_balance = $totals_array[$key];

                                            // var_dump($year); die();
                                            // if($key == 10)
                                            // {
                                            //     var_dump($changed_amount); die();
                                            // }

                                            $income_result_changed .='
                                                    <td>'.number_format($changed_bal_amount ).'</td>'; 

                                            $changed_bal_amount = $changed_bal_amount+$cummulative_amount_account - $cummulative_amount_account_balance;
                                        }

                    $income_result_changed.='</tr>';
        
                    echo $income_result_changed;
        
                ?>
                <?php echo $income_result;?>
                <tr>
                    <td colspan="14">OUTFLOWS</td>
                </tr>
                <?php echo $result;?>
        
                <?php
        
                $income_result_changed_bottom = '<tr class="primary">
                                        <td colspan="2">Bank Balance</td>';
                                        $changed_amount = $balance;
                                        $changed_amount_balance = 0;
                                        $total_months = 12;

                                         
                                        for($r=0;$r<$total_months;$r++)
                                        {
                                            $month_id = $year[$r];
                                                $cummulative_amount_account = $total_incomes[$month_id];
                                                $cummulative_amount_account_balance = $totals_array[$month_id];
                                                $changed_amount = $changed_amount+$cummulative_amount_account - $cummulative_amount_account_balance;

                                                $income_result_changed_bottom .='
                                                        <td><strong>'.number_format($changed_amount).'</strong></td>'; 
                                            
                                        }
                    $income_result_changed_bottom .='</tr>';
        
                    echo $income_result_changed_bottom;
                ?>
        
            </tbody>
        </table>    
    </div>
</section>