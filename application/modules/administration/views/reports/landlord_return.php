<?php

$result = '';
$total_invoice_balance =0;
$total_balance_bf = 0;
$total_rent_payable = 0;

$this_year_item = $this->session->userdata('year');
$this_month_item = $this->session->userdata('month');
$return_date = $this->session->userdata('return_date');
$total_invoice_balance_end =0;
$total_payable_end =0;
$total_arreas_end = 0;
$total_rent_end = 0;
$total_current_invoice = 0;
$total_rent_brought_forward = 0;

// var_dump($month);die();

$parent_invoice_date = date('Y-m-d', strtotime($year.'-'.$month.'-01'));
$previous_invoice_date = date('Y-m-d', strtotime($parent_invoice_date.'-1 month'));

$month_year = date('F Y', strtotime($year.'-'.$month.'-01'));

$count = 0;
//if users exist display them
if ($query->num_rows() > 0)
{


	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>UNIT</th>
				<th>TENANT NAME</th>
				<th>RENT ARREARS</th>
				<th>CURRENT RENT</th>
				<th>INVOICED</th>
				<th>RENT PAID</th>
				<th>BAL C/F</th>
			</tr>
		</thead>
		  <tbody>

	';


		foreach ($query->result() as $leases_row)
		{
			$rental_unit_name = $leases_row->rental_unit_name;
			$rental_unit_id = $leases_row->rental_unit_id;
			// $property_name = $leases_row->property_name;
			// $property_id = $leases_row->property_id;


			$todays_month = $month;//$this->session->userdata('month');
			$todays_year = $year;//$this->session->userdata('year');
			$return_date = '01';//$this->session->userdata('return_date');
			// var_dump($todays_year);die();

			if($todays_month < 9)
			{
				$todays_month = $todays_month;
			}
			$lease_id = $leases_row->lease_id;
			// $tenant_unit_id = $leases_row->tenant_unit_id;
			$rental_unit_id = $leases_row->rental_unit_id;
			$tenant_name = $leases_row->tenant_name;
			$tenant_phone_number = $leases_row->tenant_phone_number;
			$rent_payable = $leases_row->billing_amount;

				$count++;

		if($lease_id > 0)
		{
			// $tenants_response = $this->accounts_model->get_rent_and_service_charge($lease_id);
			$total_pardon_amount = 0;//$tenants_response['total_pardon_amount'];

			// $arrears_invoice_amount =$this->accounts_model->get_rent_invoices_brought_forward($lease_id,$parent_invoice_date,37);
			// $arrears_paid_amount = $this->accounts_model->get_rent_payments_brought_forward($lease_id,$parent_invoice_date,37);

			$total_credit_note =$this->accounts_model->get_rent_credit_note_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);
			$total_debit_note =$this->accounts_model->get_rent_debit_note_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

			// var_dump($rent_invoice_amount);die();

			// $arrears_invoice_amount =  $this->accounts_model->get_current_month_invoice($lease_id,$todays_month,$todays_year,37,$previous_invoice_date,$parent_invoice_date);
			// $arrears_paid_amount = $this->accounts_model->get_current_months_payments($lease_id,$todays_month,$todays_year,37,$previous_invoice_date,$parent_invoice_date);

			// $total_rent_brought_forward = ($rent_invoice_amount +$arrears_invoice_amount+$total_debit_note) - ($rent_paid_amount +$arrears_paid_amount + $total_credit_note) ;
				// var_dump($rent_paid_amount);die();

			$rent_payment_amount = $this->accounts_model->get_rent_payments_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

			$rent_invoice_amount =$this->accounts_model->get_rent_invoices_brought_forward($lease_id,$parent_invoice_date,1,$todays_month,$todays_year,$rental_unit_id);

			$brought_forward = ($rent_invoice_amount+$total_debit_note) - ($rent_payment_amount+$total_credit_note);
			// var_dump($brought_forward);die();
			$rent_current_invoice =  $this->accounts_model->get_current_month_invoice($lease_id,$todays_month,$todays_year,1,$previous_invoice_date,$parent_invoice_date);
			$rent_current_payment = $this->accounts_model->get_current_months_payments($lease_id,$todays_month,$todays_year,1,$previous_invoice_date,$parent_invoice_date);

			$total_current_variance = $rent_current_invoice - $rent_current_payment;

			$total_balance = $brought_forward + $total_current_variance  ;


			$result .=
						'	<tr>
								<td>'.$rental_unit_name.'</td>
								<td>'.$tenant_name.'</td>
								<td>'.number_format($brought_forward,2).'</td>
								<td>'.number_format($rent_payable,2).'</td>
								<td>'.number_format($rent_current_invoice,2).'</td>
								<td>'.number_format($rent_current_payment,2).'</td>
								<td>'.number_format($total_balance,0).'</td>
							</tr>
						';
			// add all the balances


			$total_invoice_balance_end = $total_invoice_balance_end + $total_balance;
			$total_arreas_end = $total_arreas_end + $brought_forward;
			$total_payable_end = $total_payable_end + $rent_current_payment;
			$total_current_invoice += $rent_current_invoice;

			$total_rent_end = $total_rent_end + $rent_payable;
		}
		else{

			$result .=
					'
						<tr>
							<td>'.$rental_unit_name.'</td>
							<td>Vacant House</td>
							<td>0.00</td>
							<td>0.00</td>
							<td>0.00</td>
							<td>0.00</td>
							<td>0.00</td>
						</tr>
					';
		}

}


	$result .='<tr>
					<td></td>
					<td><strong>Totals</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_arreas_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_rent_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_current_invoice,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_payable_end,2).'</strong></td>
					<td style="border-bottom: #888888 medium solid; border-top: #888888 medium solid; margin-bottom:1px;"><strong>'.number_format($total_invoice_balance_end,2).'</strong></td>
				</tr>';

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no items for this month";
}



$property_id = $this->session->userdata('property_id');


//  GET ALL OUT FLOWS OF THE MONTH
$outflows = $this->reports_model->get_property_month_outflows($property_id,$this_month_item,$this_year_item);
$total_outflows = 0;
$outflow_x = 0;


// var_dump($outflows); die();
if($outflows->num_rows() > 0)
{
	$outflow = '';

	foreach ($outflows->result() as $key) {
		# code...

		$account_name = $key->document_number;
		$finance_purchase_description = $key->finance_purchase_description;
		$invoice_amount = $key->finance_purchase_amount;
		$outflow_x++;
		$outflow .='<tr>
						<td>'.$outflow_x.'</td>
						<td>'.$finance_purchase_description.'</td>
						<td>'.$account_name.'</td>
						<td>KES. ('.number_format($invoice_amount,2).')</td>
					</tr>';
		$total_outflows = $total_outflows + $invoice_amount;
	}





	// $outflow .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows ,2).' </strong> </li>';
}else
{
	$outflow ='<tr><td colspan=3>No expenses added</td></tr>';
}


$landlord_payment = $this->reports_model->get_month_landlord_payments($property_id,$this_month_item,$this_year_item);
$landlord_payment_amount = 0;


// var_dump($landlord_payment); die();
if($landlord_payment->num_rows() > 0)
{

	foreach ($landlord_payment->result() as $key) {
		# code...

		$account_name = $key->account_name;
		$landlord_transaction_description = '';
		$invoice_amount = $key->amount_paid;
		$outflow_x++;
		$outflow .='<tr>
						<td>'.$outflow_x.'</td>
						<td>'.$account_name.'</td>
						<td>'.$landlord_transaction_description.'</td>
						<td>KES. ('.number_format($invoice_amount,2).')</td>
					</tr>';
		$total_outflows = $total_outflows + $invoice_amount;
	}
}
$outflow .='<tr>
							<td colspan=2></td>
							<td><b>TOTAL EXPENSES</b></td>
							<td><b>KES. ('.number_format($total_outflows,2).') </b></td>
						</tr>';

$landlord_outflows = $this->reports_model->get_property_month_landlord_outflows($property_id,$this_month_item,$this_year_item);
$total_landlord_outflows = 0;
$x = 0;

$landlord_outflow = '';
// var_dump($landlord_outflows); die();
if($landlord_outflows->num_rows() > 0)
{


	foreach ($landlord_outflows->result() as $key) {
		# code...

		$account_name = $key->document_number;
		$landlord_transaction_description = $key->remarks;
		$invoice_amount = $key->landlord_transaction_amount;
		$x++;
		$landlord_outflow .='<tr>
						<td>'.$x.'</td>
						<td>'.$landlord_transaction_description.'</td>
						<td>'.$account_name.'</td>
						<td>KES. ('.number_format($invoice_amount,2).')</td>
					</tr>';
		$total_landlord_outflows = $total_landlord_outflows + $invoice_amount;
	}




	// $outflow .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_outflows ,2).' </strong> </li>';
}else
{
	$landlord_outflow ='<tr><td colspan=3>No landlord payments</td></tr>';
}
// var_dump($total_outflows); die();

$income = $this->reports_model->get_property_month_income($property_id,$this_month_item,$this_year_item);
$income_amount = 0;
$x = 0;


// var_dump($income); die();
if($income->num_rows() > 0)
{
	$incomes = '';

	foreach ($income->result() as $key) {
		# code...

		$account_name = $key->account_name;
		$landlord_transaction_description = $key->landlord_transaction_description;
		$invoice_amount = $key->landlord_transaction_amount;
		$x++;
		$incomes .='<tr>
						<td>'.$x.'</td>
						<td>'.$account_name.'</td>
						<td>'.$landlord_transaction_description.'</td>
						<td>KES. '.number_format($invoice_amount,2).'</td>
					</tr>';
		$income_amount = $income_amount + $invoice_amount;
	}
}else
{
	$incomes ='<tr><td colspan=3>No other incomes recorded</td></tr>';
}

$landlord_receipt = $this->reports_model->get_month_landlord_receipts($property_id,$this_month_item,$this_year_item);
$landlord_receipt_amount = 0;

// var_dump($landlord_receipt); die();
$landlord_receipts = '';
$_x =0;
if($landlord_receipt->num_rows() > 0)
{
	foreach ($landlord_receipt->result() as $key) {
		# code...


		$property_id = $key->property_id;
		$parent_transaction = $key->parent_transaction;
		if(empty($parent_transaction))
		{
			$account_name = 'Landlord Receipts';
			$this->db->where('property_id',$property_id);
			$query_prop = $this->db->get('property');
			$prop = $query_prop->row();
			$property_name = $prop->property_name;
			$name = $property_name;
		}
		else{
			$account_name = 'Landlord Receipts';
			$this->db->where('property_id',$property_id);
			$query_prop = $this->db->get('property');
			$prop = $query_prop->row();
			$property_name = $prop->property_name;

			$this->db->where('property_id',$parent_transaction);
			$query_prop = $this->db->get('property');
			$prop = $query_prop->row();
			$property_to_name = $prop->property_name;

			$name = $property_to_name.' - '.$property_name;
		}

		$landlord_transaction_description = $key->remarks;
		$invoice_amount = $key->landlord_receipt_amount;
		$receipt_number = $key->document_number;
		$_x++;
		$landlord_receipts .='<tr>
						<td>'.$_x.'</td>
						<td>'.$landlord_transaction_description.'</td>
						<td>'.$receipt_number.'</td>
						<td>KES. '.number_format($invoice_amount,2).'</td>
					</tr>';
		$landlord_receipt_amount = $landlord_receipt_amount + $invoice_amount;
	}
}

// landlord receipt_spacing



// $percent = $this->reports_model->get_manager_percent($property_id);
// $commission = $total_payable_end * ($percent/100);
// $x++;
// $outflow .='<tr>
// 						<td>'.$x.'</td>
// 						<td colspan=2>Agent Commission '.$percent.'% </td>
// 						<td>KES. '.number_format($commission,2).'</td>
// 					</tr>';
// $total_outflows += $commission;
//
//
// $outflow .='<tr>
// 				<td colspan=2>Total<td>
// 				<td>KES. '.number_format($total_outflows,2).'</td>
// 		</tr>';



$leases = $this->reports_model->get_property_month_leases($property_id,$this_month_item,$this_year_item);
$total_leases = 0;
$x = 0;
$total_deposit_refund = 0;
if($leases->num_rows() > 0)
{
	$closed_leases = '';

	foreach ($leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		$remarks = $key->remarks;
		$expense_amount = $key->expense_amount;
		$lease_id = $key->lease_id;

		$deposit_amount = $this->reports_model->get_lease_deposit($lease_id);
		$x++;
		$closed_leases .='  <tr>
								<td>'.$x.'</td>
								<td>'.$rental_unit_name.'</td>
								<td>'.$tenant_name.'</td>
								<td>'.$remarks.'</td>
								<td>KES. '.number_format($expense_amount,2).'</td>
								<td>KES. '.number_format($deposit_amount - $expense_amount,2).'</td>
							</tr>';

		$total_leases = $total_leases + $expense_amount;
		$total_deposit_refund = $total_deposit_refund + ($deposit_amount - $expense_amount);
	}
	$closed_leases .='<tr>
						<td colspan=3>Total<td>
						<td>KES. '.number_format($total_leases,2).'</td>
						<td>KES. '.number_format($total_deposit_refund,2).'</td>
				</tr>';
	// $closed_leases .='<li>Amount banked - <strong style="border-bottom: #888888 medium solid;margin-bottom:1px;"> KES '.number_format($total_payable_end - $total_leases ,2).' </strong> </li>';
}else
{
	$closed_leases ='<tr><td colspan=3>No closed leases</td></tr>';
}



$new_leases = $this->reports_model->get_property_month_new_leases($property_id,$this_month_item,$this_year_item);
$total_new_lease = 0;
$total_deposit = 0;
$x = 0;
if($new_leases->num_rows() > 0)
{
	$closed_new_leases = '';

	foreach ($new_leases->result() as $key) {
		# code...

		$rental_unit_name = $key->rental_unit_name;
		$tenant_name = $key->tenant_name;
		// $remarks = $key->remarks;
		// $expense_amount = $key->expense_amount;
		$lease_id = $key->lease_id;

		$deposit_amount = $key->deposit_amount;//$this->reports_model->get_lease_deposit($lease_id);

		$x++;
		$closed_new_leases .='<tr>
						<td>'.$x.'</td>
						<td>'.$rental_unit_name.'</td>
						<td>'.$tenant_name.'</td>
						<td>'.number_format($deposit_amount,2).'</td>
					</tr>';
		// if($expense_amount > 0)
		// {

		// }
		$total_new_lease = $total_new_lease + $deposit_amount;
	}

	$closed_new_leases .='<tr>
					<td colspan="2"></td>
					<td><b>TOTAL</b></td>
					<td>'.number_format($total_new_lease,2).'</td>
				</tr>';
}else
{
	$closed_new_leases ='<tr><td colspan=4>No expense added</td></tr>';
}
?>
<!DOCTYPE html>
<html lang="en">
      <head>
        <title><?php echo $contacts['company_name'];?> | Property Statement</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
				<style type="text/css">
				body {
								font-family: "tohoma";
								font-size: 14px;
								line-height: 0;
								color: #333333;
								background-color: #ffffff;
						}
						.receipt_spacing {
								letter-spacing: 0px;
								font-size: 14px;
						}
						.center-align{margin:0 auto; text-align:center;}

						.receipt_bottom_border{border-bottom: #888888 medium solid;}
						.row .col-md-12 table {
							border:solid #000 !important;
							border-width:1px 0 0 1px !important;
							/* font-size:10px; */
						}
						.row .col-md-12 th, .row .col-md-12 td {
							border:1px solid #000 !important;
							border-width:0 1px 1px 0 !important;
						}
						.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
						{
							 padding: 1px;
								 border:1px solid #000 !important;
						}
						/* .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td
						{

						} */

				.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
				.title-img{float:left; padding-left:30px;}
				/*img.logo{margin:0 auto;}*/
				</style>
    </head>
     <body class="receipt_spacing">
			 <div class="row " >
					<div class="col-xs-6" >
							 <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo" />
					</div>
					<div class="col-xs-6" >
						<h6><?php echo $contacts['company_name'];?></h6>
						<h6> <?php echo $contacts['location'];?>,</h6>
						<h6>	Address : P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></h6> 
						<h6>Phone:  <?php echo $contacts['phone'];?></h6>
						<h6>Email:	 <?php echo $contacts['email'];?>.</h6>
					</div>
			</div>
			<div class="row">
            	<div class="col-md-12">
            	<tr>
                    <th class="align-center"><h4><strong>LANDLORD STATEMENT FOR THE MONTH OF <?php echo strtoupper($month_year)?> </strong> </h4></th>
                </tr>
               </div>
            </div>
        <div class="row">
            <div class="col-md-12">
            	<tr>
                    <th class="align-center"><h4><strong><?php echo $title?> </strong> </h4></th>
                </tr>
                <?php echo $result;?>
            </div>
        </div>
        <div class="row" >
       		<div class="col-md-12" >
       			<h4>Key Areas to Note:</h4>
       			<div class="row">
       				<div class="col-md-12">
	       				<h5><strong>Tenant Deposits</strong></h5>
	       				<table class="table table-condensed table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Rental Unit</th>
										  <th style="width:25%">Tenant</th>
											<th style="width:25%">Deposit Amount</th>

										</tr>
									</thead>
									<tbody>
			       						<?php echo $closed_new_leases;?>
									</tbody>
								</table>

	       			</div>


       			</div>
       			<div class="row">
	       			<div class="col-md-12">
	       				<h5>Expenses</h5>
	       				<table class="table table-condensed table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Description</th>
										  <th style="width:25%">Document No</th>
										  <th style="width:25%">Amount</th>
										</tr>
									</thead>
									<tbody>
			       						<?php echo $outflow;?>
									</tbody>
								</table>



								<h5>Incomes</h5>
	       				<table class="table table-condensed table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Account</th>
										  <th style="width:25%">Description</th>
										  <th style="width:25%">Amount</th>
										</tr>
									</thead>
									<tbody>
			       						<?php echo $incomes;?>
									</tbody>
								</table>


								<h5>Landloard Receipts</h5>
	       				<table class="table table-condensed table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Account</th>
										  <th style="width:25%">Description</th>
										  <th style="width:25%">Amount</th>
										</tr>
									</thead>
									<tbody>
			       						<?php echo $landlord_receipts;?>
									</tbody>
								</table>


								<h5>Landlord payments</h5>
	       				<table class="table table-condensed table-bordered ">
								 	<thead>
										<tr>
										  <th style="width:25%">#</th>
										  <th style="width:25%">Account</th>
										  <th style="width:25%">Description</th>
										  <th style="width:25%">Amount</th>
										</tr>
									</thead>
									<tbody>
			       						<?php echo $landlord_outflow;?>
									</tbody>
								</table>
	       			</div>

	       		</div>

				<div class="row">
					<div class="col-md-6">
					</div>
   					<div class="col-md-6">
						<h4>Total Rent Received</h4>
						<table class="table table-condensed table-bordered ">
							<thead>

							<tbody>
										<tr>
												<td>Total Gross Payable</td>
												<td><?php echo number_format($total_payable_end,2);?></td>
										</tr>
										<tr>
												<td>Add Other Deposits</td>
												<td><?php echo number_format($total_new_lease,2);?></td>
										</tr>
										<tr>
												<td>Add Refunds</td>
												<td><?php echo number_format($landlord_receipt_amount+$income_amount,2);?></td>
										</tr>
										<tr>
												<td>Less Landlord Advance</td>
												<td><?php echo number_format($total_landlord_outflows,2);?></td>
										</tr>
										<tr>
												<td>Less Other Payments</td>
												<td><?php echo number_format($total_outflows,2);?></td>
										</tr>
										<tr>
												<td>OUTSTANDING PAYABLE</td>
												<td><?php echo number_format(0,2);?></td>
										</tr>
										<tr>
												<td><b>NET AMOUNT PAYABLE</b></td>
												<td><?php echo number_format(($total_payable_end + $total_new_lease  + $landlord_receipt_amount + $income_amount) - ($total_landlord_outflows + $total_outflows),2);?></td>
										</tr>
							</tbody>
						</table>
					</div>
				</div>
       		</div>
       	</div>
        <div class="row" >
       		<div class="col-xs-12 align-center" style="border-top:1px #000 solid;">
       			<span style="font-size:10px; " > <?php echo $contacts['company_name'];?> | <?php echo $contacts['location'];?>, <?php echo $contacts['building'];?>, <?php echo $contacts['floor'];?>
                     E-mail: <?php echo $contacts['email'];?>.<br/> Tel : <?php echo $contacts['phone'];?>
                     P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?></span>
            </div>
       	</div>
     </body>
    </html>
