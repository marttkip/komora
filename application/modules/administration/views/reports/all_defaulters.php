<!-- search -->
<?php echo $this->load->view('search/defaulters', '', TRUE);?>
<!-- end search -->
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?> <a href="<?php echo site_url();?>export-defaulters" class="btn btn-sm btn-success pull-right" style='margin-top:-5px;'> Export Defaulters List</a>

            	 <?php
            	 	$search =  $this->session->userdata('all_defaulters_search');
					if(!empty($search))
					{
						echo '<a href="'.site_url().'print-tenant-report" style="margin-top:-5px; margin-right:4px;" target="_blank" class="btn btn-sm btn-primary pull-right">Print Tenant Report</a>';
					}

            	 ?>

            	 </h2>
            </header>             

          <!-- Widget content -->
           <div class="panel-body">
	          <h5 class="center-align"><?php echo $this->session->userdata('search_title');?></h5>
	         <?php

				$result = '';
				$search =  $this->session->userdata('all_defaulters_search');
				if(!empty($search))
				{
					echo '<a href="'.site_url().'administration/reports/close_defaulters_search/'.$module.'" class="btn btn-sm btn-warning">Close Search</a>';
				}
				
				//if users exist display them
				if ($query->num_rows() > 0)
				{
					$count = $page;
					
					$result .= 
					'
					<table class="table table-bordered table-striped table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>Property</th>
								<th>Flat No.</th>
								<th>Tenant Name</th>
								<th>Arreas B/F</th>
								<th>Receipt/Date</th>
								<th>Amount paid</th>
								<th>Arreas C/F</th>
								<th>Phone Number</th>
								<th colspan="7">Actions</th>

							</tr>
						</thead>
						  <tbody>
						  
					';
					
					
					foreach ($query->result() as $leases_row)
					{
						$lease_id = $leases_row->lease_id;
						$tenant_unit_id = $leases_row->tenant_unit_id;
						$property_name = $leases_row->property_name;
						$rental_unit_name = $leases_row->rental_unit_name;
						$tenant_name = $leases_row->tenant_name;
						$lease_start_date = $leases_row->lease_start_date;
						$lease_duration = $leases_row->lease_duration;
						$rent_amount = $leases_row->rent_amount;
						$lease_number = $leases_row->lease_number;
						// $arreas_bf = $leases_row->arreas_bf;
						$rent_calculation = $leases_row->rent_calculation;
						$deposit = $leases_row->deposit;
						$deposit_ext = $leases_row->deposit_ext;
						$lease_status = $leases_row->lease_status;
						$tenant_phone_number = $leases_row->tenant_phone_number;
						// $arrears_bf = $leases_row->arrears_bf;
						$created = $leases_row->created;

						// $lease_start_date = date('jS M Y',strtotime($lease_start_date));
						
						// $expiry_date  = date('jS M Y',strtotime($lease_start_date, mktime()) . " + 365 day");
						// $expiry_date  = date('jS M Y', strtotime(''.$lease_start_date.'+1 years'));
						
					
							
						$lease_start_date = date('jS M Y',strtotime($lease_start_date));
	     	
				     	$this_month = date('m');
				     	$amount_paid = 0;
					     	$payments = $this->accounts_model->get_this_months_payment($lease_id,$this_month);
					     	$current_items = '<td> -</td>
					     			  <td>-</td>'; 
					     	$total_paid_amount = 0;
					     	if($payments->num_rows() > 0)
					     	{
					     		$counter = 0;
					     		
					     		$receipt_counter = '';
					     		foreach ($payments->result() as $value) {
					     			# code...
					     			$receipt_number = $value->receipt_number;
					     			$amount_paid = $value->amount_paid;

					     			if($counter > 0)
					     			{
					     				$addition = '#';
					     				// $receipt_counter .= $receipt_number.$addition;
					     			}
					     			else
					     			{
					     				$addition = ' ';
					     				
					     			}
					     			$receipt_counter .= $receipt_number.$addition;

					     			$total_paid_amount = $total_paid_amount + $amount_paid;
					     			$counter++;
					     		}
					     		$current_items = '<td>'.$receipt_number.'</td>
					     					<td>'.number_format($amount_paid,0).'</td>';
					     	}
					     	else
					     	{
					     		$current_items = '<td> -</td>
					     					<td>-</td>';
					     	}
							
							$todays_date = date('Y-m-d');
							$todays_month = date('m');
							$todays_year = date('Y');
							$total_payments = $this->accounts_model->get_total_payments_before($lease_id,$todays_year,$todays_month);
							
							$total_invoices = $this->accounts_model->get_total_invoices_before($lease_id,$todays_year,$todays_month);
							// var_dump($total_invoices); die();
							$current_balance = $total_invoices - $total_payments;

							$current_invoice_amount = $this->accounts_model->get_latest_invoice_amount($lease_id,date('Y'),date('m'));
							$total_current_invoice = $current_invoice_amount + $current_balance - $total_paid_amount;


							
								$count++;
								$result .= 
								'
									<tr>
										<td>'.$count.'</td>
										<td>'.$property_name.'</td>
										<td>'.$rental_unit_name.'</td>
										<td>'.$tenant_name.'</td>
										<td>'.number_format($current_balance,2).'</td>
											'.$current_items.'
										<td>'.number_format($total_current_invoice,2).'</td>
										<td>'.$tenant_phone_number.'</td>
										<td><a href="'.site_url().'reports/tenant-statement/'.$lease_id.'" target="_blank" class="btn btn-sm btn-default" ><i class="fa fa-upload"></i> View Statement</a></td>
					                    <td><a href="'.site_url().'reports/send-statement/'.$lease_id.'"  class="btn btn-sm btn-warning" ><i class="fa fa-upload"></i> Send Statement</a></td>
					
										
									</tr> 
								';

							
							
				}
				$result .= 
				'
							  </tbody>
							</table>
				';
			}

			else
			{
				$result .= "There are no defaulters";
			}

			echo $result;
			?>  
			
	        </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>
 <script type="text/javascript">
	$(function() {
	    $("#property_id").customselect();
	    $("#branch_code").customselect();
	});
	$(document).ready(function(){
		$(function() {
			$("#property_id").customselect();
			$("#branch_code").customselect();
		});
	});
</script>