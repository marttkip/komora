<?php

class Water_management_model extends CI_Model 
{
	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}

	/*
	*	Retrieve all personnel
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_water_records($table, $where, $per_page, $page, $order = 'water_management.document_number', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('water_management.document_number');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Import Template
	*
	*/
	function import_template()
	{
		$this->load->library('Excel');
		
		$title = 'Water Management Template';
		$count=1;
		$row_count=0;
		
		$report[$row_count][0] = 'HSE Number';
		$report[$row_count][1] = 'Meter Number';
		$report[$row_count][2] = 'Prev Reading';
		$report[$row_count][3] = 'Curr Reading';
		$report[$row_count][4] = 'Units Consumed';
		$report[$row_count][5] = 'Prev B/f';
		$report[$row_count][6] = 'Invoice Date';
		
		$row_count++;
		
		//create the excel document
		$this->excel->addArray ( $report );
		$this->excel->generateXML ($title);
	}

	public function import_csv_charges($upload_path)
	{
		//load the file model
		$this->load->model('admin/file_model');
		/*
			-----------------------------------------------------------------------------------------
			Upload csv
			-----------------------------------------------------------------------------------------
		*/
		$response = $this->file_model->upload_csv($upload_path, 'import_csv');
		
		if($response['check'])
		{
			$file_name = $response['file_name'];
			
			$array = $this->file_model->get_array_from_csv($upload_path.'/'.$file_name);
			//var_dump($array); die();
			$response2 = $this->sort_csv_charges_data($array);
		
			if($this->file_model->delete_file($upload_path."\\".$file_name, $upload_path))
			{
			}
			
			return $response2;
		}
		
		else
		{
			$this->session->set_userdata('error_message', $response['error']);
			return FALSE;
		}
	}
	public function sort_csv_charges_data($array)
	{
		//count total rows
		$total_rows = count($array);
		$total_columns = count($array[0]);
		$property_invoice_id = $this->input->post('property_invoice_id');
		// var_dump($total_columns); die();
		//get invoice
		//$this->db->where(array('property_invoice_id' => $property_invoice_id, 'invoice_type_id' => $invoice_type_id));
		$this->db->where(array('property_invoice_id' => $property_invoice_id));
		$invoices = $this->db->get('property_invoice');
		
		if($invoices->num_rows() > 0)
		{
			$row = $invoices->row();
			$property_invoice_id = $row->property_invoice_id;
			$property_invoice_amount = $row->property_invoice_amount;
			$property_invoice_units = $row->property_invoice_units;
			$property_invoice_number = $row->property_invoice_number;
			$property_id = $row->property_id;
			$invoice_type_id = $row->invoice_type_id;
			$property_id = $row->property_id;
			
			//if products exist in array
			if(($total_rows > 0) && ($total_columns == 7))
			{
				$count = 0;
				$comment = '';
				$items['modified_by'] = $this->session->userdata('personnel_id');
				$document_number = $this->create_document_number();

				$this->db->where('property_id = '.$property_id.' AND invoice_type_id = '.$invoice_type_id.' AND billing_schedule_id = 4');
				$billing_query = $this->db->get('property_billing');
				$rate_per_use = 0;
				if($billing_query->num_rows() > 0)
				{
					foreach ($billing_query->result() as $key_billing) {
						# code...
						$rate_per_use = $key_billing->billing_amount;
						$charge_to = $key_billing->charge_to;
					}
				}
				// $rate_per_use = 1;


				//retrieve the data from array
				for($r = 1; $r < $total_rows; $r++)
				{
					$hse_name = $array[$r][0];
					$meter_number = $array[$r][1];
					$prev_reading = $array[$r][2];
					$curr_reading = $array[$r][3];
					$units_consumed = $array[$r][4];
					$prev_bal = $array[$r][5];
					$invoice_date = $array[$r][6];

					if($invoice_type_id == 2)
					{
						if(empty($curr_reading) || empty($prev_reading) )
						{
                                $total_due = 0;
						}
						else
						{
							if(!empty($prev_bal))
							{

								$total_due = ($curr_reading - $prev_reading) * $rate_per_use +$prev_bal;
							}
							else
							{

								$total_due = ($curr_reading - $prev_reading) * $rate_per_use;
							}


						}
						
					}
					else if($invoice_type_id == 10)
					{
						if(empty($units_consumed))
						{
							$total_due = $prev_bal;
						}
						else
						{
							// $rate_per_use = 0.5;
							$total_due = $units_consumed * $rate_per_use;
						}
						
					}
					else if($invoice_type_id == 3)
					{
						$rate_per_use = 40;
						$total_due = $units_consumed;
					}
					else if($invoice_type_id == 4)
					{
						$total_due = $prev_bal;
					}
					else if($invoice_type_id == 7)
					{
						$total_due = $prev_bal;
					}
					else if($invoice_type_id == 8)
					{
						$total_due = $prev_bal;
					}
					else if($invoice_type_id == 9)
					{
						$total_due = $units_consumed;
					}
					else if($invoice_type_id == 5)
					{
						$total_due = $units_consumed;
					}
					else if($invoice_type_id == 14)
					{
						$total_due = ($units_consumed * $rate_per_use) + 450 * $prev_bal  ;
					}
					else if($invoice_type_id == 15)
					{
						$total_due = $units_consumed;
					}
					$current_date  =  date('Y-m-d');
					$datestring=''.$current_date.' first day of last month';
					$dt=date_create($datestring);
					$previous = $dt->format('Y-m');
					$previous_date = explode('-', $previous);
					$previous_year = $previous_date[0];
					$previous_month = $previous_date[1];
					

					// $total_due_borehole = ($curr_reading - $prev_reading) * 26;
					// $total_due = $total_due + $prev_bal;
					
					// var_dump($total_due); die();
					
					$count++;
					$check_date = explode('-', $invoice_date);
					
					if(count($check_date) > 1)
					{

						$newDate = date("Y-m-d", strtotime($invoice_date));
						//echo $newDate; die();
						$invoice_explode = explode('-', $newDate);
						
						$todaym2 = $invoice_explode[1];
						$year = $invoice_explode[0];

						if($charge_to == 1)
						{
							//check if there are records for the particular month/year
							$this->db->where(array('invoice_month' => $todaym2, 'invoice_year' => $year));
							$query_invoice = $this->db->get('invoice');
							
							if($query_invoice->num_rows() > 0)
							{
								$invoice_row = $query_invoice->row();
								$invoice_id = $invoice_row->invoice_id;
								$this->db->where('invoice_id', $invoice_id);
								$mtr_query = $this->db->get('water_management');
								$row2 = $mtr_query->row();
								$document_number = 'MRT001';
							}
							$document_number = $property_invoice_number;
							// get lease id using the unit name 
							if(!empty($hse_name))
							{
								$lease_id = $this->get_lease_id($hse_name);
								// var_dump($total_due); die();
								if($lease_id > 0)
								{
									
									$datestring=''.$invoice_date.' first day of next month';
									$dt=date_create($datestring);
									$next = $dt->format('Y-m-d');
									$next_date = explode('-', $next);
									$next_year = $next_date[0];
									$next_month = $next_date[1];
									$next_date = $next_year.'-'.$next_month.'-'.'01';
									$next_date = strtotime($next_date);
									$next_quarter = ceil(date('m', $next_date) / 3);
									$next_month = ($next_quarter * 3) - 2;
									$next_year = date('Y', $next_date);

									$next_quarter = 'AC'.$next_quarter.'-'.$next_year;



									$invoice_number = $this->get_invoice_number();
									$insert_array = array(
													'lease_id' => $lease_id,
													'invoice_date' => $invoice_date,
													'invoice_month' => $todaym2,
													'invoice_year' => $year,
													'invoice_amount' => $total_due,
													'arrears_bf' => $total_due,
													'invoice_number' => $invoice_number,
													'invoice_type' => $invoice_type_id,
													'property_invoice_id' => $property_invoice_id,
													'billing_schedule_quarter'=> $next_quarter
												 );
									
									if($this->db->insert('invoice',$insert_array))
									{
										$comment .= '<br/>Details successfully added to the database';
										$class = 'success';

										$invoice_id = $this->db->insert_id();
										// service charge entry

										if($invoice_type_id == 2 OR $invoice_type_id == 3)
										{
											$service_charge_insert = array(
																	"house_number" => $hse_name,
																	"prev_reading" => $prev_reading,
																	"current_reading" => $curr_reading,
																	"units_consumed" => $units_consumed,
																	"total_due" => $total_due,
																	"prev_bill" => $prev_bal,
																	"created" => date("Y-m-d"),
																	"created_by" => $this->session->userdata('personnel_id'),
																	"branch_code" => $this->session->userdata('branch_code'),
																	'property_id' => $property_id,
																	'property_invoice_id' => $property_invoice_id,
																	'document_number' => $document_number,
																	'invoice_id' => $invoice_id
																);
											$this->db->insert('water_management', $service_charge_insert);
										}
										
									}
									
									else
									{
										$comment .= '<br/>Not saved internal error';
										$class = 'danger';
									}
									
			
									$return['response'] = TRUE;
									$return['check'] = TRUE;
			
								}
			
								
							}
							else
							{
			
								$return['response'] = FALSE;
								$return['check'] = FALSE;
							}
						}
						else if($charge_to == 0)
						{

							// var_dump($todaym2); die();
							$this->db->where(array('invoice_month' => $todaym2, 'invoice_year' => $year));
							$query_invoice = $this->db->get('home_owners_invoice');
							
							if($query_invoice->num_rows() > 0)
							{
								$invoice_row = $query_invoice->row();
								$invoice_id = $invoice_row->invoice_id;
								$this->db->where('invoice_id', $invoice_id);
								$mtr_query = $this->db->get('water_management');
								$row2 = $mtr_query->row();
								$document_number = 'MRT001';
							}
							
							// get lease id using the unit name 
							if(!empty($hse_name))
							{
								// $lease_id = $this->get_lease_id($hse_name);
								$rental_unit_id = $this->accounts_model->get_rental_unit_id($hse_name);
								// var_dump($total_due); die();
								// var_dump($total_due);die(); 
								if($rental_unit_id > 0)
								{
									
									$datestring=''.$invoice_date.' first day of next month';
									$dt=date_create($datestring);
									$next = $dt->format('Y-m-d');
									$next_date = explode('-', $next);
									$next_year = $next_date[0];
									$next_month = $next_date[1];
									$next_date = $next_year.'-'.$next_month.'-'.'01';
									$next_date = strtotime($next_date);
									$next_quarter = ceil(date('m', $next_date) / 3);
									$next_month = ($next_quarter * 3) - 2;
									$next_year = date('Y', $next_date);

									$next_quarter = 'AC'.$next_quarter.'-'.$next_year;



									$invoice_number = $this->get_invoice_number();
									$insert_array = array(
													'rental_unit_id' => $rental_unit_id,
													'invoice_date' => $invoice_date,
													'invoice_month' => $todaym2,
													'invoice_year' => $year,
													'invoice_amount' => $total_due,
													'arrears_bf' => $total_due,
													'invoice_number' => $invoice_number,
													'invoice_type' => $invoice_type_id,
													'property_invoice_id' => $property_invoice_id,
													'billing_schedule_quarter'=> $next_quarter
												 );
									
									if($this->db->insert('home_owners_invoice',$insert_array))
									{
										$comment .= '<br/>Details successfully added to the database';
										$class = 'success';

										$invoice_id = $this->db->insert_id();
										// service charge entry

										if($invoice_type_id == 2 OR $invoice_type_id == 3 OR $invoice_type_id == 14)
										{
											$service_charge_insert = array(
																	"house_number" => $hse_name,
																	"prev_reading" => $prev_reading,
																	"current_reading" => $curr_reading,
																	"units_consumed" => $units_consumed,
																	"total_due" => $total_due,
																	"prev_bill" => $prev_bal,
																	"created" => date("Y-m-d"),
																	"created_by" => $this->session->userdata('personnel_id'),
																	"branch_code" => $this->session->userdata('branch_code'),
																	'property_id' => $property_id,
																	'property_invoice_id' => $property_invoice_id,
																	'invoice_id' => $invoice_id
																);
											$this->db->insert('water_management', $service_charge_insert);
										}
										
									}
									
									else
									{
										$comment .= '<br/>Not saved internal error';
										$class = 'danger';
									}
									
			
									$return['response'] = TRUE;
									$return['check'] = TRUE;
			
								}
			
								
							}
							else
							{
			
								$return['response'] = FALSE;
								$return['check'] = FALSE;
							}
						}

					}
					else
					{
	
						$return['response'] = FALSE;
						$return['check'] = FALSE;
					}
				}	
					
			}
			else
			{
				$return['response'] = FALSE;
				$return['check'] = FALSE;
			}
		}
		
		else
		{
			$return['response'] = FALSE;
			$return['check'] = FALSE;
		}
		
		return $return;
	}
	public function get_invoice_number()
	{
		//select product code
		$preffix = $this->session->userdata('branch_code');
		$this->db->from('invoice');
		$this->db->where("invoice_number LIKE '".$preffix."%'");
		$this->db->select('MAX(invoice_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	public function get_lease_id($hse_name)
	{
		$this->db->where('rental_unit.rental_unit_id = tenant_unit.rental_unit_id AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status = 1 AND rental_unit.rental_unit_name = "'.$hse_name.'"');
		$this->db->select('lease_id');
		$query = $this->db->get('leases,rental_unit,tenant_unit');
		if($query->num_rows() > 0)
		{
			$item = $query->row();
			$lease_id = $item->lease_id;
			return $lease_id;
		}
		else
		{
			return FALSE;
		}
	}

	
	public function create_document_number()
	{
		$preffix = $this->session->userdata('branch_code');
		$this->db->from('water_management');
		$this->db->where("document_number LIKE '".$preffix."%'");
		$this->db->select('MAX(document_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	public function get_document_details($document_number)
	{
		$this->db->where('property_invoice.property_invoice_id = water_management.property_invoice_id AND document_number = "'.$document_number.'"');
		$this->db->from('water_management, property_invoice');
		$this->db->select('*');
		$query = $this->db->get();

		return $query;
	}
	public function get_rental_units()
	{
		$this->db->order_by('rental_unit_name');
		$this->db->from('rental_unit');
		$this->db->select('*');
		$query = $this->db->get();

		return $query;
	}

	public function sms($phone,$message)
	{
        // This will override any configuration parameters set on the config file
		// max of 160 characters
		// to get a unique name make payment of 8700 to Africastalking/SMSLeopard
		// unique name should have a maximum of 11 characters
		$phone_number = '+254'.$phone;
		// get items 

		$configuration = $this->admin_model->get_configuration();

		$mandrill = '';
		$configuration_id = 0;
		
		if($configuration->num_rows() > 0)
		{
			$res = $configuration->row();
			$configuration_id = $res->configuration_id;
			$mandrill = $res->mandrill;
			$sms_key = $res->sms_key;
			$sms_user = $res->sms_user;
	        $sms_suffix = $res->sms_suffix;
	        $sms_from = $res->sms_from;
		}
	    else
	    {
	        $configuration_id = '';
	        $mandrill = '';
	        $sms_key = '';
	        $sms_user = '';
	        $sms_suffix = '';

	    }

	    $actual_message = $message.' '.$sms_suffix;
	    // var_dump($actual_message); die();
		// get the current branch code
        $params = array('username' => $sms_user, 'apiKey' => $sms_key);  

        $this->load->library('AfricasTalkingGateway', $params);
		// var_dump($params)or die();
        // Send the message
		try 
		{
        	$results = $this->africastalkinggateway->sendMessage($phone_number, $actual_message, $sms_from);
			
			//var_dump($results);die();
			foreach($results as $result) {
				// status is either "Success" or "error message"
				// echo " Number: " .$result->number;
				// echo " Status: " .$result->status;
				// echo " MessageId: " .$result->messageId;
				// echo " Cost: "   .$result->cost."\n";
			}
			return $result->status;

		}
		
		catch(AfricasTalkingGatewayException $e)
		{
			// echo "Encountered an error while sending: ".$e->getMessage();
			return FALSE;
		}
    }
	
	public function get_invoice_types()
	{
		$this->db->order_by('invoice_type_name');
		return $this->db->get('invoice_type');
	}
	
	public function add_property_invoice()
	{
		$data = array(
			'property_id'=>$this->input->post('property_id'),
			'property_invoice_number'=>$this->input->post('property_invoice_number'),
			'invoice_type_id'=>$this->input->post('invoice_type_id'),
			'property_invoice_amount'=>$this->input->post('property_invoice_amount'),
			'property_invoice_units'=>$this->input->post('property_invoice_units'),
			'property_invoice_date'=>$this->input->post('property_invoice_date'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id'),
			'created'=>date('Y-m-d H:i:s')
		);
		
		if($this->db->insert('property_invoice', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	public function edit_property_invoice($property_invoice_id)
	{
		$data = array(
			'property_id'=>$this->input->post('property_id'),
			'invoice_type_id'=>$this->input->post('invoice_type_id'),
			'invoice_amount'=>$this->input->post('invoice_amount'),
			'invoice_units'=>$this->input->post('invoice_units'),
			'invoice_date'=>$this->input->post('invoice_date'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id'),
			'created'=>date('Y-m-d H:i:s')
		);
		
		$this->db->where('property_invoice_id', $property_invoice_id);
		if($this->db->update('property_invoice', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_property_invoices()
	{
		$this->db->select('property.property_name, invoice_type.invoice_type_name, property_invoice.*');
		$this->db->where('property_invoice.property_id = property.property_id AND property_invoice.invoice_type_id = invoice_type.invoice_type_id');
		$this->db->order_by('property_invoice.property_invoice_date', 'DESC');
		$this->db->order_by('property.property_name', 'ASC');
		$this->db->order_by('invoice_type.invoice_type_name', 'ASC');
		return $this->db->get('property_invoice, property, invoice_type');
	}
	public function delete_roperty_invoice($property_invoice_id)
	{
		$this->db->where('property_invoice_id', $property_invoice_id);
		if($this->db->delete('property_invoice'))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_service_charge_debt($lease_id,$month,$year,$charge_type)
	{
		$where = 'lease_id = '.$lease_id.' AND invoice_type_id = '.$charge_type.'  AND payment_item_status = 1 AND payment_month = "'.$month.'" AND payment_year = "'.$year.'"';
		// var_dump($where);die();
		$this->db->from('payment_item');
		// $this->db->select('balance_cf AS balance');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$this->db->order_by('lease_id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$total_paid = 0;

		foreach ($query->result() as $key) {
			# code...
			$total_paid = $key->total_paid;
		}
		if($total_paid == NULL)
		{
			$total_paid = 0;
		}

		return $total_paid;
	}
}
