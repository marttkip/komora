<!-- search -->
<?php echo $this->load->view('search_creditor_account', '', TRUE);?>
<!-- end search -->

<div class="row">
  <div class="col-md-12">
    <div class="box">
       <div class="box-header with-border">
         <h3 class="box-title">Make a Transfer</h3>

         <div class="box-tools pull-right">
           <a href="<?php echo base_url().'accounting/creditors/print_creditor_account/'.$creditor_id?>" class="btn btn-sm btn-success"   target="_blank"><i class="fa fa-print"></i> Print</a>
           <button type="button" class="btn btn-sm btn-primary"  data-toggle="modal" data-target=".bd-example-modal-lg" ><i class="fa fa-plus"></i> Add Invoice</button>
           <button type="button" class="btn btn-sm btn-info"  data-toggle="modal" data-target=".bd-example-modal-lg" ><i class="fa fa-plus"></i> Add Credit Note</button>
             <a href="<?php echo site_url();?>accounting/creditors" class="btn btn-sm btn-warning" ><i class="fa fa-arrow-left"></i> Back to creditors</a>

         </div>
       </div>
       <div class="box-body">
          <div class="modal fade bd-example-modal-lg" id="record_creditor_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg modal-primary" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">Record Invoice</h4>
                      </div>
                      <div class="modal-body">
                          <?php echo form_open("accounting/creditors/record_creditor_account/".$creditor_id, array("class" => "form-horizontal"));?>
                          <input type="hidden" name="account_from_id" value="<?php echo $creditor_id;?>">
                          <div class="form-group">
                              <label class="col-md-4 control-label">Transaction date: </label>

                              <div class="col-md-8">
                                  <div class="input-group">
                                      <span class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                      </span>
                                      <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="creditor_account_date" placeholder="Transaction date" id='datepicker'>
                                  </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-4 control-label">Transaction Code *</label>

                              <div class="col-md-8">
                                  <input type="text" class="form-control" name="transaction_code" placeholder="Code e.g cheque number/MPESA code"/>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-md-4 control-label">Description *</label>

                              <div class="col-md-8">
                                  <textarea class="form-control" name="creditor_account_description"></textarea>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-md-4 control-label">Amount *</label>

                              <div class="col-md-8">
                                  <input type="text" class="form-control" name="creditor_account_amount" placeholder="Amount"/>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-md-8 col-md-offset-4">
                                  <div class="center-align">
                                      <button type="submit" class="btn btn-primary">Save record</button>
                                  </div>
                              </div>
                          </div>
                          <?php echo form_close();?>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                  </div>
              </div>
          </div>


      			<?php
      			$error = $this->session->userdata('error_message');
      			$success = $this->session->userdata('success_message');

      			if(!empty($error))
      			{
      				echo '<div class="alert alert-danger">'.$error.'</div>';
      				$this->session->unset_userdata('error_message');
      			}

      			if(!empty($success))
      			{
      				echo '<div class="alert alert-success">'.$success.'</div>';
      				$this->session->unset_userdata('success_message');
      			}

      			$search = $this->session->userdata('creditor_payment_search');
      			$search_title = $this->session->userdata('creditor_search_title');

      			if(!empty($search))
      			{
      				echo '
      				<a href="'.site_url().'accounting/creditors/close_creditor_search/'.$creditor_id.'" class="btn btn-warning btn-sm ">Close Search</a>
      				';
      				echo $search_title;
      			}

      				$creditor_result = $this->creditors_model->get_creditor_statement($creditor_id);
      			?>

				        <table class="table table-hover table-bordered ">
				 	<thead>
						<tr>
						  <th>Transaction Date</th>
						  <th>Invoice Number</th>
						  <th>Description</th>
						  <th>Debit</th>
						  <th>Credit</th>
						</tr>
					 </thead>
				  	<tbody>
				  		<?php  echo $creditor_result['result'];?>
					</tbody>
				</table>
          	</div>
		</div>
    </div>
</div>
