<div class="row statistics">
    
    <div class="col-md-12 col-sm-12">
    	 <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	<h2 class="panel-title">Transaction breakdown for <?php echo $total_visits;?> patients</h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                <div class="row">
                   
                    <div class="col-md-3">
						<?php
                        	$total_invoices_revenue = $this->accounting_model->get_visit_invoice_totals();
                            $total_payments_revenue = $this->accounting_model->get_visit_payment_totals();
                            $total_waiver_revenue = $this->accounting_model->get_visit_waiver_totals();

                        ?>
                        <h5>VISIT TYPE BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <tr>
                                    <th>TOTAL COLLECTION</th>
                                    <td><?php echo number_format($total_payments_revenue, 2);?></td>
                                </tr>
                                <tr>
                                    <th>TOTAL INVOICES</th>
                                    <td><?php echo number_format($total_invoices_revenue, 2);?></td>
                                </tr>
                                 <tr>
                                    <th>TOTAL WAIVERS</th>
                                    <td><?php echo number_format($total_waiver_revenue, 2);?></td>
                                </tr>

                                <tr>
                                    <th>DEBT BALANCE</th>
                                    <td><?php echo number_format(($total_invoices_revenue - $total_payments_revenue - $total_waiver_revenue), 2);?></td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <div class="clearfix"></div>
            		</div>
                    <!-- End Transaction Breakdown -->
                   
                    <div class="col-md-3">
                        <h5>PAYMENT METHODS BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
								<?php
								$total_cash_breakdown = 0;
								$payment_methods = $this->accounting_model->get_payment_methods();
                                if($payment_methods->num_rows() > 0)
                                {
                                    foreach($payment_methods->result() as $res)
                                    {
                                        $method_name = $res->payment_method;
                                        $payment_method_id = $res->payment_method_id;
                                        $total = $this->accounting_model->get_amount_collected($payment_method_id);
                                 
                                        
                                        echo 
										'
										<tr>
											<th>'.$method_name.'</th>
											<td>'.number_format($total, 2).'</td>
										</tr>
										';
										$total_cash_breakdown += $total;
                                    }
                                    
									echo 
									'
									<tr>
										<th>Total</th>
										<td>'.number_format($total_cash_breakdown, 2).'</td>
									</tr>
									';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                   
                   
                    <div class="col-md-2 center-align">
                        <h4>INVOICED</h4>
                        <h3>Ksh <?php echo number_format($total_invoices_revenue-$total_waiver_revenue, 2);?></h3>
                        <!-- <p><?php echo $title;?></p> -->
                    </div>
                    <div class="col-md-2 center-align">
                         <h4>PAYMENTS</h4>
                        <h3>Ksh <?php echo number_format($total_payments_revenue, 2);?></h3>
                        <!-- <p><?php echo $title;?></p> -->
                    </div>
                    <div class="col-md-2 center-align">
                         <h4>BALANCE</h4>
                        <h3>Ksh <?php echo number_format($total_invoices_revenue - $total_payments_revenue - $total_waiver_revenue, 2);?></h3>
                        <!-- <p><?php echo $title;?></p> -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        
                    </div>
                     
                </div>
          	</div>
		</section>
    </div>
</div>