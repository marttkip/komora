<?php
  $leases_row = $query->row();


  $lease_id = $leases_row->lease_id;
  $tenant_unit_id = $leases_row->tenant_unit_id;
  $rental_unit_id = $leases_row->unit_id;
  $rental_unit_name = $leases_row->rental_unit_name;
  $tenant_name = $leases_row->tenant_name;
  $tenant_email = $leases_row->tenant_email;
  $tenant_phone_number = $leases_row->tenant_phone_number;
  $lease_start_date = $leases_row->lease_start_date;
  $lease_duration = $leases_row->lease_duration;
  $rent_amount = $leases_row->rent_amount;
  $lease_number = $leases_row->lease_number;
  $arreas_bf = $leases_row->arrears_bf;
  $rent_calculation = $leases_row->rent_calculation;
  $property_name = $leases_row->property_name;
  $deposit = $leases_row->deposit;
  $deposit_ext = $leases_row->deposit_ext;
  $lease_status = $leases_row->lease_status;
  $points = 0;//$leases_row->points;
  $ticket_created = $leases_row->ticket_created;
  $ticket_description = $leases_row->ticket_description;
  $ticket_status = $leases_row->ticket_status;
  $ticket_number = $leases_row->ticket_number;
  $account_id = $leases_row->account_id;
  $ticket_id = $leases_row->ticket_id;
  $ticket_status = $leases_row->ticket_status;
  $date_closed = $leases_row->date_closed;
  $general_observations = $leases_row->general_observations;

  //create deactivated status display
  if($ticket_status == 0)
  {
    $status = '<span class="label label-default"> Deactivated</span>';

    $button = '';
    $delete_button = '';
  }
  //create activated status display
  else if($ticket_status == 1)
  {
    $status = '<span class="label label-success">Active</span>';
    $button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
    $delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

  }

  $ticket_created = date('jS M Y',strtotime($ticket_created));





  // var_dump($leases_row);die();

  $chat_query = $this->tickets_model->get_tenants_ticket_chat($ticket_id);
  $result = '';
  if($chat_query->num_rows() > 0)
  {
    foreach ($chat_query->result() as $key => $value) {
      // code...
      $chat_type = $value->chat_type;
      $chat_message = $value->chat_message;
      $created = $value->created;
      $user_id = $value->user_id;
      $personnel_fname = $value->personnel_fname;
      $personnel_onames = $value->personnel_onames;

      if($chat_type == 1)
      {
        $username = '';
        $result .= '<div class="direct-chat-msg">
                        <div class="direct-chat-info clearfix">
                          <span class="direct-chat-name pull-left">'.$tenant_name.'</span>
                          <span class="direct-chat-timestamp pull-right">'.$created.'</span>
                        </div>
                        <div class="direct-chat-text">
                          '.$chat_message.'
                        </div>
                      </div>';
      }
      else
      {
        $username = $value->personnel_fname;

        $result .= '<div class="direct-chat-msg right">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">'.$personnel_fname.' '.$personnel_onames.'</span>
                        <span class="direct-chat-timestamp pull-left">'.$created.'</span>
                      </div>
                      <div class="direct-chat-text">
                        '.$chat_message.'
                      </div>
                    </div>';
      }



    }
  }

?>

<section class="content">
    <div class="row">
      <div class="col-md-12">
    		<a href="<?php echo site_url();?>ticket-management/resolved-tickets" class="btn btn-sm btn-warning pull-right"  ><i class="fa fa-arrow-left"></i> Back to unresolved tickets</a>
    	</div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
          <div class="box-body box-profile">
            <!-- <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture"> -->

            <h3 class="profile-username text-center"><?php echo strtoupper($tenant_name);?></h3>

            <p class="text-muted text-center"><?php echo strtoupper($property_name);?></p>
            <p class="text-muted text-center"><?php echo strtoupper($rental_unit_name);?></p>

            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Resolved Tickets</b> <a class="pull-right">0</a>
              </li>
              <li class="list-group-item">
                <b>Unresolved Tickets</b> <a class="pull-right">0</a>
              </li>
              <li class="list-group-item">
                <b>Total Tickets</b> <a class="pull-right">0</a>
              </li>
            </ul>

            <a href="#" class="btn btn-primary btn-block"><b>View All Tickets</b></a>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">About Ticket #<?php echo $ticket_number?></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <!-- <strong><i class="fa fa-book margin-r-5"></i> Education</strong> -->
            <!-- <strong><i class="fa fa-file-text-o margin-r-5"></i> Ticket Description</strong> -->

            <p>"<?php echo $ticket_description?>"</p>
            <hr>
            <?php echo form_open("close-ticket/".$ticket_id, array("class" => "form-horizontal"));?>

            <div class="form-group" id="payment_method">
              <label class="col-md-8 control-label">General Comment</label>

              <div class="col-md-12 ">
                  <textarea class="form-control" name="general_observations" required><?php echo $general_observations;?></textarea>
                </div>
            </div>
            <?php
            // var_dump($ticket_status); die();
            if($ticket_status != 2)
            {
              ?>
                <button type="submit" class="btn btn-danger btn-block" onclick="return confirm('Are you sure you want to close this ticket ? ')"><b>Close Ticket</b></button>
              <?php
            }
            ?>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      <div class="col-md-9">

        <div class="box box-primary direct-chat direct-chat-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $ticket_number?> Conversation</h3>

            <div class="box-tools pull-right">
              <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span> -->
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle">
                <i class="fa fa-comments"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <!-- Conversations are loaded here -->
            <div class="direct-chat-messages">
                <?php echo $result;?>
            </div>
            <!--/.direct-chat-messages-->

            <!-- Contacts are loaded here -->
            <div class="direct-chat-contacts">
              <ul class="contacts-list">
                <li>
                  <a href="#">
                    <img class="contacts-list-img" src="../dist/img/user1-128x128.jpg" alt="User Image">

                    <div class="contacts-list-info">
                          <span class="contacts-list-name">
                            Count Dracula
                            <small class="contacts-list-date pull-right">2/28/2015</small>
                          </span>
                      <span class="contacts-list-msg">How have you been? I was...</span>
                    </div>
                    <!-- /.contacts-list-info -->
                  </a>
                </li>
                <!-- End Contact Item -->
              </ul>
              <!-- /.contatcts-list -->
            </div>
            <!-- /.direct-chat-pane -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <?php
            if($ticket_status != 2)
            {
              ?>
            <?php echo form_open("add-ticket-message/".$ticket_id."/".$lease_number."/".$lease_id, array("class" => "form-horizontal", "role" => "form"));?>
            <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
              <div class="input-group">
                <input type="text" name="message" placeholder="Type Message ..." class="form-control" autocomplete="off">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-primary btn-flat">Send</button>
                    </span>
              </div>
            </form>
            <?php
          }
          ?>
          </div>
          <!-- /.box-footer-->
        </div>

        <!-- /.nav-tabs-custom -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

  </section>
