<?php echo $this->load->view('search/ticket_search','', true); ?>
<?php

$result = '';

//if users exist display them
if ($query->num_rows() > 0)
{
	$count = $page;

	$result .=
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th><a>Created</a></th>
				<th><a>Ticket No.</a></th>
				<th><a>Tenant Name</a></th>
        <th><a>Phone </a></th>
				<th><a>Property </a></th>
				<th><a>Unit Name</a></th>
				<th><a>Assigned to </a></th>
				<th><a>Ticket Status</a></th>
				<th colspan="1">Actions</th>
			</tr>
		</thead>
		  <tbody>

	';


	foreach ($query->result() as $leases_row)
	{
		$lease_id = $leases_row->lease_id;
		$tenant_unit_id = $leases_row->tenant_unit_id;
		$rental_unit_id = $leases_row->unit_id;
		$rental_unit_name = $leases_row->rental_unit_name;
		$tenant_name = $leases_row->tenant_name;
		$tenant_email = $leases_row->tenant_email;
		$tenant_phone_number = $leases_row->tenant_phone_number;
		$lease_start_date = $leases_row->lease_start_date;
		$lease_duration = $leases_row->lease_duration;
		$rent_amount = $leases_row->rent_amount;
		$lease_number = $leases_row->lease_number;
		$arreas_bf = $leases_row->arrears_bf;
		$rent_calculation = $leases_row->rent_calculation;
		$property_name = $leases_row->property_name;
		$deposit = $leases_row->deposit;
		$deposit_ext = $leases_row->deposit_ext;
		$lease_status = $leases_row->lease_status;
		$points = 0;//$leases_row->points;
		$ticket_created = $leases_row->ticket_created;
    $ticket_description = $leases_row->ticket_description;
    $ticket_status = $leases_row->ticket_status;
    $ticket_number = $leases_row->ticket_number;
		$account_id = $leases_row->account_id;
    $ticket_id = $leases_row->ticket_id;

		//create deactivated status display
		if($ticket_status == 0)
		{
			$status = '<span class="label label-default"> Deactivated</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($ticket_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" onclick="return confirm(\'Do you want to deactivate '.$lease_number.'?\');" title="Deactivate '.$lease_number.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$lease_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$lease_number.'?\');" title="Delete '.$lease_number.'"><i class="fa fa-trash"></i></a></td>';

		}

			$ticket_created = date('jS M Y',strtotime($ticket_created));

				$count++;

				$send_notification_button = '<td><a class="btn btn-sm btn-default" href="'.site_url().'send-arrears/'.$lease_id.'" onclick="return confirm(\'Do you want to send an sms of the arrears ?\')"> Send Message</a></td>';


				$result .=
				'
					<tr>
						<td>'.$count.'</td>
            <td>'.$ticket_created.'</td>
						<td>'.$ticket_number.'</td>
            <td>'.$tenant_name.'</td>
            <td>'.$tenant_phone_number.'</td>
            <td>'.$property_name.'</td>
						<td>'.$rental_unit_name.'</td>
            <td></td>
						<td>'.$status.'</td>
						<td><a class="btn btn-sm btn-warning" href="'.site_url().'ticket-timeline/'.$ticket_id.'" > <i class="fa fa-folder"></i> View Ticket</a></td>

					</tr>
				';


	}

	$result .=
	'
				  </tbody>
				</table>
	';
}

else
{
	$result .= "There are no leases created";
}


$accounts_search_title = $this->session->userdata('accounts_search_title');
?>
<div class="row">
  <div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $title;?></h3>

          <div class="box-tools pull-right">
          	 <a  href="<?php echo site_url();?>ticket-management/add-ticket"   class="btn btn-sm btn-success" > <i class="fa fa-plus"></i> Add ticket </a>
    		       <!-- <a href="<?php echo site_url();?>accounts/send_notifications"  class="btn btn-sm btn-default" onclick="return confirm('Do you want to send notifications for <?php echo $accounts_search_title;?> ?')"> <i class="fa fa-outbox"></i> Send Bulk Notifications</a> -->
          </div>
        </div>
        <div class="box-body">
          <?php
            $success = $this->session->userdata('success_message');

            if(!empty($success))
            {
              echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
              $this->session->unset_userdata('success_message');
            }

            $error = $this->session->userdata('error_message');

            if(!empty($error))
            {
              echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
              $this->session->unset_userdata('error_message');
            }
            $search =  $this->session->userdata('search_tickets');
            if(!empty($search))
            {
              echo '<a href="'.site_url().'close-tickets-search" class="btn btn-sm btn-warning">Close Search</a>';
            }

            ?>
            <div class="table-responsive">

              <?php echo $result;?>

            </div>
             <div class="panel-footer">
            <?php if(isset($links)){echo $links;}?>
          </div>
        </div>
    </div>
  </div>
</div>
