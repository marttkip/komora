<?php
$tenant_where = 'tenants.tenant_id = tenant_unit.tenant_id AND tenant_unit.rental_unit_id = rental_unit.rental_unit_id  AND tenant_unit.tenant_unit_id = leases.tenant_unit_id AND leases.lease_status < 4 AND tenant_unit.tenant_unit_id = leases.tenant_unit_id' ;
$tenant_table = 'tenants,tenant_unit,rental_unit,leases';
$tenant_order = 'tenants.tenant_name';

$tenant_query = $this->tenants_model->get_tenant_list($tenant_table, $tenant_where, $tenant_order);
$rs8 = $tenant_query->result();
$tenants_list = '';
foreach ($rs8 as $tenant_rs) :
  $tenant_id = $tenant_rs->tenant_id;
  $tenant_name = $tenant_rs->tenant_name;

  $tenant_national_id = $tenant_rs->tenant_national_id;
  $tenant_phone_number = $tenant_rs->tenant_phone_number;
  $rental_unit_name = $tenant_rs->rental_unit_name;
  $tenant_number = $tenant_rs->tenant_number;
  $lease_number = $tenant_rs->lease_number;
  $lease_id = $tenant_rs->lease_id;

    $tenants_list .="<option value='".$lease_id."'>".$tenant_number."  ".$tenant_name." A/C No: ".$lease_number." ".$rental_unit_name."</option>";

endforeach;
?>
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?php echo $title;?></h3>

      <div class="box-tools pull-right">
			     <a href="<?php echo site_url().'ticket-management/unresolved-tickets'?>" class="btn btn-sm btn-warning " ><i class="fa fa arrow-left"></i> Back to tickets</a>

      </div>
    </div>
    <div class="box-body">
      <div class="row" style="margin-bottom:20px;">
        <div class="col-lg-12 col-sm-12 col-md-12">
          <div class="row">
          <?php
              echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));
          ?>
              <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-lg-5 control-label">Contract Id: </label>

                          <div class="col-lg-7">
                            <select id='lease_id' name='lease_id' class='form-control select2'>
                                <option value=''>None - Please Select a Tenant</option>
                                <?php echo $tenants_list;?>
                              </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                          <div class="form-group">
                              <label class="col-lg-5 control-label">Issue Description: </label>

                              <div class="col-lg-7">
                                <textarea class="form-control" name="ticket_description"></textarea>
                              </div>
                          </div>
                      </div>
                      </div>
                        <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">
                              <div class="form-actions text-center">
                                  <button class="submit btn btn-primary" type="submit">
                                      Add Ticket
                                  </button>
                              </div>
                          </div>
                      </div>
              </div>
              <?php echo form_close();?>
              <!-- end of form -->
            </div>


        </div>

      </div>
</div>
  </div>
</div>
