<?php
class Tickets_model extends CI_Model
{

  /*
  *	Count all items from a table
  *	@param string $table
  * 	@param string $where
  *
  */
  public function count_items($table, $where, $limit = NULL)
  {
    if($limit != NULL)
    {
      $this->db->limit($limit);
    }
    $this->db->from($table);
    $this->db->where($where);
    return $this->db->count_all_results();
  }

  /*
  *	Retrieve all tenants
  *	@param string $table
  * 	@param string $where
  *
  */
  public function get_all_tenants_tickets($table, $where, $per_page, $page, $order = 'tenants.tenant_name', $order_method = 'ASC')
  {
    //retrieve all tenants
    $this->db->from($table);
    $this->db->select('*');
    $this->db->where($where);
    // $this->db->order_by($order, $order_method);
    $query = $this->db->get('', $per_page, $page);

    return $query;
  }

  public function get_tenants_ticket_chat($ticket_id)
  {
    $this->db->from('chat');
    $this->db->select('chat.*,personnel.personnel_fname,personnel.personnel_onames');
    $this->db->where('chat.ticket_id = '.$ticket_id.'');
    $this->db->join('personnel','personnel.personnel_id = chat.sender_id','left');
    $query = $this->db->get();
    return $query;
  }

  public function add_ticket()
  {
    $this->db->where('tenant_unit.tenant_unit_id = leases.tenant_unit_id AND lease_id ='.$this->input->post('lease_id'));
    $query = $this->db->get('leases,tenant_unit');

    $rows = $query->row();
    $tenant_id = $rows->tenant_id;
    // var_dump($tenant_id); die();
		$data = array(
										'ticket_description'=>$this->input->post('ticket_description'),
										'lease_id'=>$this->input->post('lease_id'),
										'ticket_number'=>date('ymdhis'),
										'created'=>date('Y-m-d'),
										'tenant_id'=>$tenant_id,
										'created_by'=>$this->session->userdata('personnel_id')
									);

		if($this->db->insert('ticket', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
  }



}
?>
