<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup extends MX_Controller 
{	

	var $documents_path;


	function __construct()
	{
		parent:: __construct();
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('setup_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/admin_model');
		$this->load->model('admin/file_model');



		$this->documents_path = realpath(APPPATH . '../assets/uploads');
		
		if(!$this->auth_model->check_login())
		{
			redirect('login');
		}
	}
    
	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function property_index($order = 'property_name', $order_method = 'ASC') 
	{

		$where = 'property.property_id > 0';
		$table = 'property';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->setup_model->get_all_properties($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Registed Properties';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('setup/setup/all_property', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	/*
	*
	*	Add a new customer
	*
	*/
	public function property_add() 
	{
		
		//form validation rules
		$this->form_validation->set_rules('property_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('property_location', 'Location', 'required|xss_clean');
		$this->form_validation->set_rules('total_units', 'Total Units', 'required|xss_clean');
		

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->setup_model->add_property())
			{
				$this->session->set_userdata('success_message', 'Property added successfully');
				redirect('setup/property');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Property. Please try again');
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Register Property';
		$v_data['title'] = $data['title'];
	
		$data['content'] = $this->load->view('setup/setup/add_property', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

   /*
	*
	*	Edit an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function property_edit($property_id) 
	{
		//form validation rules
		$this->form_validation->set_rules('property_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('property_location', 'Location', 'required|xss_clean');
		$this->form_validation->set_rules('total_units', 'Total Units', 'required|xss_clean');
		

		//if form has been submitted
		if ($this->form_validation->run())
		{
			//update vehicle
			if($this->setup_model->update_property($property_id))
			{
				$this->session->set_userdata('success_message', 'Property updated successfully');
				redirect('setup/property');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update property. Please try again');
			}
		}
		
		//open the add new vehicle
		$data['title'] = 'Update Property Data';
		$v_data['title'] = $data['title'];
		
		//select the vehicle from the database
		$query = $this->setup_model->get_property($property_id);
		
		if ($query->num_rows() > 0)
		{
			$v_data['property'] = $query->result();
			$data['content'] = $this->load->view('setup/setup/edit_property', $v_data, true);
		}
		
		else
		{
			$data['content'] = 'Property does not exist';
		}
		
		$this->load->view('admin/templates/general_page', $data);
	}
	/*
	*
	*	Deactivate an existing Customer
	*	@param int $vehicle_id
	*
	*/
	public function deactivate_property($property_id)
	{
		if($this->setup_model->deactivate_property($property_id))
		{
			$this->session->set_userdata('success_message', 'Property disabled successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Property could not be disabled. Please try again');
		}
		redirect('setup/property');
	}
	/*
	*
	*	Activate an existing vehicle
	*	@param int $vehicle_id
	*
	*/
	public function activate_property($property_id)
	{
		if($this->setup_model->activate_property($property_id))
		{
			$this->session->set_userdata('success_message', 'Property activated successfully');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Property could not be activated. Please try again');
		}
		redirect('setup/property');
	}


	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function unit_index($property_id) 
	{
		$order = 'rental_unit.rental_unit_id';
		$order_method = 'ASC';

		$where = 'rental_unit.rental_unit_id > 0 and property.property_id = rental_unit.property_id and rental_unit.property_id = '.$property_id;
		$table = 'rental_unit, property';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->setup_model->get_all_units($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Registed Units';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['property_id'] = $property_id;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('setup/setup/all_unit', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function unit_add($property_id) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('unit_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('unit_size', 'Location', 'required|xss_clean');
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->setup_model->add_unit($property_id))
			{
				$this->session->set_userdata('success_message', 'Unit added successfully');
				redirect('setup/units/'.$property_id);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Unit. Please try again');
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Register Unit';
		$v_data['title'] = $data['title'];
	
		$data['content'] = $this->load->view('setup/setup/add_unit', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}




	/*
	*
	*	Default action is to show all the customer
	*
	*/
	public function meter_index($property_id,$rental_unit_id) 
	{
		$order = 'rental_unit.rental_unit_id';
		$order_method = 'ASC';

		$where = 'water_meter.rental_unit_id = rental_unit.rental_unit_id and rental_unit.rental_unit_id > 0  and water_meter.rental_unit_id = '.$rental_unit_id;
		$table = 'rental_unit,water_meter';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->setup_model->get_all_water_meters($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Registed Meters Per Unit';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['rental_unit_id'] = $rental_unit_id;
		$v_data['property_id'] = $property_id;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('setup/setup/all_meter', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function meter_add($property_id, $rental_unit_id) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('meter_number', 'Number', 'required|xss_clean');
		
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->setup_model->add_meter($rental_unit_id))
			{
				$this->session->set_userdata('success_message', 'Meter added successfully');
				redirect('setup/units/'.$property_id);
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not Meter Unit. Please try again');
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Register Meter';
		$v_data['title'] = $data['title'];
	
		$data['content'] = $this->load->view('setup/setup/add_meter', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function tenant_index($order = 'tenant_name', $order_method = 'ASC') 
	{

		$where = 'tenants.tenant_id > 0';
		$table = 'tenants';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'bike/customer/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->setup_model->get_all_tenants($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Register Tenants';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('setup/setup/all_tenant', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function tenant_add() 
	{
		
		//form validation rules
		$this->form_validation->set_rules('customer_surname', 'Surname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_first_name', 'Firstname', 'required|xss_clean');
		$this->form_validation->set_rules('customer_phone', 'Phone Number', 'required|xss_clean');
		$this->form_validation->set_rules('customer_email', 'Email', 'xss_clean');
		$this->form_validation->set_rules('customer_post_code', 'Postal Code', 'xss_clean');
		$this->form_validation->set_rules('customer_address', 'Address', 'xss_clean');

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->setup_model->add_tenant())
			{
				$this->session->set_userdata('success_message', 'Tenant added successfully');
				redirect('setup/tenant');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Tenant. Please try again');
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Add Tenant';
		$v_data['title'] = $data['title'];
	
		$data['content'] = $this->load->view('setup/setup/add_tenant', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function tenant_upload($tenant_id)
	{


		$this->form_validation->set_rules('document_name', 'Document Name', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//upload product's gallery images
			
			if(is_uploaded_file($_FILES['post_image']['tmp_name']))
			{
				$documents_path = $this->documents_path;

				$response = $this->file_model->upload_documents($documents_path, 'post_image');
				if($response['check'])
				{
					$file_name = $response['file_name'];
				}
			
				else
				{
					$this->session->set_userdata('error_message', $response['error']);
					
					
				}
			}
			
			else{

				$file_name = '';
			}
			
			if($this->setup_model->add_tenant_uploads($file_name, $tenant_id))
			{
				$this->session->set_userdata('success_message', 'Document was uploaded successfully added successfully');
				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add post. Please try again');
			}
		}
		
		//open the add new Customers
		$where = 'tenant_id ='.$tenant_id;
		$table = 'tenant_uploads';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'tenants-uploads/'.$tenant_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->setup_model->get_all_uploads($table, $where, $config["per_page"], $page, $order='upload_id', $order_method='ASC');
		
		$data['title'] = 'Tenant Uploads';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['tenant_id'] = $tenant_id;
		$v_data['page'] = $page;
	
		$data['content'] = $this->load->view('setup/setup/uploads_view', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function tenant_complaints($tenant_id)
	{


		$this->form_validation->set_rules('complaint', 'Complaint', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{

			if($this->setup_model->add_tenant_complaint($tenant_id))
			{
				$this->session->set_userdata('success_message', 'Complaint logged successfully');
				
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add complaint. Please try again');
			}
		}
		
		//open the add new Customers
		$where = 'tenant_id ='.$tenant_id;
		$table = 'tenant_complaints';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'tenants-complaints/'.$tenant_id;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->setup_model->get_all_complaints($table, $where, $config["per_page"], $page, $order='complaint_id', $order_method='ASC');
		
		$data['title'] = 'Tenant Complaints';
		$v_data['title'] = $data['title'];
		
		$v_data['query'] = $query;
		$v_data['tenant_id'] = $tenant_id;
		$v_data['page'] = $page;
	
		$data['content'] = $this->load->view('setup/setup/complaints_view', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


	public function unit_add_lease($rental_unit_id) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('lease_start_date', 'Start Date', 'required|xss_clean');
		$this->form_validation->set_rules('lease_end_date', 'End Date', 'required|xss_clean');
		$this->form_validation->set_rules('tenant_id', 'Tenant Data', 'required|xss_clean');

		$this->form_validation->set_rules('rent_amount', 'Rent', 'required|xss_clean');
		$this->form_validation->set_rules('rent_amount_period', 'Rent Period', 'required|xss_clean');
		$this->form_validation->set_rules('rent_deposit', 'Rent Deposit', 'required|xss_clean');
		$this->form_validation->set_rules('water_deposit', 'Water Deposit', 'required|xss_clean');
		$this->form_validation->set_rules('electricity_deposit', 'Electricity Deposit', 'required|xss_clean');

		$this->form_validation->set_rules('garbage_charges', 'Garbage Charges', 'required|xss_clean');
		$this->form_validation->set_rules('garbage_charges_period', 'Garbage Charge Period', 'required|xss_clean');
		

		$this->form_validation->set_rules('security_charges', 'Security Charges', 'required|xss_clean');
		$this->form_validation->set_rules('security_charges_period', 'Electricity Charge Period', 'required|xss_clean');

		$this->form_validation->set_rules('water_charges', 'Water Charge', 'required|xss_clean');
		$this->form_validation->set_rules('water_charges_period', 'Water Charge Period', 'required|xss_clean');

		$this->form_validation->set_rules('rent_arrears', 'Rent Arrears', 'required|xss_clean');
		$this->form_validation->set_rules('garbage_arrears', 'Garbage Arrears', 'required|xss_clean');
		$this->form_validation->set_rules('security_arrears', 'Security Arrears', 'required|xss_clean');
		$this->form_validation->set_rules('water_arrears', 'Water Arrears', 'required|xss_clean');
		$this->form_validation->set_rules('electricity_arrears', 'Electricity Arrears', 'required|xss_clean');
		

		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->setup_model->add_lease($rental_unit_id))
			{
				$this->session->set_userdata('success_message', 'Lease added successfully');
				redirect('setup/property');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Lease. Please try again');
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Register Lease';
		$v_data['title'] = $data['title'];
		$v_data['rental_unit_id'] = $rental_unit_id;
		$v_data['products'] = $this->setup_model->all_tenants();
	
		$data['content'] = $this->load->view('setup/setup/add_leases', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function invoice_type_index()
	{
		$data['title'] = "Invoice Types";
		$v_data['title'] = $data['title'];
		$v_data['query'] = $this->setup_model->get_invoice_types();
		
		$data['content'] = $this->load->view('setup/setup/all_invoice_types', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function invoice_type_add() 
	{
		
		//form validation rules
		$this->form_validation->set_rules('invoice_type_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('invoice_type_code', 'Code', 'required|xss_clean');
	
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->setup_model->add_invoice_type())
			{
				$this->session->set_userdata('success_message', 'Invoice Type added successfully');
				redirect('setup/invoice-type');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Invoice Type. Please try again');
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Register Invoice Type';
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('setup/setup/add_invoice_type', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function invoice_billing_schedule_index()
	{
		$data['title'] = "Invoice Billing Schedule";
		$v_data['title'] = $data['title'];
		$v_data['query'] = $this->setup_model->get_billing_schedule();
		
		$data['content'] = $this->load->view('setup/setup/all_billing_schedule', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function invoice_billing_schedule_add() 
	{
		
		//form validation rules
		$this->form_validation->set_rules('billing_schedule_name', 'Name', 'required|xss_clean');
				//if form has been submitted
		if ($this->form_validation->run())
		{
			
			if($this->setup_model->add_billing_schedule())
			{
				$this->session->set_userdata('success_message', 'Billing Schedule added successfully');
				redirect('setup/billing-schedule');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not add Billing Schedule. Please try again');
			}
		}
		
		//open the add new Customers
		
		$data['title'] = 'Register Billing Schedule';
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('setup/setup/add_billing_schedule', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	



}
?>