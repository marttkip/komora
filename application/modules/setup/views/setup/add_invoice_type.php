<?php
//personnel data
$invoice_type_name = set_value('invoice_type_name');
$invoice_type_code = set_value('invoice_type_code');


?>   
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>setup/invoice-type" class="btn btn-info pull-right">Back to Setup</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                        $success = $this->session->userdata('success_message');
                        $error = $this->session->userdata('error_message');
                        
                        if(!empty($success))
                        {
                            echo '
                                <div class="alert alert-success">'.$success.'</div>
                            ';
                            
                            $this->session->unset_userdata('success_message');
                        }
                        
                        if(!empty($error))
                        {
                            echo '
                                <div class="alert alert-danger">'.$error.'</div>
                            ';
                            
                            $this->session->unset_userdata('error_message');
                        }
                        $validation_errors = validation_errors();
                        
                        if(!empty($validation_errors))
                        {
                            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                        }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
          <div class="form-group">
            <label class="col-lg-5 control-label">Invoice Type Code: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="invoice_type_code" placeholder="Code" value="<?php echo $invoice_type_code;?>">
            </div>
        </div>
        
        
     
         
        
    </div>
    
    <div class="col-md-6">
        
       <div class="form-group">
            <label class="col-lg-5 control-label">Invoice Type Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="invoice_type_name" placeholder="Name" value="<?php echo $invoice_type_name;?>">
            </div>
        </div>
       
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add Invoice Type
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>