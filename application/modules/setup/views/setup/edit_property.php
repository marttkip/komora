
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>setup/property" class="btn btn-info pull-right">Back to Properties</a>
                        </div>
                    </div>
                    <!-- Adding Errors -->
            <?php
            if(isset($error)){
                echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
            }
            
			//the vehicle details
			$property_id = $property[0]->property_id;
			$property_name = $property[0]->property_name;
			$property_location = $property[0]->property_location;
			$total_units = $property[0]->total_units;
		
            
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
				$property_name = set_value('property_name');
				$property_location = set_value('property_location');
				$total_units = set_value('total_units');
				
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
			
            ?>
            
          <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
    <div class="col-md-6">
          <div class="form-group">
            <label class="col-lg-5 control-label">Property Name: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="property_name" placeholder="Property Name" value="<?php echo $property_name;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Property Location: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="property_location" placeholder="Property Location" value="<?php echo $property_location;?>">
            </div>
        </div>
        
     
         
        
    </div>
    
    <div class="col-md-6">
        
       
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Total Units: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="total_units" placeholder="Total Units" value="<?php echo $total_units;?>">
            </div>
        </div>
        
       
    

    </div>
</div>
<div class="row" style="margin-top:10px;">
    <div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Edit Property
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>