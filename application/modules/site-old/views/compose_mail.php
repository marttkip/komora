<?php

if(count($contacts) > 0)
{
	$email = $contacts['email'];
	$facebook = $contacts['facebook'];
	$linkedin = $contacts['linkedin'];
	$logo = $contacts['logo'];
	$company_name = $contacts['company_name'];
	$phone = $contacts['phone'];
	$address = $contacts['address'];
	$post_code = $contacts['post_code'];
	$city = $contacts['city'];
	$building = $contacts['building'];
	$floor = $contacts['floor'];
	$location = $contacts['location'];
	
	$working_weekday = $contacts['working_weekday'];
	$working_weekend = $contacts['working_weekend'];
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="print"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="print">
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
        </style>
    </head>
    <body class="receipt_spacing">
    	    
        <div class="row receipt_bottom_border">
        	<div class="col-md-12">
            	<?php echo $text;?>
                <p>Thank You</p>
                <p><?php echo $company_name;?></p>
            </div>
        </div>
        
        <div class="row">
        	<div class="col-md-12">
                <table class="table" style="width:100%">
                    <tr>
                        <th class="align-right" style="width:50%; text-align:left;">
                            <?php echo $company_name;?><br/>
                            <?php echo $address;?> <?php echo $post_code;?> <?php echo $city;?><br/>
                            E-mail: <?php echo $email;?><br/>
                            Tel : <?php echo $phone;?><br/>
                            <?php echo $location;?> <?php echo $building;?> <?php echo $floor;?>
                        </th>
                        <th style="width:50%">
                            <img src="<?php echo base_url().'assets/logo/'.$logo;?>" alt="<?php echo $company_name;?>" class="img-responsive logo" style="float:right;" width="200"/>
                        </th>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
