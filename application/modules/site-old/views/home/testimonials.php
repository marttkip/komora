		<section class="section-light-grey section-review" id="reviews">
            <div class="container-fluid">
                
                <!-- START SECTION HEADER -->
                <h2 class="reveal reveal-top">Reviews</h2>
                <!-- END SECTION HEADER -->
                
                <!-- START WIDGET: CAROUSEL --> 
                <div class="reviews-carousel reveal reveal-bottom">
					<div class="review-wrapper">
                        <div class="review">
                            Well Done!!
                        </div>
                        <div class="review-author">
                            <div class="media">
                                <div class="media-left">
                                    <a href="index.html#">
                                        <!-- Set here an author icon. The image sets from remote server -->
                                        <img class="media-object author-icon" src="<?php echo base_url();?>assets/img/avatar.jpg" alt="author icon"/>
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="author-name">Alvaro</h4>
                                    <div class="author-company">Managing Director</div>
                                </div>
                            </div>
                        </div>
                    </div>

					<div class="review-wrapper">
                        <div class="review">
                            Revolutionary
                        </div>
                        <div class="review-author">
                            <div class="media">
                                <div class="media-left">
                                    <a href="index.html#">
                                        <!-- Set here an author icon. The image sets from remote server -->
                                        <img class="media-object author-icon" src="<?php echo base_url();?>assets/img/avatar.jpg" alt="author icon"/>
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="author-name">Betty</h4>
                                    <div class="author-company">Accountant</div>
                                </div>
                            </div>
                        </div>
                    </div>

					<div class="review-wrapper">
                        <div class="review">
                            Convenient and affordable
                        </div>
                        <div class="review-author">
                            <div class="media">
                                <div class="media-left">
                                    <a href="index.html#">
                                        <!-- Set here an author icon. The image sets from remote server -->
                                        <img class="media-object author-icon" src="<?php echo base_url();?>assets/img/avatar.jpg" alt="author icon"/>
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="author-name">Terry</h4>
                                    <div class="author-company">Project Manager</div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
                <!-- END WIDGET: CAROUSEL --> 

            </div>
        </section>
        