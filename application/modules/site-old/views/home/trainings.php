
        <!-- START VIDEO SECTION -->
        <section class="section-video" id="video" data-parallax="scroll" data-image-src="<?php echo base_url()."assets/themes/theme/";?>assets/img/decore/video_background.jpg" data-z-index="9998">      <!-- Set the URL of your section background image to "data-image-src" attr. -->      
            <!--You can add a poster to the video. Set the image URL into the data-image attribute for this. --> 
            <div class="video-start-btn" data-toggle="modal" data-target=".modal-video" >
                
                <!-- START SECTION HEADER -->
                <h2 class="reveal reveal-top">How It Works</h2>
                <div class="subheading reveal reveal-top" >
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do 
                    eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </div>
                <!-- END SECTION HEADER -->
                
                <div class="video-play fa fa-play"></div>
            </div>
        </section>
        <!-- START VIDEO SECTION -->
        
        <!-- START MODAL -->
        <section class="modal fade modal-video" role="dialog">
            <!-- The modal opens when a user clicks the video section -->
            <div class="modal-dialog">
                <div class="modal-content">
                    
                    <!-- START MODAL HEADER -->
                    <div class="modal-header">
                        <h2 class="modal-title"></h2>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- END MODAL HEADER -->
                    
                    <!-- START MODAL BODY -->
                    <div class="modal-body">
                        <!-- JS CODE ADDS VIDEO HERE DYNAMICALLY -->
                    </div>
                    <!-- END MODAL BODY -->
                    
                </div>
            </div>
        </section>
        <!-- END MODAL -->
        