<?php
$partners_result = '';
if($partners->num_rows() > 0)
	{
		foreach($partners->result() as $partners)
		{
			$partners_name = $partners->partners_name;
			$description = $partners->partners_description;
			$partners_image = $partners->partners_image_name;
			$partners_type_id = $partners->partner_type_id;
			$partners_link = $partners->partners_link;
			$partners_button_text = $partners->partners_button_text;
			$description = $this->site_model->limit_text($description, 8);
			
			$partners_result .= ' 
				<li class="item"><img src="'.base_url().'assets/partners/'.$partners_image.'" alt="'.$partners_name.'"></li>
			';
		}
	}

 ?>            
			<!-- client section -->
            <section>
                <div class="container pad-container">

                    <div class="col-md-6">
                        <div class="row">
                            <h3 class="heading">Our Partners</h3>
                       
                                <div class="clients">
                                    <ul>
                                        <?php echo $partners_result;?>
                                    </ul>
                                </div>
                           
                        </div>                        
                    
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                        
                            <h3 class="heading">What People Say</h3>
                       
                                <div class="owl-testimonials owl-carousel t-pad20">
                                
                                    <div class="item">
                                        <div class="testimonials">
                                            <div class="testimonials-content1">
                                            <p> “Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. eiusmod tempor incididunt ut labore et dolore magna aliquea. Ut enim ad minim veniam, quis nostrud exercitation Ut enim ad minim veniam, quis nostrud exercitation ullamco. Etiam sit amet fringilla lacus. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor." </p>
                                            </div>
                                            <div class="testimonials-author">
                                                    <img src="<?php base_url();?>assets/images/avatar.png" alt="">
                                                    <p class="colored"> Amos Macharia - CEO </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="item">
                                        <div class="testimonials">
                                            <div class="testimonials-content1">
                                            <p> “Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. eiusmod tempor incididunt ut labore et dolore magna aliquea. Ut enim ad minim veniam, quis nostrud exercitation Ut enim ad minim veniam, quis nostrud exercitation ullamco. Etiam sit amet fringilla lacus. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor." </p>
                                            </div>
                                            <div class="testimonials-author">
                                                    <img src="<?php base_url();?>assets/images/avatar.png" alt="">
                                                    <p class="colored"> Peter Maingi - Director </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="item">
                                        <div class="testimonials">
                                            <div class="testimonials-content1">
                                            <p> “Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. eiusmod tempor incididunt ut labore et dolore magna aliquea. Ut enim ad minim veniam, quis nostrud exercitation Ut enim ad minim veniam, quis nostrud exercitation ullamco. Etiam sit amet fringilla lacus. Pellentesque suscipit ante at ullamcorper pulvinar neque porttitor." </p>
                                            </div>
                                            <div class="testimonials-author">
                                                    <img src="<?php base_url();?>assets/images/avatar.png" alt="">
                                                    <p class="colored"> Jack Mwaura - Manager  </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                           
                       		 </div>                        
                    
                   		 </div>

                	</div>
                    
                </div>
                
            </section>
            <!--/ client section -->
            