<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['scaffolding_trigger'] = 'scaffolding';
|
| This route lets you set a "secret" word that will trigger the
| scaffolding feature for added security. Note: Scaffolding must be
| enabled in the controller in which you intend to use it.   The reserved
| routes must come before any wildcard or regular expression routes.
|
*/

$route['default_controller'] = "auth";
$route['404_override'] = '';

/*
*	Auth Routes
*/
$route['login'] = 'auth/login_user';
$route['logout-admin'] = 'auth/logout';

/*
*	Admin Routes
*/
$route['dashboard'] = 'admin/dashboard';
$route['change-password'] = 'admin/users/change_password';


/*
*	administration Routes
*/
$route['administration/configuration'] = 'admin/configuration';
$route['administration/edit-configuration'] = 'admin/edit_configuration';
$route['administration/edit-configuration/(:num)'] = 'admin/edit_configuration/$1';
$route['administration/sections'] = 'admin/sections/index';
$route['administration/sections/(:any)/(:any)/(:num)'] = 'admin/sections/index/$1/$2/$3';
$route['administration/add-section'] = 'admin/sections/add_section';
$route['administration/edit-section/(:num)'] = 'admin/sections/edit_section/$1';

$route['administration/edit-section/(:num)/(:num)'] = 'admin/sections/edit_section/$1/$2';
$route['administration/delete-section/(:num)'] = 'admin/sections/delete_section/$1';
$route['administration/delete-section/(:num)/(:num)'] = 'admin/sections/delete_section/$1/$2';
$route['administration/activate-section/(:num)'] = 'admin/sections/activate_section/$1';
$route['administration/activate-section/(:num)/(:num)'] = 'admin/sections/activate_section/$1/$2';
$route['administration/deactivate-section/(:num)'] = 'admin/sections/deactivate_section/$1';
$route['administration/deactivate-section/(:num)/(:num)'] = 'admin/sections/deactivate_section/$1/$2';

#$route['administration/company-profile'] = 'admin/contacts/show_contacts';
$route['administration/branches'] = 'admin/branches/index';
$route['administration/branches/(:any)/(:any)/(:num)'] = 'admin/branches/index/$1/$2/$3';
$route['administration/branches/(:any)/(:any)'] = 'admin/branches/index/$1/$2';
$route['administration/add-branch'] = 'admin/branches/add_branch';
$route['administration/edit-branch/(:num)'] = 'admin/branches/edit_branch/$1';
$route['administration/edit-branch/(:num)/(:num)'] = 'admin/branches/edit_branch/$1/$2';
$route['administration/delete-branch/(:num)'] = 'admin/branches/delete_branch/$1';
$route['administration/delete-branch/(:num)/(:num)'] = 'admin/branches/delete_branch/$1/$2';
$route['administration/activate-branch/(:num)'] = 'admin/branches/activate_branch/$1';
$route['administration/activate-branch/(:num)/(:num)'] = 'admin/branches/activate_branch/$1/$2';
$route['administration/deactivate-branch/(:num)'] = 'admin/branches/deactivate_branch/$1';
$route['administration/deactivate-branch/(:num)/(:num)'] = 'admin/branches/deactivate_branch/$1/$2';

/*
*	HR Routes
*/
$route['human-resource/my-account'] = 'admin/dashboard';
$route['human-resource/my-account/edit-about/(:num)'] = 'hr/personnel/my_account/update_personnel_about_details/$1';
$route['human-resource/edit-personnel-account/(:num)'] = 'hr/personnel/update_personnel_account_details/$1';
$route['human-resource/configuration'] = 'hr/configuration';
$route['human-resource/add-job-title'] = 'hr/add_job_title';
$route['human-resource/edit-job-title/(:num)'] = 'hr/edit_job_title/$1';
$route['human-resource/delete-job-title/(:num)'] = 'hr/delete_job_title/$1';
$route['human-resource/personnel'] = 'hr/personnel/index';
$route['human-resource/personnel/(:any)/(:any)/(:num)'] = 'hr/personnel/index/$1/$2/$3';
$route['human-resource/add-personnel'] = 'hr/personnel/add_personnel';
$route['human-resource/edit-personnel/(:num)'] = 'hr/personnel/edit_personnel/$1';
$route['human-resource/edit-personnel-about/(:num)'] = 'hr/personnel/update_personnel_about_details/$1';
$route['human-resource/edit-personnel-account/(:num)'] = 'hr/personnel/update_personnel_account_details/$1';
$route['human-resource/edit-personnel/(:num)/(:num)'] = 'hr/personnel/edit_personnel/$1/$2';
$route['human-resource/delete-personnel/(:num)'] = 'hr/personnel/delete_personnel/$1';
$route['human-resource/delete-personnel/(:num)/(:num)'] = 'hr/personnel/delete_personnel/$1/$2';
$route['human-resource/activate-personnel/(:num)'] = 'hr/personnel/activate_personnel/$1';
$route['human-resource/activate-personnel/(:num)/(:num)'] = 'hr/personnel/activate_personnel/$1/$2';
$route['human-resource/deactivate-personnel/(:num)'] = 'hr/personnel/deactivate_personnel/$1';
$route['human-resource/deactivate-personnel/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel/$1/$2';
$route['human-resource/reset-password/(:num)'] = 'hr/personnel/reset_password/$1';
$route['human-resource/update-personnel-roles/(:num)'] = 'hr/personnel/update_personnel_roles/$1';
$route['human-resource/add-emergency-contact/(:num)'] = 'hr/personnel/add_emergency_contact/$1';
$route['human-resource/activate-emergency-contact/(:num)/(:num)'] = 'hr/personnel/activate_emergency_contact/$1/$2';
$route['human-resource/deactivate-emergency-contact/(:num)/(:num)'] = 'hr/personnel/deactivate_emergency_contact/$1/$2';
$route['human-resource/delete-emergency-contact/(:num)/(:num)'] = 'hr/personnel/delete_emergency_contact/$1/$2';

$route['human-resource/add-dependant-contact/(:num)'] = 'hr/personnel/add_dependant_contact/$1';
$route['human-resource/activate-dependant-contact/(:num)/(:num)'] = 'hr/personnel/activate_dependant_contact/$1/$2';
$route['human-resource/deactivate-dependant-contact/(:num)/(:num)'] = 'hr/personnel/deactivate_dependant_contact/$1/$2';
$route['human-resource/delete-dependant-contact/(:num)/(:num)'] = 'hr/personnel/delete_dependant_contact/$1/$2';

$route['human-resource/add-personnel-job/(:num)'] = 'hr/personnel/add_personnel_job/$1';
$route['human-resource/activate-personnel-job/(:num)/(:num)'] = 'hr/personnel/activate_personnel_job/$1/$2';
$route['human-resource/deactivate-personnel-job/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel_job/$1/$2';
$route['human-resource/delete-personnel-job/(:num)/(:num)'] = 'hr/personnel/delete_personnel_job/$1/$2';


$route['human-resource/add-personnel-property/(:num)'] = 'hr/personnel/add_personnel_properties/$1';
$route['human-resource/activate-personnel-property/(:num)/(:num)'] = 'hr/personnel/activate_personnel_properties/$1/$2';
$route['human-resource/deactivate-personnel-property/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel_properties/$1/$2';
$route['human-resource/delete-personnel-property/(:num)/(:num)'] = 'hr/personnel/delete_personnel_properties/$1/$2';


$route['human-resource/leave'] = 'hr/leave/calender';
$route['human-resource/leave/(:any)/(:any)'] = 'hr/leave/calender/$1/$2';
$route['human-resource/view-leave/(:any)'] = 'hr/leave/view_leave/$1';
$route['human-resource/add-personnel-leave/(:num)'] = 'hr/personnel/add_personnel_leave/$1';
$route['human-resource/add-leave/(:any)'] = 'hr/leave/add_leave/$1';
$route['human-resource/add-calender-leave'] = 'hr/leave/add_calender_leave';
$route['human-resource/activate-leave/(:num)/(:any)'] = 'hr/leave/activate_leave/$1/$2';
$route['human-resource/deactivate-leave/(:num)/(:any)'] = 'hr/leave/deactivate_leave/$1/$2';
$route['human-resource/delete-leave/(:num)/(:any)'] = 'hr/leave/delete_leave/$1/$2';
$route['human-resource/activate-personnel-leave/(:num)/(:num)'] = 'hr/personnel/activate_personnel_leave/$1/$2';
$route['human-resource/deactivate-personnel-leave/(:num)/(:num)'] = 'hr/personnel/deactivate_personnel_leave/$1/$2';
$route['human-resource/delete-personnel-leave/(:num)/(:num)'] = 'hr/personnel/delete_personnel_leave/$1/$2';

$route['human-resource/delete-personnel-role/(:num)/(:num)'] = 'hr/personnel/delete_personnel_role/$1/$2';


/*
*	Accounts Routes
*/
$route['accounts/closed-visits'] = 'accounts/payroll/accounts_closed_visits';
$route['accounts/un-closed-visits'] = 'accounts/payroll/accounts_unclosed_queue';
$route['accounts/change-branch'] = 'accounts/payroll/change_branch';

$route['accounts/activate_payments/(:num)/(:num)'] = 'accounts/accounts/activate_payment/$1/$2';
$route['accounts/deactivate_payments/(:num)/(:num)'] = 'accounts/accounts/deactivate_payment/$1/$2';

$route['accounts/print-paye-report/(:num)'] = 'accounts/payroll/print_paye_report/$1';
$route['accounts/print-nhif-report/(:num)'] = 'accounts/payroll/print_nhif_report/$1';
$route['accounts/print-nssf-report/(:num)'] = 'accounts/payroll/print_nssf_report/$1';
$route['accounts/print-payroll/(:num)'] = 'accounts/payroll/print_payroll/$1';
$route['accounts/print-month-payslips/(:num)'] = 'accounts/payroll/print_monthly_payslips/$1';
$route['accounts/export-payroll/(:num)'] = 'accounts/payroll/export_payroll/$1';
$route['accounts/print-payroll-pdf/(:num)'] = 'accounts/payroll/print_payroll_pdf/$1';
$route['accounts/payroll/print-payslip/(:num)/(:num)'] = 'accounts/payroll/print_payslip/$1/$2';
$route['accounts/payroll/download-payslip/(:num)/(:num)'] = 'accounts/payroll/download_payslip/$1/$2';
$route['accounts/payroll-payslips/(:num)'] = 'accounts/payroll/payroll_payslips/$1';
$route['accounts/salary-data'] = 'accounts/payroll/salaries';
$route['accounts/search-payroll'] = 'accounts/payroll/search_payroll';
$route['accounts/close-payroll-search'] = 'accounts/payroll/close_payroll_search';
$route['accounts/create-payroll'] = 'accounts/payroll/create_payroll';
$route['accounts/deactivate-payroll/(:num)'] = 'accounts/payroll/deactivate_payroll/$1';
$route['accounts/print-payslips'] = 'accounts/payroll/print_payslips';
$route['accounts/payroll/edit-payment-details/(:num)'] = 'accounts/payroll/edit_payment_details/$1';
$route['accounts/payroll/edit_allowance/(:num)'] = 'accounts/payroll/edit_allowance/$1';
$route['accounts/payroll/delete_allowance/(:num)'] = 'accounts/payroll/delete_allowance/$1';
$route['accounts/payroll/edit_deduction/(:num)'] = 'accounts/payroll/edit_deduction/$1';
$route['accounts/payroll/delete_deduction/(:num)'] = 'accounts/payroll/delete_deduction/$1';
$route['accounts/payroll/edit_saving/(:num)'] = 'accounts/payroll/edit_saving/$1';
$route['accounts/payroll/delete_saving/(:num)'] = 'accounts/payroll/delete_saving/$1';
$route['accounts/payroll/edit_loan_scheme/(:num)'] = 'accounts/payroll/edit_loan_scheme/$1';
$route['accounts/payroll/delete_loan_scheme/(:num)'] = 'accounts/payroll/delete_loan_scheme/$1';
$route['accounts/payroll'] = 'accounts/payroll/payrolls';
$route['accounts/all-payroll'] = 'accounts/payroll/all_payrolls';
$route['accounts/payment-details/(:num)'] = 'accounts/payroll/payment_details/$1';
$route['accounts/save-payment-details/(:num)'] = 'accounts/payroll/save_payment_details/$1';
$route['accounts/update-savings/(:num)'] = 'accounts/payroll/update_savings/$1';
$route['accounts/update-loan-schemes/(:num)'] = 'accounts/payroll/update_loan_schemes/$1';
$route['payroll/configuration'] = 'accounts/payroll/payroll_configuration';
$route['accounts/payroll-configuration'] = 'accounts/payroll/payroll_configuration';
$route['accounts/payroll/edit-nssf/(:num)'] = 'accounts/payroll/edit_nssf/$1';
$route['accounts/payroll/edit-nhif/(:num)'] = 'accounts/payroll/edit_nhif/$1';
$route['accounts/payroll/delete-nhif/(:num)'] = 'accounts/payroll/delete_nhif/$1';
$route['accounts/payroll/edit-paye/(:num)'] = 'accounts/payroll/edit_paye/$1';
$route['accounts/payroll/delete-paye/(:num)'] = 'accounts/payroll/delete_paye/$1';
$route['accounts/payroll/edit-payment/(:num)'] = 'accounts/payroll/edit_payment/$1';
$route['accounts/payroll/delete-payment/(:num)'] = 'accounts/payroll/delete_payment/$1';
$route['accounts/payroll/edit-benefit/(:num)'] = 'accounts/payroll/edit_benefit/$1';
$route['accounts/payroll/delete-benefit/(:num)'] = 'accounts/payroll/delete_benefit/$1';
$route['accounts/payroll/edit-allowance/(:num)'] = 'accounts/payroll/edit_allowance/$1';
$route['accounts/payroll/delete-allowance/(:num)'] = 'accounts/payroll/delete_allowance/$1';
$route['accounts/payroll/edit-deduction/(:num)'] = 'accounts/payroll/edit_deduction/$1';
$route['accounts/payroll/edit-relief/(:num)'] = 'accounts/payroll/edit_relief/$1';
$route['accounts/payroll/delete-deduction/(:num)'] = 'accounts/payroll/delete_deduction/$1';
$route['accounts/payroll/edit-other-deduction/(:num)'] = 'accounts/payroll/edit_other_deduction/$1';
$route['accounts/payroll/delete-other-deduction/(:num)'] = 'accounts/payroll/delete_other_deduction/$1';
$route['accounts/payroll/edit-loan-scheme/(:num)'] = 'accounts/payroll/edit_loan_scheme/$1';
$route['accounts/payroll/delete-loan-scheme/(:num)'] = 'accounts/payroll/delete_loan_scheme/$1';
$route['accounts/payroll/edit-saving/(:num)'] = 'accounts/payroll/edit_saving/$1';
$route['accounts/payroll/delete-saving/(:num)'] = 'accounts/payroll/delete_saving/$1';
$route['accounts/payroll/edit-personnel-payments/(:num)'] = 'accounts/payroll/edit_personnel_payments/$1';
$route['accounts/payroll/edit-personnel-allowances/(:num)'] = 'accounts/payroll/edit_personnel_allowances/$1';
$route['accounts/payroll/edit-personnel-benefits/(:num)'] = 'accounts/payroll/edit_personnel_benefits/$1';
$route['accounts/payroll/edit-personnel-deductions/(:num)'] = 'accounts/payroll/edit_personnel_deductions/$1';
$route['accounts/payroll/edit-personnel-other-deductions/(:num)'] = 'accounts/payroll/edit_personnel_other_deductions/$1';
$route['accounts/payroll/edit-personnel-savings/(:num)'] = 'accounts/payroll/edit_personnel_savings/$1';
$route['accounts/payroll/edit-personnel-loan-schemes/(:num)'] = 'accounts/payroll/edit_personnel_loan_schemes/$1';
$route['accounts/payroll/edit-personnel-relief/(:num)'] = 'accounts/payroll/edit_personnel_relief/$1';
$route['accounts/payroll/view-payslip/(:num)'] = 'accounts/payroll/view_payslip/$1';
$route['accounts/payroll/generate-batch-payroll/(:num)/(:num)/(:num)'] = 'accounts/payroll/generate_payroll/$1/$2/$3';
$route['accounts/payroll/generate-batch-payroll/(:num)/(:num)/(:num)/(:num)'] = 'accounts/payroll/generate_payroll/$1/$2/$3/$4';
$route['accounts/payroll/view-batch-payslip/(:num)/(:num)'] = 'accounts/payroll/view_batch_payslip/$1/$2';
$route['accounts/payroll/send-batch-payslip/(:num)/(:num)'] = 'accounts/payroll/send_batch_payslip/$1/$2';
$route['accounts/print-month-summary/(:num)/(:num)'] = 'accounts/payroll/month_summary/$1/$2';
$route['accounts/print-month-payslips2/(:num)'] = 'accounts/payroll/print_monthly_payslips2/$1';
$route['payroll/add-overtime-hours/(:num)'] = 'accounts/payroll/add_overtime_hours/$1';
$route['accounts/create-data-file/(:num)/(:num)'] = 'accounts/payroll/create_data_file/$1/$2';
$route['accounts/list-batches/(:num)/(:num)'] = 'accounts/payroll/list_batches/$1/$2';
$route['accounts/list-batches/(:num)/(:num)/(:num)'] = 'accounts/payroll/list_batches/$1/$2/$3';

//import personnel routes
$route['import/personnel'] = 'hr/personnel/import_personnel';
$route['import/personnel-template'] = 'hr/personnel/import_personnel_template';
$route['import/import-personnel'] = 'hr/personnel/do_personnel_import';

//import personnel emails
$route['import/personnel-emails'] = 'hr/personnel/import_personnel_emails';
$route['import/personnel-emails-template'] = 'hr/personnel/import_personnel_emails_template';
$route['import/import-personnel-emails'] = 'hr/personnel/do_personnel_emails_import';

//import branches routes
$route['import/branches'] = 'admin/branches/import_branches';
$route['import/branches-template'] = 'admin/branches/import_branches_template';
$route['import/import-branches'] = 'admin/branches/do_branches_import';

//payroll data import
$route['import/payroll-data'] = 'hr/import_payroll';
$route['import/payroll-template'] = 'hr/import_payroll_template';
$route['import/import-payroll']= 'hr/do_payroll_import';

//import salary advances
$route['salary-advance/import-salary-advance'] = 'accounts/salary_advance/import_salary_advance';
$route['accounts/salary-advance/import-salary-advance'] = 'accounts/salary_advance/import_salary_advance';
$route['import/import-salary-advances'] = 'accounts/salary_advance/do_advance_import';
$route['import/advance-template'] = 'accounts/salary_advance/advances_template';
$route['download-salary-advance'] = 'accounts/salary_advance/download_salary_advance';

// p9 form
$route['my-account/p9'] = 'accounts/payroll/generate_p9_form';
$route['accounts/generate_p9_form'] = 'accounts/payroll/p9_form';

//p10 form
$route['accounts/p10'] = 'accounts/payroll/generate_p10_form';
$route['accounts/generate_p10_form'] = 'accounts/payroll/p10_form';

//timesheets
$route['timesheets/add-timesheet'] = 'hr/personnel/add_personnel_timesheet';

//bank reports
$route['accounts/bank'] = 'accounts/payroll/bank';
$route['accounts/generate-bank-report/(:num)'] = 'accounts/payroll/generate_bank_report/$1';
//petty cash
$route['accounts/petty-cash'] = 'accounts/petty_cash/index';
$route['accounts/petty-cash/(:any)/(:any)'] = 'accounts/petty_cash/index/$1/$2';
$route['accounts/petty-cash/(:any)'] = 'accounts/petty_cash/index/$1';
//salary advances
$route['salary-advance'] = 'accounts/salary_advance/index';
$route['accounts/salary-advance'] = 'accounts/salary_advance/index';
$route['accounts/search-advances'] = 'accounts/salary_advance/search_salary_advance';
$route['close-salary-advance-search'] = 'accounts/salary_advance/close_advance_search';
$route['salary-advance/(:any)/(:any)'] = 'accounts/salary_advance/index/$1/$2';

//creditors
$route['accounts/creditors'] = 'accounts/creditors/index';
//payroll reports routes
$route['accounts/payroll-reports'] = 'accounts/payroll/payroll_report';
$route['accounts/search-payroll-reports'] = 'accounts/payroll/search_payroll_reports';


//inflows
$route['accounts/inflows'] = 'accounts/inflows/index';
$route['accounts/inflows/add_inflows'] = 'accounts/inflows/add_inflow';
$route['accounts/inflow-services'] = 'inflows/inflow_service/index';
$route['add-inflow-service'] = 'inflows/inflow_service/add_inflow_service';





//account balances
$route['accounts/account-balances'] = 'accounts/petty_cash/account_balances';
$route['accounts/account-balances/activate-account/(:num)'] = 'accounts/petty_cash/activate_account/$1';
$route['accounts/account-balances/deactivate-account/(:num)'] = 'accounts/petty_cash/deactivate_account/$1';
$route['accounts/account-balances/edit-account/(:num)'] = 'accounts/petty_cash/edit_account/$1';
$route['accounts/add-account'] = 'accounts/petty_cash/add_account';

//import overtime-hours
$route['import/overtime'] = 'accounts/payroll/import_overtime';
$route['import/overtime-template'] = 'accounts/payroll/import_overtime_template';
$route['import/import-overtime'] = 'accounts/payroll/do_overtime_import';

//send payslips to the specific personnel
$route['accounts/send-month-payslips/(:num)'] = 'accounts/payroll/send_monthly_payslips/$1';
$route['accounts/payroll/access-payslip/(:num)/(:num)'] = 'accounts/payroll/access_payslip/$1/$2';

//payslips per duration
$route['my-account/payslips'] = 'accounts/payroll/generate_duration_payslips';
$route['accounts/generate-personnel-payslips-duration'] = 'accounts/payroll/generate_personnel_payslip_for_duration';


//Always comes last
$route['accounts/payroll/(:any)/(:any)'] = 'accounts/payroll/payrolls/$1/$2';
$route['accounts/payroll/(:any)/(:any)/(:num)'] = 'accounts/payroll/payrolls/$1/$2/$3';
$route['accounts/salary-data/(:any)/(:any)'] = 'accounts/payroll/salaries/$1/$2';
$route['accounts/salary-data/(:any)/(:any)/(:num)'] = 'accounts/payroll/salaries/$1/$2/$3';




/*
*	Messaging Routes
*/

$route['messaging/dashboard'] = 'messagin/dashboard';
$route['messages'] = 'messaging/unsent_messages';
$route['messaging/unsent-messages'] = 'messaging/unsent_messages';
$route['messaging/unsent-messages/(:num)'] = 'messaging/unsent_messages/$1';
$route['messaging/sent-messages'] = 'messaging/sent_messages';
$route['messaging/sent-messages/(:num)'] = 'messaging/sent_messages/$1';
$route['messaging/spoilt-messages'] = 'messaging/spoilt_messages';
$route['messaging/spoilt-messages/(:num)'] = 'messaging/spoilt_messages/$1';
// import functions of messages
$route['messaging/validate-import/(:num)'] = 'messaging/do_messages_import/$1';
$route['messaging/import-template'] = 'messaging/import_template';
$route['messaging/import-messages'] = 'messaging/import_messages';

$route['messaging/send-messages'] = 'messaging/send_messages';

$route['messaging/emails'] = 'messaging/emails';
$route['messaging/emails/(:num)'] = 'messaging/emails/$1';
$route['messaging/sms'] = 'messaging/sms';
$route['messaging/sms/(:num)'] = 'messaging/sms/$1';
$route['resend-sms-message/(:num)'] = 'messaging/resend_sms/$1';
$route['resend-email-message/(:num)'] = 'messaging/resend_email/$1';


/*
*	Land Owners Routes
*/

$route['property-manager/property-owner'] = 'real_estate_administration/property_owners/index';
$route['property-manager/property-owner/add-property-owner'] = 'real_estate_administration/property_owners/add_property_owner';
$route['property-manager/property-owner/edit-property-owner/(:num)'] = 'real_estate_administration/property_owners/edit_property_owner_details/$1';
$route['property-manager/property-owner/delete-property-owner/(:num)'] = 'real_estate_administration/property_owners/delete_property_owner/$1';
$route['property-manager/property-owner/activate-property-owner/(:num)'] = 'real_estate_administration/property_owners/activate_property_owner/$1';
$route['property-manager/property-owner/deactivate-property-owner/(:num)'] = 'real_estate_administration/property_owners/deactivate_property_owner/$1';
$route['property-manager/property-owner/(:num)'] = 'real_estate_administration/property_owners/index/$1';
$route['property-manager/property-owner/(:any)/(:any)/(:num)'] = 'real_estate_administration/property_owners/index/$1/$2/$3';
$route['search-property-owners'] = 'real_estate_administration/property_owners/search_property_owners';
$route['close-search-property-owners'] = 'real_estate_administration/property_owners/close_property_owners_search';

$route['property-manager/properties'] = 'real_estate_administration/property/index';
$route['property-manager/properties/(:num)'] = 'real_estate_administration/property/index/$1';
$route['search-properties'] = 'real_estate_administration/property/search_property';

$route['property-manager/owner-properties/(:num)'] = 'real_estate_administration/property/property_owner_index/$1';
$route['property-manager/owner-properties/(:num)/(:num)'] = 'real_estate_administration/property/property_owner_index/$1/$2';


$route['property-manager/properties/add-property'] = 'real_estate_administration/property/add_property';
$route['property-manager/properties/edit-property/(:num)'] = 'real_estate_administration/property/edit_property/$1';
$route['property-manager/properties/activate-property/(:num)'] = 'real_estate_administration/property/activate_property/$1';
$route['property-manager/properties/deactivate-property/(:num)'] = 'real_estate_administration/property/deactivate_property/$1';


$route['property-rental-units/(:num)'] = 'real_estate_administration/rental_unit/property_rental_unit_index/$1';
$route['property-rental-units/(:num)/(:num)'] = 'real_estate_administration/rental_unit/property_rental_unit_index/$1/$2';




$route['property-manager/properties/(:any)/(:any)/(:num)'] = 'real_estate_administration/property/index/$1/$2/$3';
$route['property-manager/properties/(:any)/(:any)'] = 'real_estate_administration/property/index/$1/$2';
$route['admin/add-property'] = 'real_estate_administration/property/add_property';
$route['edit-property/(:num)'] = 'real_estate_administration/property/edit_property/$1';
$route['edit-property/(:num)/(:num)'] = 'real_estate_administration/property/edit_property/$1/$2';
$route['admin/delete-property/(:num)'] = 'real_estate_administration/property/delete_property/$1';
$route['admin/delete-property/(:num)/(:num)'] = 'real_estate_administration/property/delete_property/$1/$2';
$route['activate-property/(:num)'] = 'real_estate_administration/property/activate_property/$1';
$route['activate-property/(:num)/(:num)'] = 'real_estate_administration/property/activate_property/$1/$2';
$route['deactivate-property/(:num)'] = 'real_estate_administration/property/deactivate_property/$1';
$route['deactivate-property/(:num)/(:num)'] = 'real_estate_administration/property/deactivate_property/$1/$2';
$route['statement/(:num)'] = 'real_estate_administration/property/property_statement/$1';

$route['admin/import-property-template'] = 'real_estate_administration/property/import_charges_template';
$route['admin/import-property/(:num)'] = 'real_estate_administration/property/do_charges_import/$1';
$route['admin/import-charges/(:num)'] = 'real_estate_administration/property/import_charges/$1';

$route['water-management/property-readings'] = 'water_management/index';
$route['cash-office/property-readings'] = 'water_management/index';
$route['import-water-readings-template'] = 'water_management/import_water_readings_template';
$route['import-water-readings'] = 'water_management/do_water_readings_import';
$route['print-water-readings/(:any)'] = 'water_management/print_water_readings/$1';
$route['add-property-readings'] = 'water_management/add_property_invoice';
$route['delete-property-invoices/(:num)'] = 'water_management/delete_property_invoice/$1';


$route['rental-units/(:num)'] = 'real_estate_administration/rental_unit/index/$1';
$route['rental-units/(:num)'] = 'real_estate_administration/rental_unit/index/$1';
$route['rental-units/(:any)/(:any)/(:num)'] = 'real_estate_administration/rental_unit/index/$1/$2/$3';
$route['rental-units/(:any)/(:any)'] = 'real_estate_administration/rental_unit/index/$1/$2';
$route['add-unit'] = 'real_estate_administration/rental_unit/add_unit';
$route['edit-rental-unit/(:num)'] = 'real_estate_administration/rental_unit/edit_rental_unit/$1';
$route['edit-rental-unit/(:num)/(:num)'] = 'real_estate_administration/rental_unit/edit_rental_unit/$1/$2';
$route['change-rental-unit-price/(:num)'] = 'real_estate_administration/rental_unit/edit_rental_unit_price/$1';

$route['admin/delete-unit/(:num)'] = 'real_estate_administration/rental_unit/delete_unit/$1';
$route['admin/delete-unit/(:num)/(:num)'] = 'real_estate_administration/rental_unit/delete_unit/$1/$2';
$route['activate-rental-unit/(:num)'] = 'real_estate_administration/rental_unit/activate_rental_unit/$1';
$route['activate-rental-unit/(:num)/(:num)'] = 'real_estate_administration/rental_unit/activate_rental_unit/$1/$2';
$route['deactivate-rental-unit/(:num)'] = 'real_estate_administration/rental_unit/deactivate_rental_unit/$1';

$route['deactivate-lease/(:num)/(:num)'] = 'real_estate_administration/rental_unit/deactivate_lease/$1/$2';
$route['activate-lease/(:num)'] = 'real_estate_administration/rental_unit/activate_lease/$1';

$route['delete-lease/(:num)'] = 'real_estate_administration/leases/delete_lease/$1';

$route['deactivate--rental-unit/(:num)/(:num)'] = 'real_estate_administration/rental_unit/deactivate_rental_unit/$1/$2';
$route['property-manager/rental-units'] = 'real_estate_administration/rental_unit/rental_units';
$route['property-manager/rental-units/(:num)'] = 'real_estate_administration/rental_unit/rental_units/$1';
$route['add-rental-unit'] = 'real_estate_administration/rental_unit/add_rental_unit';
$route['add-rental-unit-bulk'] = 'real_estate_administration/rental_unit/add_rental_unit_bulk';

$route['search-rental-units'] = 'real_estate_administration/rental_unit/search_rental_units';
$route['close_search_rental_units'] = 'real_estate_administration/rental_unit/close_tenants_search';



$route['tenants'] = 'real_estate_administration/tenants/index';
$route['tenants/(:num)'] = 'real_estate_administration/tenants/all_tenants/$1';
$route['property-manager/tenants'] = 'real_estate_administration/tenants/all_tenants';
$route['property-manager/tenants/(:num)'] = 'real_estate_administration/tenants/all_tenants/$1';
$route['tenants/(:num)'] = 'real_estate_administration/tenants/index/$1';
$route['activate-tenant/(:num)'] = 'real_estate_administration/tenants/activate_tenant/$1';
$route['deactivate-tenant/(:num)'] = 'real_estate_administration/tenants/deactivate_tenant/$1';
$route['edit-tenant/(:num)'] = 'real_estate_administration/tenants/edit_tenant/$1';
$route['tenants/(:num)/(:num)'] = 'real_estate_administration/tenants/index/$1/$2';
$route['tenants/(:any)/(:any)/(:num)'] = 'real_estate_administration/tenants/index/$1/$2/$3';
$route['tenants/(:any)/(:any)'] = 'real_estate_administration/tenants/index/$1/$2';
$route['tenancy-detail/(:num)'] = 'real_estate_administration/tenants/tenancy_detail/$1';
$route['tenancy-detail/(:num)/(:num)'] = 'real_estate_administration/tenants/tenancy_detail/$1/$2';


$route['add-tenant'] = 'real_estate_administration/tenants/add_tenant';
$route['add-tenant/(:num)'] = 'real_estate_administration/tenants/add_tenant/$1';
// $route['add-tenant-unit/(:num)'] = 'real_estate_administration/tenants/allocate_tenant_to_unit/$1';
$route['search-tenants'] = 'real_estate_administration/tenants/search_tenants';
$route['close_search_tenants'] = 'real_estate_administration/tenants/close_tenants_search';





// leases

$route['create-new-lease/(:num)/(:num)'] = 'real_estate_administration/leases/add_lease/$1/$2';
$route['lease-management/leases'] = 'real_estate_administration/leases/index';
$route['lease-management/leases/(:num)'] = 'real_estate_administration/leases/index/$1';

$route['lease-manager/leases-on-hold'] = 'real_estate_administration/leases/leases_on_hold';
$route['lease-manager/leases-on-hold/(:num)'] = 'real_estate_administration/leases/leases_on_hold/$1';
$route['search-held-leases'] = 'real_estate_administration/leases/search_held_leases';
$route['close-held-lease-search'] = 'real_estate_administration/leases/close_held_lease_search';

$route['lease-manager/terminated-leases'] = 'real_estate_administration/leases/terminated_leases';
$route['lease-manager/terminated-leases/(:num)'] = 'real_estate_administration/leases/terminated_leases/$1';
$route['search-terminated-leases'] = 'real_estate_administration/leases/search_terminated_leases';
$route['close-terminated-lease-search'] = 'real_estate_administration/leases/close_terminated_lease_search';





// cash office

$route['cash-office/tenants-accounts'] = 'accounts/index';
$route['cash-office/tenants-accounts/(:num)'] = 'accounts/index/$1';
$route['cash-office/tenants-accounts/(:num)/(:num)'] = 'accounts/index/$1/$2';
$route['search-accounts'] = 'accounts/search_accounts';
$route['search-owners-home'] = 'accounts/search_home_owners';


$route['cash-office/tenants-leases'] = 'accounts/tenant_leases';
$route['cash-office/tenants-leases/(:num)'] = 'accounts/tenant_leases/$1';
$route['cash-office/tenants-leases/(:num)/(:num)'] = 'accounts/tenant_leases/$1/$2';


$route['view-tenant-payments/(:num)/(:num)'] = 'accounts/tenant_lease_payments/$1/$2';
$route['view-tenant-payments/(:num)/(:num)/(:num)'] = 'accounts/tenant_lease_payments/$1/$2/$3';

$route['view-tenant-invoices/(:num)/(:num)'] = 'accounts/tenant_lease_invoices/$1/$2';
$route['view-tenant-invoices/(:num)/(:num)/(:num)'] = 'accounts/tenant_lease_invoices/$1/$2/$3';


$route['view-tenant-credits/(:num)/(:num)'] = 'accounts/tenant_lease_credits/$1/$2';
$route['view-tenant-credits/(:num)/(:num)/(:num)'] = 'accounts/tenant_lease_credits/$1/$2/$3';

$route['view-tenant-debits/(:num)/(:num)'] = 'accounts/tenant_lease_debits/$1/$2';
$route['view-tenant-debits/(:num)/(:num)/(:num)'] = 'accounts/tenant_lease_debits/$1/$2/$3';




$route['cash-office/tenants-payments'] = 'accounts/tenants_payments';
$route['cash-office/tenants-payments/(num)'] = 'accounts/tenants_payments/$1';
$route['search-tenant-units'] = 'accounts/search_tenants_payments';
$route['delete-payment-item/(:num)'] = 'accounts/delete_payment_items_id/$1';




$route['cash-office/tenants-credit-notes'] = 'accounts/tenants_credit_notes';
$route['cash-office/tenants-credit-notes/(num)'] = 'accounts/tenants_credit_notes/$1';
$route['search-credit-notes'] = 'accounts/search_tenants_credit_notes';
$route['delete-credit-note-item/(:num)'] = 'accounts/delete_credit_note_id/$1';

// debit Notes

$route['cash-office/tenants-debit-notes'] = 'accounts/tenants_debit_notes';
$route['cash-office/tenants-debit-notes/(num)'] = 'accounts/tenants_debit_notes/$1';
$route['search-debit-notes'] = 'accounts/search_tenants_debit_notes';
$route['delete-debit-note-item/(:num)'] = 'accounts/delete_debit_note_id/$1';

// end of debit notes


// invoice items



$route['tenant-invoices/(:num)'] = 'accounts/get_tenants_invoices/$1';
$route['tenant-credit-notes/(:num)'] = 'accounts/get_tenants_credit_notes/$1';

$route['cash-office/tenants-invoices'] = 'accounts/tenants_invoices';
$route['cash-office/tenants-invoices/(num)'] = 'accounts/tenants_invoices/$1';
$route['search-invoice'] = 'accounts/search_tenants_invoice';
$route['delete-invoice/(:num)/(:num)'] = 'accounts/delete_lease_invoice/$1/$2';
$route['delete-invoice-item/(:num)'] = 'accounts/delete_invoice_items_id/$1';
// end of invoice items

$route['close_search_accounts'] = 'accounts/close_accounts_search';
$route['cash-office/payments/(:num)/(:num)'] = 'accounts/payments/$1/$2';
$route['owners-payments/(:num)/(:num)'] = 'accounts/owners_payments/$1/$2';


$route['cash-office/cash-return-form'] = 'reports/all_transactions';
$route['cash-office/cash-return-form/(:num)'] = 'reports/all_transactions/$1';

$route['reports/tenants-payments-report'] = 'administration/reports/all_transactions';
$route['reports/tenants-payments-report/(:num)'] = 'administration/reports/all_transactions/$1';
$route['reports/owners-payments-report'] = 'administration/reports/all_transactions_owners';
$route['reports/owners-payments-report/(:num)'] = 'administration/reports/all_transactions_owners/$1';

$route['reports/tenants-accounts'] = 'administration/reports/all_defaulters';
$route['reports/tenants-accounts/(:num)'] = 'administration/reports/all_defaulters/$1';
$route['reports/owners-accounts'] = 'administration/reports/all_defaulters_owners';
$route['reports/owners-accounts/(:num)'] = 'administration/reports/all_defaulters_owners/$1';

$route['export-defaulters'] = 'administration/reports/export_defaulters';
$route['export-defaulters-owners'] = 'administration/reports/export_defaulters_owners';
$route['export-transactions'] = 'administration/reports/export_transactions';

$route['search-transactions/(:any)'] = 'administration/reports/search_transactions/$1';
$route['search-transactions-owners'] = 'administration/reports/search_transactions_owners';
$route['search-defaulters'] = 'administration/reports/search_defaulters';
$route['search-defaulters-owners'] = 'administration/reports/search_defaulters_owners';
$route['reports/income-and-expense'] = 'administration/reports/income_and_expense';
$route['reports/income-and-expense/(:any)/(:num)'] = 'administration/reports/income_and_expense/$1/$2';
$route['print-income-statement'] = 'administration/reports/income_and_expense_print';


$route['admin/points-categories'] = 'real_estate_administration/points_category/index';
$route['admin/points-categories/(:any)/(:any)/(:num)'] = 'real_estate_administration/points_category/index/$1/$2/$3';
$route['admin/points-categories/(:any)/(:any)'] = 'real_estate_administration/points_category/index/$1/$2';
$route['admin/add-points-category'] = 'real_estate_administration/points_category/add_points_category';
$route['edit-points-category/(:num)'] = 'real_estate_administration/points_category/edit_points_category/$1';
$route['edit-points-category/(:num)/(:num)'] = 'real_estate_administration/points_category/edit_points_category/$1/$2';
$route['admin/delete-points-category/(:num)'] = 'real_estate_administration/points_category/delete_points_category/$1';
$route['admin/delete-points-category/(:num)/(:num)'] = 'real_estate_administration/points_category/delete_points_category/$1/$2';
$route['activate-points-category/(:num)'] = 'real_estate_administration/points_category/activate_points_category/$1';
$route['activate-points-category/(:num)/(:num)'] = 'real_estate_administration/points_category/activate_points_category/$1/$2';
$route['deactivate-points-category/(:num)'] = 'real_estate_administration/points_category/deactivate_points_category/$1';
$route['deactivate-points-category/(:num)/(:num)'] = 'real_estate_administration/points_category/deactivate_points_category/$1/$2';


$route['import/tenants'] = 'real_estate_administration/tenants/import_tenants';
$route['import/tenants-template'] = 'real_estate_administration/tenants/tenants_import_template';
$route['import/import-tenants'] = 'real_estate_administration/tenants/tenants_import';


$route['import/tenants-units'] = 'real_estate_administration/leases/import_tenant_units';
$route['import/tenants-units-template'] = 'real_estate_administration/leases/tenant_units_import_template';
$route['import/import-tenants-units'] = 'real_estate_administration/leases/tenant_units_import';


$route['import/lease-details'] = 'real_estate_administration/leases/import_lease_details';
$route['import/lease-details-template'] = 'real_estate_administration/leases/lease_details_import_template';
$route['import/import-lease-details'] = 'real_estate_administration/leases/lease_details_import';

$route['import/invoice-details'] = 'real_estate_administration/leases/import_invoice_details';
$route['import/invoice-details-template'] = 'real_estate_administration/leases/invoice_details_import_template';
$route['import/import-invoice-details'] = 'real_estate_administration/leases/invoice_details_import';

$route['import/payment-details'] = 'real_estate_administration/leases/import_payment_details';
$route['import/payment-details-template'] = 'real_estate_administration/leases/payment_details_import_template';
$route['import/import-payment-details'] = 'real_estate_administration/leases/payment_details_import';



$route['import/home-owners'] = 'real_estate_administration/leases/import_home_owners';
$route['import/home-owners-template'] = 'real_estate_administration/leases/home_owners_import_template';
$route['import/import-home-owners'] = 'real_estate_administration/leases/home_owners_import';




$route['import/payments'] = 'accounts/import_payments';
$route['import/payments-template'] = 'accounts/payments_import_template';
$route['import/import-payments'] = 'accounts/payments_import';
$route['import/update-payment-item/(:num)'] = 'accounts/update_payment_item/$1';

$route['send-arrears/(:num)'] = 'accounts/send_arrears/$1';
$route['send-water-invoices/(:num)'] = 'water_management/send_invoices/$1';
$route['cash-office/print-receipt/(:num)/(:num)/(:num)'] = 'accounts/print_receipt/$1/$2/$3';
$route['cash-office/print-owners-receipt/(:num)/(:num)/(:num)'] = 'accounts/print_owners_receipt/$1/$2/$3';
$route['lease-statement/(:num)'] = 'accounts/print_lease_statement/$1';
$route['lease-invoices/(:num)'] = 'accounts/lease_invoices/$1';
$route['lease-invoices/(:num)/(:num)'] = 'accounts/lease_invoices/$1/$2';
$route['lease-payments/(:num)'] = 'accounts/lease_payments/$1';
$route['lease-payments/(:num)/(:num)'] = 'accounts/lease_payments/$1/$2';
$route['invoice/(:num)/(:any)/(:num)'] = 'accounts/print_invoice/$1/$2/$3';
$route['invoice/(:num)/(:any)/(:num)/(:any)'] = 'accounts/print_invoice/$1/$2/$3/$4';
$route['edit-invoice/(:num)/(:any)/(:num)'] = 'accounts/edit_invoice/$1/$2/$3';
$route['owners-invoice/(:num)/(:num)/(:any)/(:num)'] = 'accounts/print_owners_invoice/$1/$2/$3/$4';
$route['cash-office/send-sms/(:num)/(:num)/(:num)/(:any)/(:any)'] = 'accounts/send_sms/$1/$2/$3/$4/$5';
$route['rental-payments/(:num)'] = 'accounts/rental_payments/$1';

$route['send-invoice/(:num)/(:any)/(:num)/(:any)'] = 'accounts/send_single_invoices/$1/$2/$3/$4';


$route['send-owners-receipt/(:num)/(:num)/(:num)'] = 'accounts/send_owners_receipt/$1/$2/$3';

$route['reports/property'] = 'administration/reports/search_property_report';
$route['reports/property-list'] = 'administration/reports/view_property';

$route['reports/agency-reports'] = 'administration/reports/search_agency_property_report';
$route['reports/agency-property-list'] = 'administration/reports/agency_property';


$route['reports/tenants-payments-report'] = 'administration/reports/search_property_report_total_payments';
$route['reports/total-payment-list'] = 'reports/reports/print_montly_report';
// beeee

$route['cash-office/owners'] = 'accounts/owners';
$route['cash-office/owners/(:num)'] = 'accounts/owners/$1';
$route['cash-office/owners/(:num)/(:num)'] = 'accounts/owners/$1/$2';
$route['search-owners'] = 'accounts/search_owners';
$route['close_search_owners'] = 'accounts/close_owners_search';
$route['cash-office/payments/(:num)/(:num)'] = 'accounts/payments/$1/$2';
$route['send-owners/(:num)/(:num)'] = 'accounts/send_owners/$1/$2';



$route['invoices-&-payments/invoices'] = 'supervisory/invoices';
$route['invoices-&-payments/invoices/(:num)'] = 'supervisory/invoices/$1';

$route['invoices-&-payments/payments'] = 'supervisory/payments';
$route['invoices-&-payments/payments/(:num)'] = 'supervisory/payments/$1';


$route['property-billing/(:num)'] = 'real_estate_administration/property/property_billing/$1';
$route['property-billing/(:num)/(:num)'] = 'real_estate_administration/property/property_billing/$1/$2';

$route['add-biliing/(:num)'] = 'real_estate_administration/property/add_billing/$1';
$route['add-biliing/(:num)/(:num)'] = 'real_estate_administration/property/add_billing/$1/$2';



$route['property-invoicing/(:num)'] = 'real_estate_administration/property/property_invoicing/$1';
$route['property-invoicing/(:num)/(:num)'] = 'real_estate_administration/property/property_invoicing/$1/$2';

$route['add-invoice-structure/(:num)'] = 'real_estate_administration/property/add_property_invoicing/$1';
$route['add-invoice-structure/(:num)/(:num)'] = 'real_estate_administration/property/add_property_invoicing/$1/$2';



$route['property-manager/home-owners'] = 'real_estate_administration/home_owners/all_home_owners';
$route['property-manager/home-owners/(:num)'] = 'real_estate_administration/home_owners/all_home_owners/$1';
$route['home-owners/(:num)'] = 'real_estate_administration/home_owners/index/$1';
$route['activate-home-owner/(:num)'] = 'real_estate_administration/home_owners/activate_home_owner/$1';
$route['deactivate-home-owner/(:num)'] = 'real_estate_administration/home_owners/deactivate_home_owner/$1';
$route['edit-home-owner/(:num)'] = 'real_estate_administration/home_owners/edit_home_owner/$1';
$route['home-owners/(:num)/(:num)'] = 'real_estate_administration/home_owners/index/$1/$2';
$route['home-owners/(:any)/(:any)/(:num)'] = 'real_estate_administration/home_owners/index/$1/$2/$3';
$route['home-owners/(:any)/(:any)'] = 'real_estate_administration/home_owners/index/$1/$2';

$route['add-home-owner'] = 'real_estate_administration/home_owners/add_home_owner';
$route['add-home-owner/(:num)'] = 'real_estate_administration/home_owners/add_home_owner/$1';
$route['add-home-owner-unit/(:num)'] = 'real_estate_administration/home_owners/allocate_home_owner_to_unit/$1';
$route['search-home-owners'] = 'real_estate_administration/home_owners/search_home_owners';
$route['close_search_home_owners'] = 'real_estate_administration/home_owners/close_home_owners_search';


$route['update-tenant-invoice/(:num)/(:num)/(:any)/(:num)/(:num)'] = 'accounts/update_account_invoice/$1/$2/$3/$4/$5';
$route['update-tenant-payment/(:num)/(:num)/(:num)'] = 'accounts/update_account_payment/$1/$2/$3';
$route['update-payment-detail/(:num)/(:num)'] = 'accounts/update_payment_detail/$1/$2';


$route['update-owner-payment-detail/(:num)/(:num)'] = 'accounts/update_owner_payment_detail/$1/$2';
$route['update-owner-payment/(:num)/(:num)/(:num)'] = 'accounts/update_owner_account_payment/$1/$2/$3';


$route['send-tenants-receipt/(:num)/(:num)/(:num)'] = 'accounts/send_tenants_receipt/$1/$2/$3';



$route['cash-office/landlord-return'] = 'administration/reports/search_property_report';
// $route['reports/landlord-return/(:num)'] = 'administration/reports/return/$1';

$route['reports/tenant-statement/(:num)'] = 'administration/reports/print_tenant_statement/$1';
$route['reports/owner-statement/(:num)/(:num)'] = 'administration/reports/print_owner_statement/$1/$2';
$route['reports/send-statement/(:num)'] = 'administration/reports/send_tenant_statements/$1';
$route['reports/send-owner-statement/(:num)/(:num)'] = 'administration/reports/send_owner_statements/$1/$2';



$route['print-tenant-report'] = 'administration/reports/tenants_export';
$route['print-owners-report'] = 'administration/reports/owners_export';


// creditor services

$route['accounts/creditor-services']  = 'creditor/creditor_service/index';
$route['accounts/creditor-services/(:num)']  = 'creditor/creditor_service/index/$1';
$route['add-creditor-service']  = 'creditor/creditor_service/add_creditor_service';
$route['edit-creditor-service/(:num)']  = 'creditor/creditor_service/edit_creditor_service/$1';
$route['activate-creditor-service/(:num)']  = 'creditor/creditor_service/activate_creditor_service/$1';
$route['deactivate-creditor-service/(:num)']  = 'creditor/creditor_service/deactivate_creditor_service/$1';

$route['edit-inflow-service/(:num)']  = 'inflows/inflow_service/edit_inflow_service/$1';
$route['activate-inflow-service/(:num)']  = 'inflows/inflow_service/activate_inflow_service/$1';
$route['deactivate-inflow-service/(:num)']  = 'inflows/inflow_service/deactivate_inflow_service/$1';
$route['accounts/inflow/edit_inflow/(:num)']  = 'accounts/inflows/edit_inflow/$1';
$route['accounts/inflow/statement/(:num)']  = 'accounts/inflows/statement/$1';
$route['accounts/inflows/search_inflow_account/(:num)']  = 'accounts/inflows/search_inflow_account/$1';
$route['accounts/inflows/close_inflow_search/(:num)']  = 'accounts/inflows/close_inflow_search/$1';




$route['water-management/water-readings'] = 'accounts/water_tenants';
$route['water-management/water-readings/(:num)'] = 'accounts/water_tenants/$1';
$route['water-management/water-readings/(:num)/(:num)'] = 'accounts/water_tenants/$1/$2';
$route['search-accounts'] = 'accounts/search_accounts';
$route['search-tenant-leases'] = 'accounts/search_tenant_accounts';
$route['search-owners-home'] = 'accounts/search_home_owners';
$route['water-management/payments/(:num)/(:num)'] = 'accounts/payments/$1/$2';
$route['update-water-invoice/(:num)/(:num)/(:num)'] = 'accounts/update_invoice/$1/$2/$3';
$route['invoice/(:num)/(:any)/(:num)'] = 'accounts/print_invoice/$1/$2/$3';

$route['send-arrears/(:num)'] = 'accounts/send_arrears/$1';


$route['lease-invoices/(:num)'] = 'accounts/lease_invoices/$1';
$route['lease-invoices/(:num)/(:num)'] = 'accounts/lease_invoices/$1/$2';



// start from here
$route['accounts/tenants-accounts'] = 'accounts/index';
$route['accounts/tenants-accounts/(:num)'] = 'accounts/index/$1';
$route['accounts/tenants-accounts/(:num)/(:num)'] = 'accounts/index/$1/$2';
$route['search-accounts'] = 'accounts/search_accounts';
$route['close_search_accounts'] = 'accounts/close_accounts_search';
$route['close_search_leases'] = 'accounts/close_leases_search';
$route['accounts/payments/(:num)/(:num)'] = 'accounts/payments/$1/$2';


$route['lease-manager/tenants'] = 'real_estate_administration/tenants/index';
// $route['lease-manager/tenants/(:num)'] = 'real_estate_administration/tenants/all_tenants/$1';
$route['property-manager/tenants'] = 'real_estate_administration/tenants/all_tenants';
$route['property-manager/tenants/(:num)'] = 'real_estate_administration/tenants/all_tenants/$1';
$route['lease-manager/tenants/(:num)'] = 'real_estate_administration/tenants/index/$1';
$route['activate-tenant/(:num)'] = 'real_estate_administration/tenants/activate_tenant/$1';
$route['deactivate-tenant/(:num)'] = 'real_estate_administration/tenants/deactivate_tenant/$1';
$route['edit-tenant/(:num)'] = 'real_estate_administration/tenants/edit_tenant/$1';
$route['lease-manager/tenants/(:num)/(:num)'] = 'real_estate_administration/tenants/index/$1/$2';
$route['lease-manager/tenants/(:any)/(:any)/(:num)'] = 'real_estate_administration/tenants/index/$1/$2/$3';
$route['lease-manager/tenants/(:any)/(:any)'] = 'real_estate_administration/tenants/index/$1/$2';


$route['add-tenant'] = 'real_estate_administration/tenants/add_tenant';
$route['add-tenant/(:num)'] = 'real_estate_administration/tenants/add_tenant/$1';
// $route['add-tenant-unit/(:num)'] = 'real_estate_administration/tenants/allocate_tenant_to_unit/$1';
$route['search-tenants'] = 'real_estate_administration/tenants/search_tenants';
$route['close_search_tenants'] = 'real_estate_administration/tenants/close_tenants_search';



$route['lease-manager/leases'] = 'real_estate_administration/leases/index';
$route['lease-manager/leases/(:num)'] = 'real_estate_administration/leases/index/$1';
$route['search-leases'] = 'real_estate_administration/leases/search_leases';
$route['close-lease-search'] = 'real_estate_administration/leases/close_lease_search';



$route['lease-manager/due-leases'] = 'real_estate_administration/leases/due_leases';
$route['lease-manager/due-leases/(:num)'] = 'real_estate_administration/leases/due_leases/$1';
$route['search-due-leases'] = 'real_estate_administration/leases/search_leases';
$route['close-due-lease-search'] = 'real_estate_administration/leases/close_lease_search';

$route['lease-manager/vacant-houses'] = 'real_estate_administration/rental_unit/vacant_units';
$route['lease-manager/vacant-houses/(:num)'] = 'real_estate_administration/leases/vacant_units/$1';


$route['setup/configuration'] = 'admin/configuration';
$route['setup/edit-configuration'] = 'admin/edit_configuration';
$route['setup/edit-configuration/(:num)'] = 'admin/edit_configuration/$1';

$route['setup/invoice-type'] = 'setup/invoice_type_index';
$route['setup/add-invoice-type'] = 'setup/invoice_type_add';
$route['setup/billing-schedule'] = 'setup/invoice_billing_schedule_index';
$route['setup/add-billing-schedule'] = 'setup/invoice_billing_schedule_add';






$route['lease-detail/(:num)'] = 'real_estate_administration/leases/lease_detail/$1';
$route['add-tenant-unit'] = 'real_estate_administration/tenants/allocate_tenant_to_unit';
$route['add-lease-charge/(:num)'] = 'real_estate_administration/leases/add_billing/$1';

// rental unit allocation
$route['tenant-lease-detail/(:num)'] = 'real_estate_administration/leases/tenant_lease_detail/$1';

$route['update-tenant-details/(:num)'] = 'real_estate_administration/tenants/update_tenant_details/$1';
$route['update-lease-details/(:num)'] ='real_estate_administration/leases/update_lease_detail/$1';
$route['update-billing-item/(:num)/(:num)'] = 'real_estate_administration/leases/update_property_billing/$1/$2';
$route['delete-billing/(:num)/(:num)'] = 'real_estate_administration/leases/delete_property_billing/$1/$2';

$route['update-billing/(:num)'] = 'real_estate_administration/leases/update_billing/$1';
$route['close-lease/(:num)/(:num)'] = 'real_estate_administration/leases/close_lease/$1/$2';
$route['transfer-lease/(:num)/(:num)'] = 'real_estate_administration/leases/transfer_lease/$1/$2';


$route['vacation-lease/(:num)/(:num)'] = 'real_estate_administration/leases/vacation_notice_lease/$1/$2';


// accounting module

$route['accounting/hospital-accounts'] = 'accounting/hospital_accounts/index';
// $route['accounting/landlord-transactions'] = 'accounting/petty_cash/index';
// $route['accounting/landlord-transactions/(:any)/(:any)'] = 'accounting/petty_cash/index/$1/$2';
// $route['accounting/landlord-transactions/(:any)'] = 'accounting/petty_cash/index/$1';
$route['delete-invoice-entry/(:num)'] = 'accounting/petty_cash/delete_invoice_entry/$1';
$route['delete-payment-entry/(:num)'] = 'accounting/petty_cash/delete_payment_entry/$1';
$route['delete-provider-invoice-entry/(:num)/(:num)'] = 'accounting/creditors/delete_provider_invoice_entry/$1/$2';
$route['delete-provider-payment-entry/(:num)/(:num)'] = 'accounting/creditors/delete_provider_payment_entry/$1/$2';
$route['delete-creditor-invoice-entry/(:num)/(:num)'] = 'accounting/creditors/delete_creditor_invoice_entry/$1/$2';
$route['delete-creditor-payment-entry/(:num)/(:num)'] = 'accounting/creditors/delete_creditor_payment_entry/$1/$2';


//account balances
$route['setup/charts-of-accounts'] = 'accounting/petty_cash/account_balances';
$route['setup/charts-of-accounts/(:num)'] = 'accounting/petty_cash/account_balances/$1';
$route['setup/charts-of-accounts/activate-account/(:num)'] = 'accounting/petty_cash/activate_account/$1';
$route['setup/charts-of-accounts/deactivate-account/(:num)'] = 'accounting/petty_cash/deactivate_account/$1';
$route['setup/charts-of-accounts/edit-account/(:num)'] = 'accounting/petty_cash/edit_account/$1';
$route['accounting/add-account'] = 'accounting/petty_cash/add_account';




// accounting and company financials


$route['accounting/write-cheque'] = 'accounting/petty_cash/write_cheque';
$route['accounting/write-cheque/(:num)'] = 'accounting/petty_cash/write_cheque/$1';
$route['accounting/providers'] = 'accounting/creditors/providers';
$route['accounting/providers/(:num)'] = 'accounting/creditors/providers/$1';
$route['update-provider-balance/(:num)'] =  'accounting/creditors/update_opening_balance/$1';
$route['accounting/provider-statement/(:num)'] = 'accounting/creditors/provider_statement/$1';
$route['company-financials/profit-and-loss'] = 'accounting/company_financial/profit_and_loss';
$route['company-financials/balance-sheet'] = 'accounting/company_financial/balance_sheet';
$route['company-financials/balance-sheet-search'] = 'accounting/company_financial/search_balance_sheet';

$route['company-financials/general-ledger'] = 'accounting/petty_cash/ledger';
$route['company-financials/general-ledger/(:any)/(:any)'] = 'accounting/petty_cash/ledger/$1/$2';
$route['company-financials/general-ledger/(:any)'] = 'accounting/petty_cash/ledger/$1';

$route['accounting/creditors'] = 'accounting/creditors/index';
$route['accounting/creditors/(:num)'] = 'accounting/creditors/index/$1';
$route['delete-creditor-invoice/(:num)'] = 'accounting/petty_cash/delete_invoice_entry/$1';
$route['delete-creditor-payment/(:num)'] = 'accounting/petty_cash/delete_payment_entry/$1';


$route['hospital-reports/debtors-report'] = 'accounting/reports/debtors';
$route['hospital-reports/debtors-report/(:num)'] = 'accounting/reports/debtors/$1';
$route['search-debtors-report'] = 'accounting/reports/search_debtors_report';


$route['creditor-statement/(:num)'] = 'accounting/creditors/statement/$1';
$route['accounts-transactions/(:num)'] = 'accounting/petty_cash/get_transactions/$1';
$route['visit-transactions/(:num)'] = 'accounting/company_financial/search_visit_transactions/$1';




// petty cash modules
$route['accounting/petty-cash'] = 'accounting/petty_cash/petty_cash_statement';
$route['accounting/petty-cash/(:num)'] = 'accounting/petty_cash/petty_cash_statement/$1';

// end of accounting module


$route['mpesa-record/(:any)/(:any)/(:any)/(:num)/(:num)/(:num)'] = 'accounts/mpesa_transaction/$1/$2/$3/$4/$5/$6';

$route['mpesa-receive/(:num)'] = 'mpesa/mpesa_receive_transaction/$1';


$route['print-tenant-statement/(:num)/(:num)'] = 'accounts/print_tenant_statement/$1/$2';

$route['accounts/tenants-arrears'] = 'accounts/search_property_report';
$route['accounts/print-statement'] = 'accounts/property_statement';


$route['create-lease-invoice/(:num)'] = 'accounts/create_lease_invoice/$1';
$route['batch-invoice/(:num)'] = 'accounts/batch_lease_invoice/$1';

$route['cash-office/unreconcilled-payments'] = 'accounts/unreconcilled_payments';
$route['cash-office/unreconcilled-payments/(:num)'] = 'accounts/unreconcilled_payments/$1';



$route['reports/landlord-report'] = 'administration/reports/landlord_report';
$route['view-landlord-report/(:num)'] = 'administration/reports/landlord_report_detail/$1';

$route['ticket-management/resolved-tickets'] = 'ticket_management/tickets/resolved_tickets';
$route['ticket-management/resolved-tickets/(:num)'] = 'ticket_management/tickets/resolved_tickets/$1';
$route['ticket-management/unresolved-tickets'] = 'ticket_management/tickets/unresolved_tickets';
$route['ticket-management/unresolved-tickets/(:num)'] = 'ticket_management/tickets/unresolved_tickets/$1';
$route['add-ticket-message/(:num)/(:num)/(:num)'] = 'ticket_management/tickets/add_ticket_message/$1/$2/$3';
$route['ticket-management/add-ticket'] = 'ticket_management/tickets/add_ticket/$1';
$route['ticket-timeline/(:num)'] = 'ticket_management/tickets/ticket_timeline/$1';
$route['close-ticket/(:num)'] = 'ticket_management/tickets/close_ticket/$1';
$route['search-tickets'] = 'ticket_management/tickets/search_tickets/$1';
$route['close-tickets-search']  = 'ticket_management/tickets/close_search_tickets/$1';


// reports start
// personnel
$route['reports/personnel-report'] = 'reports/reports/personnel_report';
$route['search-personnel-report'] = 'reports/reports/search_personnel_report';
$route['close-search-personnel-report'] = 'reports/reports/close_searched_personnel';

//landlord
$route['reports/landlord-report'] = 'reports/reports/landlord_report';
$route['search-landlord-report'] = 'reports/reports/search_landlord_report';
$route['close-search-landlord-report'] = 'reports/reports/close_searched_landlord';


//tenants
$route['reports/tenants-reports'] = 'reports/reports/tenants_report';
$route['search-tenants-report'] = 'reports/reports/search_tenants_report';
$route['close-search-tenants-report'] = 'reports/reports/close_searched_tenants';


//property
$route['reports/property-report'] = 'reports/reports/property_report';
$route['search-property-report'] = 'reports/reports/search_property_report';
$route['close-search-property-report'] = 'reports/reports/close_searched_property';

//property
$route['reports/expense-report'] = 'reports/reports/expense_report';
$route['search-expense-report'] = 'reports/reports/search_expense_report';
$route['close-search-expense-report'] = 'reports/reports/close_searched_expense';

// finance

// mpesa_test

$route['mpesa-pull'] = 'mpesa/cron/get_mpesa_transactions';
$route['mpesa-push'] = 'mpesa/cron/push_mpesa_transactions';
$route['cash-office/mpesa-received'] = 'accounts/unreconcilled_payments';
$route['cash-office/mpesa-received/(:num)'] = 'accounts/unreconcilled_payments/$1';







// mpesa modules
$route['reconcile-payment/(:num)'] = 'mpesa/transactions/reconcile_payment/$1';
$route['mpesa-transactions/unallocated-transactions'] = 'mpesa/transactions/unreconcilled_payments';
$route['mpesa-transactions/unallocated-transactions/(:num)'] = 'mpesa/transactions/unreconcilled_payments/$1';
$route['search-mpesa-transactions'] = 'mpesa/transactions/search_mpesa_transactions';

$route['mpesa-transactions/completed-transactions'] = 'mpesa/transactions/completed_mpesa_payments';
$route['mpesa-transactions/completed-transactions/(:num)'] = 'mpesa/transactions/completed_mpesa_payments/$1';
$route['search-completed-mpesa-transactions'] = 'mpesa/transactions/search_completed_mpesa_transactions';


$route['mpesa-transactions/reversed-transactions'] = 'mpesa/transactions/reversed_mpesa_payments';
$route['mpesa-transactions/reversed-transactions/(:num)'] = 'mpesa/transactions/reversed_mpesa_payments/$1';
$route['search-reversed-mpesa-transactions'] = 'mpesa/transactions/search_reversed_mpesa_transactions';

$route['mpesa-transactions/all-mpesa-transactions'] = 'mpesa/transactions/all_mpesa_transactions_payments';
$route['mpesa-transactions/all-mpesa-transactions/(:num)'] = 'mpesa/transactions/all_mpesa_transactions_payments/$1';
$route['search-all-mpesa-transactions'] = 'mpesa/transactions/search_all_mpesa_transactions';

$route['update-mpesa-payments'] = 'mpesa/transactions/update_mpesa_list';

$route['reverse-payment/(:num)/(:num)/(:num)/(:num)'] = 'mpesa/transactions/reverse_payment_detail/$1/$2/$3/$4';

$route['cron-update-mpesa-payments'] = 'mpesa/transactions/update_mpesa_list_cron';
$route['reverse-payment/(:num)/(:num)/(:num)'] = 'mpesa/transactions/reverse_payment_detail/$1/$2/$3';

$route['print-mpesa-transactions'] = 'mpesa/transactions/print_mpesa_transactions';
$route['print-mpesa-transactions/(:num)'] = 'mpesa/transactions/print_mpesa_transactions/$1';






$route['search-tenant-lease'] = 'accounts/search_tenant_lease';

// purchases

$route['accounting/landlord-expenses'] = 'finance/purchases/all_purchases';
$route['accounting/landlord-expenses/(:num)'] = 'finance/purchases/all_purchases/$1';


$route['accounting/petty-cash'] = 'finance/purchases/petty_cash';
$route['accounting/petty-cash/(:num)'] = 'finance/purchases/petty_cash/$1';
$route['print-petty-cash'] = 'finance/purchases/print_petty_cash';
$route['print-vourcher/(:num)'] = 'finance/purchases/print_voucher/$1';

$route['accounting/landlord-payments'] = 'finance/landlord/landlord_payments';
$route['accounting/landlord-payments/(:num)'] = 'finance/landlord/landlord_payments/$1';

$route['accounting/landlord-receipts'] = 'finance/landlord/landlord_receipts';
$route['accounting/landlord-receipts/(:num)'] = 'finance/landlord/landlord_receipts/$1';



// fiance write cheques
$route['accounting/accounts-transfer'] = 'finance/transfer/write_cheque';
$route['accounting/accounts-transfer/(:num)'] = 'finance/transfer/write_cheque/$1';

$route['accounting/purchase-payments'] = 'finance/purchases/purchase_payments';




// bills

$route['accounting/creditors'] = 'finance/creditors/creditors_list';
$route['accounting/creditor-invoices'] = 'finance/creditors/creditors_invoices';
$route['search-creditor-invoices'] = 'finance/creditors/search_creditors_invoice';
$route['search-creditor-bill/(:num)'] = 'finance/creditors/search_creditors_bill/$1';
$route['close-search-creditors-invoices'] = 'finance/creditors/close_searched_invoices_creditor';
$route['finance/add-creditor'] = 'finance/creditors/add_creditor';
$route['finance/edit-creditor/(:num)'] = 'finance/creditors/edit_creditor/$1';

// credit notes

$route['accounting/creditor-credit-notes'] = 'finance/creditors/creditors_credit_note';
$route['search-creditor-credit-notes'] = 'finance/creditors/search_creditors_credit_notes';
$route['close-search-creditors-credit-notes'] = 'finance/creditors/close_searched_credit_notes_creditor';



// payments_import
$route['accounting/creditor-payments'] = 'finance/creditors/creditors_payments';
$route['search-creditor-payments'] = 'finance/creditors/search_creditors_payments';
$route['search-creditor-payments/(:num)'] = 'finance/creditors/search_creditors_payments/$1';
$route['close-search-creditors-payments'] = 'finance/creditors/close_searched_payments_creditor';
$route['delete-creditor-payment-item/(:num)/(:num)'] = 'finance/creditors/delete_creditor_payment/$1/$2';


$route['company-financials'] = 'financials/company_financial/index';

$route['company-financials/profit-and-loss'] = 'financials/company_financial/profit_and_loss';

$route['company-financials/balance-sheet'] = 'financials/company_financial/balance_sheet';
$route['company-financials/aged-receivables'] = 'financials/company_financial/aged_receivables';
$route['company-financials/sales-taxes'] = 'financials/company_financial/sales_taxes';
$route['company-financials/customer-income'] = 'financials/company_financial/customers_income';
$route['company-financials/vendor-expenses'] = 'financials/company_financial/vendor_expenses';
$route['company-financials/aged-payables'] = 'financials/company_financial/aged_payables';
$route['creditor-statement/(:num)'] = 'financials/company_financial/creditor_statement/$1';
$route['print-creditor-statement/(:num)'] = 'financials/company_financial/print_creditor_statement/$1';


$route['company-financials/general-ledger'] = 'financials/company_financial/general_ledger';

$route['company-financials/account-transactions'] = 'financials/company_financial/account_transactions';
